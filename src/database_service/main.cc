/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes
#include "databaseserviceengine.h"
#include "databaseserviceengineconfig.h"
#include "databaseadapter.h"

// common includes
#include "filelogginghandler.h"
#include "logginghandlerinterface.h"
#include "logginghelper.h"

// system includes
#include <format>
#include <iostream>
#include <span>
#include <string>

//------------------------------------------------------------------------------

void loggingCallback(logging_handler::LoggingHandlerInterface &logging_handler_interface,
                     const logging_handler::LoggingHandlerInterface::LoggingLevel &level,
                     const std::string &msg)
{
  logging_handler_interface.log(level, msg);
}

//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  const std::span<char*> args {argv, static_cast<size_t>(argc)};
  if (argc != 2)
  {
    std::cerr << "Usage: database_service <config_file_path>\n";
    return -1;
  }

  std::unique_ptr<rpi_utilities::database::DatabaseServiceEngineConfig> config;
  try
  {
    config = std::make_unique<rpi_utilities::database::DatabaseServiceEngineConfig>(args[1]);
  }
  catch (const common::CommonException &ex)
  {
    std::cerr<< std::format("Error while parsing configuration JSON: {}\n", ex.what());
    return -1;
  }

  logging_handler::FileLoggingHandler file_logging_handler {
    "database_service",
    config->getLogDirectory()
  };

  file_logging_handler.setLoggingLevel(config->getLogLevel());

  logging_handler::setLoggingCallback(
    [&](const logging_handler::LoggingHandlerInterface::LoggingLevel &logging_level,
        const std::string &message) {
      loggingCallback(file_logging_handler, logging_level, message);
    }
  );

  std::shared_ptr<rpi_utilities::database::DatabaseAdapter> database_adapter_interface
      = std::make_shared<rpi_utilities::database::DatabaseAdapter>(config->getDatabaseAddress(),
                                                                   config->getDatabaseName(),
                                                                   config->getDatabaseUser(),
                                                                   config->getDatabasePassword());

  auto external_modules = std::make_unique<
    rpi_utilities::database::DatabaseServiceEngine::ExternalModules>(
    std::move(config), database_adapter_interface);

  rpi_utilities::database::DatabaseServiceEngine engine{std::move(external_modules)};

  engine.start();

  while (std::tolower(std::cin.get()) != 'q');
  return 0;
}
