# Database Service

## General

This service is able to receive **MQTT** messages from other services utilizing plugins.

## Configuration

As already mentioned, it is possible to configure the service. Please have a look [here][1].

[1]: ../../config_templates/database_service.toml
