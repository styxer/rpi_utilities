#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes
#include "databaseadapterinterface.h"

// common includes

// system includes
#include <memory>
#include <pqxx/connection>
#include <pqxx/pqxx>

namespace rpi_utilities::database {

class DatabaseAdapter : public DatabaseAdapterInterface
{
public:
  DatabaseAdapter(const DatabaseAdapter &) = delete;
  DatabaseAdapter(DatabaseAdapter &&) = delete;
  DatabaseAdapter& operator=(const DatabaseAdapter &) = delete;
  DatabaseAdapter& operator=(DatabaseAdapter &&) = delete;
  explicit DatabaseAdapter(std::string database_address,
                           std::string database_name,
                           std::string database_user,
                           std::string database_password);
  ~DatabaseAdapter() override = default;

  bool connect() override;
  void disconnect() override;

  bool createTable(const std::string &table_name,
                   const std::string &column_names) override;

  bool insertValue(const std::string &table_name,
                   const std::string &column_names,
                   const std::string &column_data) override;

private:
  const std::string m_database_address;
  const std::string m_database_name;
  const std::string m_database_user;
  const std::string m_database_password;
  std::unique_ptr<pqxx::connection> m_database_connection = nullptr;

  bool executeQuery(const std::string &query_string);
};

} // namespace rpi_utilities::database
