/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: database
*
************************************************/

// local includes
#include "groceryplugin.h"
#include "databaseadapter.h"

// common includes
#include "grocery_entry.pb.h"
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(GroceryPlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void GroceryPlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                               std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &key_value_pair : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", key_value_pair.name);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool GroceryPlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Can't insert value if plugin is not initialized");
    return false;
  }

  try
  {
    const auto grocery_entry = parseProtobufMessage<message::GroceryEntry>(data, size);
    const std::string query_string =
      std::format("{}, {}, {}, '{}', {}", grocery_entry.day(), grocery_entry.month(), grocery_entry.year(),
                  grocery_entry.shop_name(), grocery_entry.amount());

    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config = getPluginConfig();
    const DatabaseServiceEngineConfig::Plugin::Tables &tables = plugin_config->tables;

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface
      = getDatabaseAdapterInterface();

    bool ret = database_adapter_interface->insertValue(tables.front().name,
                                                       "day, month, year, shop_name, amount",
                                                       query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
    else
    {
      logging_handler::debug(FUNC_PREAMBLE, "Inserted:\n\n{}", grocery_entry.DebugString());
    }
  }
  catch(const std::exception &e)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool GroceryPlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
