#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: database
*
************************************************/

// local includes
#include "databaseplugininterface.h"
#include "databaseadapterinterface.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

class SmartMeterServicePlugin : public DatabasePluginInterface
{
public:
  explicit SmartMeterServicePlugin() = default;
  SmartMeterServicePlugin(const SmartMeterServicePlugin &) = delete;
  SmartMeterServicePlugin(SmartMeterServicePlugin &&) = delete;
  SmartMeterServicePlugin &operator=(const SmartMeterServicePlugin &) = delete;
  SmartMeterServicePlugin &operator=(SmartMeterServicePlugin &&) = delete;
  ~SmartMeterServicePlugin() override = default;

  //! \see interface
  void initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                  std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config) override;

  //! \see interface
  bool insertValue(const char *data, size_t size) const override;

  //! \see interface
  [[nodiscard]] bool isInitialized() const override;
};

} // namespace rpi_utilities::database
