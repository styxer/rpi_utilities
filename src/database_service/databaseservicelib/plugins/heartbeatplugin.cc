/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: database
*
************************************************/

// local includes
#include "heartbeatplugin.h"

// common includes
#include "heartbeat.pb.h"
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(HeartbeatPlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void HeartbeatPlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                                 std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &key_value_pair : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", key_value_pair.name);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool HeartbeatPlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    return false;
  }

  try
  {
    const auto heartbeat = parseProtobufMessage<message::Heartbeat>(data, size);
    const std::string query_string = std::format(
      "{}, '{}'", heartbeat.heartbeat_timestamp(), heartbeat.node_id());

    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config = getPluginConfig();
    const DatabaseServiceEngineConfig::Plugin::Tables &tables = plugin_config->tables;

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface
      = getDatabaseAdapterInterface();

    bool ret = database_adapter_interface->insertValue(tables.front().name,
                                                       "timestamp, node_id",
                                                       query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
  }
  catch(const std::exception &e)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool HeartbeatPlugin::isInitialized() const
{
    return getIsInitialized();
}

} // namespace rpi_utilities
