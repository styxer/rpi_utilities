#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: databasese
*
************************************************/

// local includes
#include "databaseplugininterface.h"
#include "databaseadapterinterface.h"

// common includes

// system includes
#include <memory>

//------------------------------------------------------------------------------

namespace rpi_utilities::database {

class CpuTemperaturePlugin : public DatabasePluginInterface
{

public:
  explicit CpuTemperaturePlugin() = default;
  CpuTemperaturePlugin(const CpuTemperaturePlugin &) = delete;
  CpuTemperaturePlugin(CpuTemperaturePlugin &&) = delete;
  CpuTemperaturePlugin &operator=(const CpuTemperaturePlugin &) = delete;
  CpuTemperaturePlugin &operator=(CpuTemperaturePlugin &&) = delete;
  ~CpuTemperaturePlugin() override = default;

  //! \see interface
  void initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                  std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config) override;

  //! \see interface
  bool insertValue(const char *data, size_t size) const override;

  //! \see interface
  [[nodiscard]] bool isInitialized() const override;
};

} // namespace rpi_utilities::database
