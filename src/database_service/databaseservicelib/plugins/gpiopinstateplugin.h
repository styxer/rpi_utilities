#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: database
*
************************************************/

// local includes
#include "databaseplugininterface.h"
#include "databaseadapterinterface.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

class GpioPinStatePlugin : public DatabasePluginInterface
{
public:
  explicit GpioPinStatePlugin () = default;
  GpioPinStatePlugin(const GpioPinStatePlugin &) = delete;
  GpioPinStatePlugin(GpioPinStatePlugin &&) = delete;
  GpioPinStatePlugin &operator=(const GpioPinStatePlugin &) = delete;
  GpioPinStatePlugin &operator=(GpioPinStatePlugin &&) = delete;
  ~GpioPinStatePlugin() override = default;

  //! \see interface
  void initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                  std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config) override;

  //! \see interface
  bool insertValue(const char *data, size_t size) const override;

  //! \see interface
  [[nodiscard]] bool isInitialized() const override;
};

} // namespace rpi_utilities::database
