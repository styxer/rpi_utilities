#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2024
*
* Module: database
*
************************************************/

// local includes
#include "databaseplugininterface.h"
#include "databaseadapterinterface.h"

// common includes
#include "nlohmann/json.hpp"

// system includes
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

class ShellyPlusPlugSPlugin : public DatabasePluginInterface
{
public:
  explicit ShellyPlusPlugSPlugin() = default;
  ShellyPlusPlugSPlugin(const ShellyPlusPlugSPlugin &) = delete;
  ShellyPlusPlugSPlugin(ShellyPlusPlugSPlugin &&) = delete;
  ShellyPlusPlugSPlugin &operator=(const ShellyPlusPlugSPlugin &) = delete;
  ShellyPlusPlugSPlugin &operator=(ShellyPlusPlugSPlugin &&) = delete;
  ~ShellyPlusPlugSPlugin() override = default;

  //! \see interface
  void initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                  std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config) override;

  //! \see interface
  bool insertValue(const char *data, size_t size) const override;

  //! \see interface
  [[nodiscard]] bool isInitialized() const override;

private:
  struct ActiveEnergy
  {
    double total = 0.0;
    std::vector<double> by_minute;
    int minute_ts = 0;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(ActiveEnergy, total, by_minute, minute_ts);
  };

  struct Temperature
  {
    double tC = 0.0;
    double tF = 0.0;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(Temperature, tC, tF);
  };

  struct SwitchStatus
  {
    int id = 0;
    std::string source;
    bool output = false;
    double apower = 0.0;
    double voltage = 0.0;
    double current = 0.0;
    ActiveEnergy aenergy;
    Temperature temperature;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(SwitchStatus, id, source, output, apower,
                                   voltage, current, aenergy, temperature);
  };
};

} // namespace rpi_utilities::database
