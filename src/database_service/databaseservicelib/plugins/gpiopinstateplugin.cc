/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: database
*
************************************************/

// local includes
#include "gpiopinstateplugin.h"

// common includes
#include "gpiopinstate.pb.h"
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(GpioPinStatePlugin, PluginAllocator, PluginDeallocator) // NOLINT

} // rpi_utilities::database

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void GpioPinStatePlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                                    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &key_value_pair : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", key_value_pair.name);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool GpioPinStatePlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    logging_handler::warning(FUNC_PREAMBLE,
                             "Can't insert value if not initialized");

    return false;
  }

  try
  {
    const auto gpio_pin_state = parseProtobufMessage<message::GpioPinState>(data, size);
    const std::string query_string = std::format("{}, {}, {}, '{}'",
                                                 gpio_pin_state.pin_number(),
                                                 gpio_pin_state.pin_state(),
                                                 gpio_pin_state.timestamp(),
                                                 gpio_pin_state.node_id());

    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config = getPluginConfig();
    const DatabaseServiceEngineConfig::Plugin::Tables &tables = plugin_config->tables;

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface = getDatabaseAdapterInterface();
    bool ret = database_adapter_interface->insertValue(tables.front().name,
                                                       "pin_number, pin_state, timestamp, node_id",
                                                       query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
    else
    {
      logging_handler::debug(FUNC_PREAMBLE, "Inserted:\n\n{}", gpio_pin_state.DebugString());
    }
  }
  catch(const std::exception& e)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool GpioPinStatePlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
