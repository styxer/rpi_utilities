/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2024
*
* Module: database
*
************************************************/

// local includes
#include "shellyplusplugsplugin.h"
#include "databaseadapter.h"

// common includes
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <format>
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(ShellyPlusPlugSPlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void ShellyPlusPlugSPlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                                       std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  /*
  for (const auto &key_value_pair : plugin_config.tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", key_value_pair.name);
    }
  }
  */

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool ShellyPlusPlugSPlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    return false;
  }

  try
  {
    logging_handler::debug(FUNC_PREAMBLE, "LOL: {} {}", data, size);

    const auto switch_status = nlohmann::json::parse(data).get<SwitchStatus>();
    const std::string query_string = std::vformat(
        "{}, {}, '{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}",
        std::make_format_args(
          std::chrono::system_clock::now().time_since_epoch().count(),
          switch_status.id,
          switch_status.source,
          switch_status.apower,
          switch_status.voltage,
          switch_status.current,
          switch_status.aenergy.total,
          switch_status.aenergy.by_minute.at(0),
          switch_status.aenergy.by_minute.at(1),
          switch_status.aenergy.by_minute.at(2),
          switch_status.aenergy.minute_ts,
          switch_status.temperature.tC,
          switch_status.temperature.tF
        ));

    // const DatabaseServiceEngineConfig::Plugin &plugin_config = getPluginConfig();
    // const DatabaseServiceEngineConfig::Plugin::Tables &tables = plugin_config.tables;

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface
      = getDatabaseAdapterInterface();

    /*
    bool ret = database_adapter_interface->insertValue(
        tables.front().name,
        "timestamp, switch_id, source, output, apower, voltage, current, aenergy_total, "
        "aenergy_minute_0, aenergy_minute_1, aenergy_minute_2, "
        "aenergy_minute_ts, temperature_celcius, temperature_fahrenheit",
        query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
    */

    logging_handler::debug(FUNC_PREAMBLE, query_string);
  }
  catch(const std::exception& e)
  {
    logging_handler::warning(FUNC_PREAMBLE, e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

[[nodiscard]] bool ShellyPlusPlugSPlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
