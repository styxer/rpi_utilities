#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: database
*
************************************************/

// local includes
#include "databaseserviceengineconfig.h"

// common includes

// system includes
#include <memory>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

class DatabaseAdapterInterface;

} // namespace rpi_utilities::database

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

class DatabasePluginInterface
{
public:
  DatabasePluginInterface(const DatabasePluginInterface &) = delete;
  DatabasePluginInterface(DatabasePluginInterface &&) = delete;
  DatabasePluginInterface &operator=(const DatabasePluginInterface &) = delete;
  DatabasePluginInterface &operator=(DatabasePluginInterface &&) = delete;

  explicit DatabasePluginInterface() = default;
  virtual ~DatabasePluginInterface() = default;

  //! This method is used to initialize all needed members.
  virtual void initialize(std::shared_ptr<DatabaseAdapterInterface>  database_adapter,
                          std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config) = 0;

  virtual bool insertValue(const char *data, size_t size) const = 0;

  //! Indicates if the database connection could be setup.
  //! \return True if everything is fine. False otherwise.
  [[nodiscard]] virtual bool isInitialized() const = 0;

  GET_VAL(DatabaseAdapterInterface, std::shared_ptr<DatabaseAdapterInterface>, m_database_adapter);
  GET_VAL(PluginConfig, std::shared_ptr<DatabaseServiceEngineConfig::Plugin>, m_plugin_config);
  GET_VAL(IsInitialized, bool, m_initialized);

  SET_REF(DatabaseAdapterInterface, std::shared_ptr<DatabaseAdapterInterface>, m_database_adapter);
  SET_REF(PluginConfig, std::shared_ptr<DatabaseServiceEngineConfig::Plugin>, m_plugin_config);
  SET_VAL(IsInitialized, bool, m_initialized);

private:
  std::shared_ptr<DatabaseAdapterInterface> m_database_adapter;
  std::shared_ptr<DatabaseServiceEngineConfig::Plugin> m_plugin_config;

  bool m_initialized = true;
};

} // namespace rpi_utilities::databse
