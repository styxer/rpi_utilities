/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: database
*
************************************************/

// local includes
#include "cputemperatureplugin.h"

// common includes
#include "rpimacros.h"
#include "rpiutilities.h"
#include "cputemperature.pb.h"
#include "logginghelper.h"

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::database {

DEFINE_PLUGIN(CpuTemperaturePlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------


namespace rpi_utilities::database
{

void CpuTemperaturePlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                                      std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &key_value_pair : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", key_value_pair.name);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool CpuTemperaturePlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    return false;
  }

  try
  {
    const auto cpu_temperature
      = parseProtobufMessage<message::CpuTemperature>(data, size);

    const std::string query_string = std::to_string(cpu_temperature.measurement_timestamp()) +
                                     ", '" +
                                     cpu_temperature.node_id() +
                                     "', " +
                                     std::to_string(cpu_temperature.cpu_temperature());

    const DatabaseServiceEngineConfig::Plugin::Tables &tables = getPluginConfig()->tables;
    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface =
        getDatabaseAdapterInterface();

    bool ret = database_adapter_interface->insertValue(tables.front().name,
                                                       "measurement_timestamp, node_id, cpu_temperature",
                                                       query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
    else
    {
      logging_handler::debug(FUNC_PREAMBLE, "Inserted:\n\n{}", cpu_temperature.DebugString());
    }
  }
  catch(const std::exception& e)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", e.what());

    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool CpuTemperaturePlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
