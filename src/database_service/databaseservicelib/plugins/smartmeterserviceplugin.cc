/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: database
*
************************************************/

// local includes
#include "smartmeterserviceplugin.h"

// common includes
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"
#include "scrape_service.pb.h"

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(SmartMeterServicePlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void SmartMeterServicePlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                                         std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &key_value_pair : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE,
                                "Could not create table '{}' with columns '{}'",
                                key_value_pair.name,
                                key_value_pair.columns);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool SmartMeterServicePlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Can't insert value if plugin is not initialized");
    return false;
  }

  try
  {
    const auto scrape_service_entry
      = parseProtobufMessage<message::ScrapeService>(data, size);

    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config = getPluginConfig();

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface
      = getDatabaseAdapterInterface();

    switch (scrape_service_entry.PluginData_case())
    {
      case message::ScrapeService::kStromnetzegraz:
      {
        const message::StromnetzeGraz &stromnetze_graz = scrape_service_entry.stromnetzegraz();
        std::string field_name = rpi_utilities::message::StromnetzeGraz::GetDescriptor()->name();
        transform(field_name.begin(), field_name.end(), field_name.begin(), ::tolower);

        auto it = std::find_if(
            plugin_config->tables.begin(), plugin_config->tables.end(),
            [&field_name](const DatabaseServiceEngineConfig::Table &table) {
              return table.name == field_name;
            });

        if (it == plugin_config->tables.end())
        {
          logging_handler::warning(FUNC_PREAMBLE, "Could not find a suitable table for '{}'", field_name);
          break;
        }

        for (const auto &meter_reading : stromnetze_graz.meter_readings())
        {
          for (const auto &reading : meter_reading.second.reading())
          {
            for (const auto &reading_value : reading.reading_value())
            {
              const std::string query_string =
                std::format("'{}', {}, {}, '{}', '{}', '{}', '{}', '{}', {}, {}",
                            scrape_service_entry.node_id(),
                            meter_reading.first,
                            reading.read_time(),
                            meter_reading.second.interval_type(),
                            reading_value.scale(),
                            reading_value.reading_state(),
                            reading_value.reading_type(),
                            reading_value.unit(),
                            reading_value.value(),
                            stromnetze_graz.timestamp());

              bool ret = database_adapter_interface->insertValue(it->name,
                                                                 "node_id, "
                                                                 "meter_id, "
                                                                 "read_time, "
                                                                 "interval_type, "
                                                                 "scale, "
                                                                 "reading_state, "
                                                                 "reading_type, "
                                                                 "unit, "
                                                                 "value, "
                                                                 "scrape_timestamp",
                                                                 query_string);

              if (!ret)
              {
                logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into StromNetzeGraz DB!");
              }
            }
          }
        }

        logging_handler::debug(FUNC_PREAMBLE, "Received: {}", scrape_service_entry.DebugString());
        break;
      }
      default:
        break;
    }
  }
  catch(const std::exception &e)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool SmartMeterServicePlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
