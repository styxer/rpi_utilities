/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: database
*
************************************************/

// local includes
#include "latencyplugin.h"

// common includes
#include "latency.pb.h"
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(LatencyPlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void LatencyPlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                               std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &key_value_pair : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(key_value_pair.name,
                                                 key_value_pair.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", key_value_pair.name);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool LatencyPlugin::insertValue(const char *data, size_t size) const
{
  if (!isInitialized())
  {
    return false;
  }

  try
  {
    const auto latency = parseProtobufMessage<message::Latency>(data, size);
    const std::string query_string = std::format(
      "{}, '{}', {}, '{}'", latency.measurement_timestamp(), latency.node_id(),
      latency.latency(), latency.endpoint_name());

    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config = getPluginConfig();
    const DatabaseServiceEngineConfig::Plugin::Tables &tables = plugin_config->tables;

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface
      = getDatabaseAdapterInterface();

    bool ret = database_adapter_interface->insertValue(tables.front().name,
                                                       "timestamp, node_id, latency, endpoint_name",
                                                       query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
    else
    {
      logging_handler::debug(FUNC_PREAMBLE, "Inserted:\n\n{}", latency.DebugString());
    }
  }
  catch(const std::exception& e)
  {
    logging_handler::warning(FUNC_PREAMBLE, e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool LatencyPlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
