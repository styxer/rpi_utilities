/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: database
*
************************************************/

// local includes
#include "wemosplugin.h"

// common includes
#include "logginghelper.h"
#include "rpimacros.h"

// system includes
#include <sstream>
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

DEFINE_PLUGIN(WemosPlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::database
{

void WemosPlugin::initialize(std::shared_ptr<DatabaseAdapterInterface> database_adapter,
                             std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config)
{
  setDatabaseAdapterInterface(database_adapter);
  setPluginConfig(plugin_config);

  bool initialized = true;
  for (const auto &kv : plugin_config->tables)
  {
    initialized &= database_adapter->createTable(kv.name,
                                                 kv.columns);

    if (!initialized)
    {
      logging_handler::critical(FUNC_PREAMBLE, "Could not create table '{}'", kv.name);
    }
  }

  setIsInitialized(initialized);
}

//------------------------------------------------------------------------------

bool WemosPlugin::insertValue(const char *data, size_t) const
{
  if (!isInitialized())
  {
    return false;
  }

  try
  {
    const std::string wemos_message {data};

    std::istringstream token_stream{wemos_message};
    const char delimiter = ';';

    std::string token;
    std::vector<std::string> tokens;
    while (std::getline(token_stream, token, delimiter))
    {
      tokens.push_back(token);
    }

    const std::string query_string = std::format("{}, '{}', {}", tokens[1],  tokens[0], tokens[2]);

    std::shared_ptr<DatabaseServiceEngineConfig::Plugin> plugin_config = getPluginConfig();
    const DatabaseServiceEngineConfig::Plugin::Tables &tables = plugin_config->tables;

    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface
      = getDatabaseAdapterInterface();

    bool ret = database_adapter_interface->insertValue(tables.front().name,
                                                       "timestamp, node_id, switch_state",
                                                       query_string);

    if (!ret)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not insert value into DB!");
    }
    else
    {
      logging_handler::debug(FUNC_PREAMBLE, "Inserted:\n\n{}", wemos_message);
    }
  }
  catch(const std::exception& e)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", e.what());
    return false;
  }

  return true;
}

//------------------------------------------------------------------------------

bool WemosPlugin::isInitialized() const
{
  return getIsInitialized();
}

} // namespace rpi_utilities::database
