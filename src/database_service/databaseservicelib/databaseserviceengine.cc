/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes
#include "databaseserviceengine.h"

// common includes
#include "logginghelper.h"
#include "mqttmessageadapter.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <chrono>
#include <fstream>
#include <mutex>
#include <span>

namespace rpi_utilities::database
{

DatabaseServiceEngine::DatabaseServiceEngine(std::unique_ptr<ExternalModules> external_modules)
    : m_is_running{false},
      m_external_modules{std::move(external_modules)},
      m_mqtt_adapter_interface{std::make_unique<mqttmessage::MqttMessageAdapter>(
          [this](const std::string &topic, const char *data, size_t size) {
            slotMessageReceived(topic, data, size);
          },
          [this](bool connected) {
            slotConnectionStatusChanged(connected);
          },
          nullptr,
          m_external_modules->config->getMqttBrokerAddress(),
          m_external_modules->config->getMqttClientId()
        )
      }
{
}

//------------------------------------------------------------------------------

void DatabaseServiceEngine::start()
{
  if (m_is_running)
  {
    logging_handler::critical(FUNC_PREAMBLE, "Engine is already running!");
    return;
  }

  if (m_external_modules->database_adapter_interface == nullptr)
  {
    logging_handler::critical(FUNC_PREAMBLE, "Database adapter interface is not initialized!");
    return;
  }

  m_is_running = true;

  m_mqtt_adapter_interface->connect(m_external_modules->config->getMqttUser(),
                                    m_external_modules->config->getMqttPassword());
}

//------------------------------------------------------------------------------

void DatabaseServiceEngine::stop()
{
  m_is_running = false;
}

//------------------------------------------------------------------------------

void DatabaseServiceEngine::slotMessageReceived(const std::string &topic,
                                                const char *payload,
                                                size_t size)
{
  std::lock_guard<std::mutex> guard{m_message_received_mutex};
  const std::span<const char> payload_span {payload, size};
  m_received_payloads.emplace_back(topic, std::vector<char>(payload_span.begin(), payload_span.end()), size);

  m_condition_variable.notify_all();
}

//------------------------------------------------------------------------------

void DatabaseServiceEngine::slotConnectionStatusChanged(bool connected)
{
  if (!connected)
  {
    logging_handler::info(FUNC_PREAMBLE, "Connection lost. Trying to reconnect!");

    m_is_running = false;

    m_external_modules->database_adapter_interface->disconnect();
    m_condition_variable.notify_all();
    m_future.wait();
    m_plugins.clear();

    return;
  }

  logging_handler::info(FUNC_PREAMBLE, "Connected to broker!");

  logging_handler::info(FUNC_PREAMBLE, "Trying to connect to DB!");
  m_is_running = m_external_modules->database_adapter_interface->connect();

  while (!m_is_running)
  {
    logging_handler::critical(FUNC_PREAMBLE, "Could not connect to database!");

    m_is_running = m_external_modules->database_adapter_interface->connect();
  }

  logging_handler::info(FUNC_PREAMBLE, "Connected to DB!");

  const std::set<std::string> &mqtt_topics = m_external_modules->config->getMqttTopicIds();
  logging_handler::info(FUNC_PREAMBLE, "Subscribing to topics: [ {:n} ]", mqtt_topics);

  for (const auto &mqtt_topic : mqtt_topics)
  {
    m_mqtt_adapter_interface->subscribe(
      mqtt_topic,
      mqttmessage::QualityOfServiceMode::kExactlyOnce
    );
  }

  const DatabaseServiceEngineConfig::PluginHash plugins = m_external_modules->config->getPlugins();
  for (const auto &key_value_pair : plugins)
  {
    std::shared_ptr<DatabasePluginInterface> plugin
      = loadPlugin<DatabasePluginInterface>(key_value_pair.first);

    if (!plugin)
    {
      continue;
    }

    plugin->initialize(m_external_modules->database_adapter_interface, key_value_pair.second);

    if (!plugin->isInitialized())
    {
      logging_handler::warning(FUNC_PREAMBLE, "Could not initialize plugin '{}", key_value_pair.first);
      continue;
    }

    logging_handler::info(FUNC_PREAMBLE, "Loaded plugin: {}", key_value_pair.first);

    m_plugins.insert({key_value_pair.second->topic, plugin});
  }

  m_future = std::async(std::launch::async, [this]() { return processReceivedPayloads(); });
}

//------------------------------------------------------------------------------

void DatabaseServiceEngine::processReceivedPayloads()
{
  while (true)
  {
    std::unique_lock lock{m_message_received_mutex};
    m_condition_variable.wait(lock, [this]() { return !m_received_payloads.empty() || !m_is_running; });

    const std::tuple<std::string, std::vector<char>, size_t> received_payload = m_received_payloads.back();
    m_received_payloads.pop_back();

    const std::string topic = std::get<0>(received_payload);

    auto plugins_it = m_plugins.find(topic);
    if (plugins_it == m_plugins.end())
    {
      logging_handler::warning(FUNC_PREAMBLE, "No plugin registered for topic: {}. Ignoring!", topic);
      continue;
    }

    plugins_it->second->insertValue(std::get<1>(received_payload).data(), std::get<2>(received_payload));

    if (!m_is_running)
    {
      break;
    }
  }
}

} // namespace rpi_utilities::database
