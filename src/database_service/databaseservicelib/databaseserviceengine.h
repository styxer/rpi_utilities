#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes
#include "databaseserviceengineinterface.h"
#include "databaseserviceengineconfig.h"
#include "databaseadapterinterface.h"
#include "plugins/databaseplugininterface.h"

// common includes
#include "mqttmessageadapterinterface.h"

// system includes
#include <future>
#include <map>
#include <memory>
#include <mutex>
#include <string>

namespace rpi_utilities::database {

class DatabaseServiceEngine : public DatabaseServiceEngineInterface
{
public:
  struct ExternalModules
  {
    std::unique_ptr<DatabaseServiceEngineConfig> config;
    std::shared_ptr<DatabaseAdapterInterface> database_adapter_interface;
  };

  DatabaseServiceEngine(const DatabaseServiceEngine &) = delete;
  DatabaseServiceEngine(DatabaseServiceEngine &&) = delete;
  DatabaseServiceEngine& operator=(const DatabaseServiceEngine &) = delete;
  DatabaseServiceEngine& operator=(DatabaseServiceEngine &&) = delete;

  explicit DatabaseServiceEngine(std::unique_ptr<ExternalModules> external_modules);
  ~DatabaseServiceEngine() override = default;

  //! \see interface
  void start() override;

  //! \see interface
  void stop() override;

private:
  bool m_is_running;

  std::unique_ptr<ExternalModules> m_external_modules;
  std::map<std::string, std::shared_ptr<DatabasePluginInterface>> m_plugins;

  std::unique_ptr<mqttmessage::MqttMessageAdapterInterface> m_mqtt_adapter_interface = nullptr;

  std::mutex m_message_received_mutex;
  std::condition_variable m_condition_variable;

  std::vector<std::tuple<std::string, std::vector<char>, size_t>> m_received_payloads;

  std::future<void> m_future;

  //! This slot dispatches all received MQTT messages to the correct methods.
  //! \param topic   The topic which has been received.
  //! \param payload The payload which has been received.
  //! \param size    The size of the payload.
  void slotMessageReceived(const std::string &topic,
                           const char *payload,
                            size_t size);

  void slotConnectionStatusChanged(bool connected);

  void processReceivedPayloads();
};

} // namespace rpi_utilities::database
