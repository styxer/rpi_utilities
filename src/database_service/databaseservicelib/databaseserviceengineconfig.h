#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes

// common includes
#include "commonexception.h"
#include "configbase.h"
#include "rpimacros.h"

// system includes
#include <map>
#include <set>
#include <utility>

template<>
struct std::formatter<std::vector<std::string>, char>
{
  template<class ParseContext>
  constexpr ParseContext::iterator parse(ParseContext& ctx)
  {
    auto it = ctx.begin();
    if (it == ctx.end())
    {
      return it;
    }

    if (*it == '#')
    {
      ++it;
    }

    if (it != ctx.end() && *it != '}')
    {
      throw std::format_error("Invalid format args for std::vector<std::string>.");
    }

    return it;
  }

  template<class FmtContext>
  FmtContext::iterator format(std::vector<std::string> vec, FmtContext& ctx) const
  {
    std::ostringstream out;
    for (auto it = vec.begin(); it != vec.end(); ++it)
    {
        out << *it;

        if (it != vec.end() - 1)
        {
          out << ", ";
        }
    }

    return std::ranges::copy(std::move(out).str(), ctx.out()).out;
  }
};

namespace rpi_utilities::database {

class DatabaseServiceEngineConfig : public config::ConfigBase
{

public:
  struct Table
  {
    std::string name;
    std::string columns;
  };

  struct Plugin
  {
    using Tables = std::vector<Table>;

    explicit Plugin(const toml::table &table)
    {
      const std::optional<std::filesystem::path> local_path = table["path"].value<std::string>();
      const std::optional<std::string> local_topic = table["topic"].value<std::string>();
      const toml::array *local_tables = table["tables"].as_array();

      if (!local_path.has_value() || !local_topic.has_value() || local_tables == nullptr)
      {
        throw common::CommonException("Plugin config is missing 'path', 'topic' or 'tables' entry.");
      }

      path = *local_path;
      topic = *local_topic;
      local_tables->for_each([this](auto &&elem) {
          if constexpr(!toml::is_table<decltype(elem)>)
          {
            return;
          }

          const toml::table *table = elem.as_table();

          const std::optional<std::string> table_name = (*table)["name"].value<std::string>();
          const toml::array *table_columns = (*table)["columns"].as_array();

          if (!table_name.has_value() || table_columns == nullptr)
          {
            throw common::CommonException("Missing 'name' or 'columns' within plugin config entry.");
          }

          std::vector<std::string> columns;
          table_columns->for_each([&columns](auto &&elem) {
            if constexpr(!toml::is_table<decltype(elem)>)
            {
              return;
            }

            const toml::table *table = elem.as_table();

            const std::optional<std::string> column_name = (*table)["name"].value<std::string>().value();
            const std::optional<std::string> column_type = (*table)["type"].value<std::string>().value();
            if (!column_name.has_value() || !column_type.has_value())
            {
              throw common::CommonException("Missing 'name' or 'type' within plugin.tables.columns config.");
            }

            columns.push_back(std::format("{} {}", column_name.value(), column_type.value()));
          });

          tables.emplace_back(Table{*table_name, std::format("{}", columns)});
      });
    }

    Plugin(Plugin &&) = default;
    explicit Plugin(std::filesystem::path &&path_, std::string &&topic_, Tables &&tables_)
        : path{std::move(path_)}, topic{std::move(topic_)}, tables{std::move(tables_)} {}

    Plugin(const Plugin &) = default;
    Plugin& operator=(const Plugin &) = default;
    Plugin& operator=(Plugin &&) = default;
    ~Plugin() = default;

    std::filesystem::path path;
    std::string topic;
    Tables tables;
  };

  using PluginHash = std::map<std::string, std::shared_ptr<Plugin>>;

  explicit DatabaseServiceEngineConfig(const std::filesystem::path &config_path)
    : ConfigBase{config_path}
  {
    const std::optional<std::string> database_address = m_toml[kDatabaseSectionName][kDatabaseAddressName].value<std::string>();
    const std::optional<std::string> database_name = m_toml[kDatabaseSectionName][kDatabaseNameName].value<std::string>();
    const std::optional<std::string> database_user = m_toml[kDatabaseSectionName][kDatabaseUserName].value<std::string>();
    const std::optional<std::string> database_password = m_toml[kDatabaseSectionName][kDatabasePasswordName].value<std::string>();
    const toml::array *database_plugins = m_toml[kDatabaseSectionName][kDatabasePluginsName].as_array();

    if (!database_address.has_value() || !database_name.has_value() ||
        !database_user.has_value() || !database_password.has_value() ||
        database_plugins == nullptr)
    {
      throw common::CommonException(
        "Missing 'address', 'name', 'user', 'password', 'plugins' "
        "within 'database' section of configuration TOML.");
    }

    m_database_address = *database_address;
    m_database_name = *database_name;
    m_database_user = *database_user;
    m_database_password = *database_password;

    database_plugins->for_each([this](auto &&elem) {
        if constexpr(!toml::is_table<decltype(elem)>)
        {
          return;
        }

        const toml::table *table = elem.as_table();
        auto plugin_config = std::make_shared<Plugin>(*table);

        m_plugins.emplace(plugin_config->path, plugin_config);
    });
  }

  DatabaseServiceEngineConfig(const DatabaseServiceEngineConfig &) = default;
  DatabaseServiceEngineConfig(DatabaseServiceEngineConfig &&) = default;
  DatabaseServiceEngineConfig& operator=(const DatabaseServiceEngineConfig &) = default;
  DatabaseServiceEngineConfig& operator=(DatabaseServiceEngineConfig &&) = default;

  explicit DatabaseServiceEngineConfig(std::string &&mqtt_broker_address_,
                                       std::string &&mqtt_client_id_,
                                       std::string &&mqtt_user_,
                                       std::string &&mqtt_password_,
                                       std::filesystem::path &&log_directory_,
                                       std::string &&database_address_,
                                       std::string &&database_name_,
                                       std::string &&database_user_,
                                       std::string &&database_password_,
                                       PluginHash  &&plugins_)
      : config::ConfigBase {
          std::move(mqtt_broker_address_),
          std::move(mqtt_client_id_),
          std::move(mqtt_user_),
          std::move(mqtt_password_),
          {},
          {},
          {},
          std::move(log_directory_),
          {},
        },
        m_database_address{std::move(database_address_)},
        m_database_name{std::move(database_name_)},
        m_database_user{std::move(database_user_)},
        m_database_password{std::move(database_password_)},
        m_plugins{std::move(plugins_)}
  {
  }
  ~DatabaseServiceEngineConfig() override = default;

  GET_CONST_REF(DatabaseAddress,  std::string, m_database_address)
  GET_CONST_REF(DatabaseName,     std::string, m_database_name)
  GET_CONST_REF(DatabaseUser,     std::string, m_database_user)
  GET_CONST_REF(DatabasePassword, std::string, m_database_password)
  GET_CONST_REF(Plugins,          PluginHash,  m_plugins)

private:
  static constexpr const char *kDatabaseSectionName = "database";
  static constexpr const char *kDatabaseAddressName = "address";
  static constexpr const char *kDatabaseNameName = "name";
  static constexpr const char *kDatabaseUserName = "user";
  static constexpr const char *kDatabasePasswordName = "password";
  static constexpr const char *kDatabasePluginsName = "plugins";

  std::string m_database_address;
  std::string m_database_name;
  std::string m_database_user;
  std::string m_database_password;
  PluginHash  m_plugins;
};

} // namespace rpi_utilities::database
