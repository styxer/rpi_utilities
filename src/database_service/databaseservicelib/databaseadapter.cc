/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes
#include "databaseadapter.h"

// common includes
#include "rpimacros.h"
#include "logginghelper.h"

// system includes
#include <memory>
#include <string>
#include <pqxx/transaction>

namespace rpi_utilities::database
{

DatabaseAdapter::DatabaseAdapter(std::string database_address,
                                 std::string database_name,
                                 std::string database_user,
                                 std::string database_password)
    : m_database_address{std::move(database_address)},
      m_database_name{std::move(database_name)},
      m_database_user{std::move(database_user)},
      m_database_password{std::move(database_password)}
{
}

//------------------------------------------------------------------------------

bool DatabaseAdapter::connect()
{
  if (m_database_connection != nullptr && m_database_connection->is_open())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Database is already opened. Can't open twice!");
    return false;
  }

  try
  {
    const std::string connection_options
      = "user=" + m_database_user +
        " password=" + m_database_password +
        " host=" + m_database_address +
        " port=5432 " +
        " dbname=" + m_database_name;

    m_database_connection
      = std::make_unique<pqxx::connection>(connection_options);
  }
  catch (const std::exception &ex)
  {
    logging_handler::error(FUNC_PREAMBLE, "Error while connecting to DB: '{}'", ex.what());
    return false;
  }

  return m_database_connection->is_open();
}

//------------------------------------------------------------------------------

void DatabaseAdapter::disconnect()
{
  m_database_connection->close();
}

//------------------------------------------------------------------------------

bool DatabaseAdapter::createTable(const std::string &table_name,
                                  const std::string &column_names)
{
  return executeQuery("CREATE TABLE IF NOT EXISTS " +
                      table_name +
                      " (" + column_names + ")");
}

//------------------------------------------------------------------------------

bool DatabaseAdapter::insertValue(const std::string &table_name,
                                  const std::string &column_names,
                                  const std::string &column_data)
{
  return executeQuery("INSERT INTO " +
                      table_name  +
                      " (" + column_names + ") " +
                      " VALUES (" +
                      column_data + ")");
}

//------------------------------------------------------------------------------

bool DatabaseAdapter::executeQuery(const std::string &query_string)
{
  if (m_database_connection == nullptr)
  {
    logging_handler::error(FUNC_PREAMBLE, "Can't execute query without DB connection!");
    return false;
  }

  try
  {
    pqxx::work transaction{*m_database_connection};
    transaction.exec(query_string);
    transaction.commit();
  }
  catch (const std::exception &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE,
                             "Could not execute query '{}': '{}'",
                             query_string,
                             ex.what());

    return false;
  }

  return true;
}

} // namespace rpi_utilities
