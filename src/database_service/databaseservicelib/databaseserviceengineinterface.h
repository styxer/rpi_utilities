#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes

// common includes

// system includes

namespace rpi_utilities::database {

class DatabaseServiceEngineInterface
{
public:
  explicit DatabaseServiceEngineInterface() = default;
  DatabaseServiceEngineInterface(const DatabaseServiceEngineInterface &) = delete;
  DatabaseServiceEngineInterface(DatabaseServiceEngineInterface &&) = delete;
  DatabaseServiceEngineInterface& operator=(const DatabaseServiceEngineInterface &) = delete;
  DatabaseServiceEngineInterface& operator=(DatabaseServiceEngineInterface &&) = delete;
  virtual ~DatabaseServiceEngineInterface() = default;

  //! This method starts the engine.
  virtual void start() = 0;

  //! This method stops the engine.
  virtual void stop() = 0;
};

} // namespace rpi_utilities::database
