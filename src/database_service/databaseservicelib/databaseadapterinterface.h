#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: database
*
************************************************/

// local includes

// common includes

// system includes
#include <string>

namespace rpi_utilities::database {

class DatabaseAdapterInterface
{

public:
  DatabaseAdapterInterface() = default;
  DatabaseAdapterInterface(const DatabaseAdapterInterface &) = delete;
  DatabaseAdapterInterface(DatabaseAdapterInterface &&) = delete;
  DatabaseAdapterInterface& operator=(const DatabaseAdapterInterface &) = default;
  DatabaseAdapterInterface& operator=(DatabaseAdapterInterface &&) = delete;
  virtual ~DatabaseAdapterInterface() = default;

  virtual bool connect() = 0;
  virtual void disconnect() = 0;

  virtual bool createTable(const std::string &table_name,
                           const std::string &column_names) = 0;

  virtual bool insertValue(const std::string &table_name,
                           const std::string &column_names,
                           const std::string &column_data) = 0;
};

} // namespace rpi_utilities::database
