# Scrape Service

## General

This service requests data from various smart meter APIs. Each API needs to be implemented via a
plugin. Currently there are plugins for the following APIs:

* Stromnetz Graz

## Configuration

As already mentioned, it is possible to configure the service. Please have a look [here][1].

[1]: ../../config_templates/scrape_service.toml
