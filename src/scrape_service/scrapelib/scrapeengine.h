#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape_service
*
************************************************/

// local includes
#include "scrapeengineinterface.h"
#include "scrapeengineconfig.h"

// common includes
#include "heartbeat.h"
#include "scrape_service.pb.h"

// system includes
#include <future>
#include <memory>
#include <mutex>
#include <vector>

//-----------------------------------------------------------------------------

namespace rpi_utilities
{
  namespace scrape
  {
    class PluginInterface;
  }

  namespace http_request
  {
    class HttpRequestAdapter;
  }
}

namespace mqttmessage
{
  class MqttMessageAdapterInterface;
}

//-----------------------------------------------------------------------------

namespace rpi_utilities::scrape {

class ScrapeEngine : public ScrapeEngineInterface
{
  public:
    struct ExternalModules
    {
      std::shared_ptr<http_request::HttpRequestAdapter> http_request_adapter;
      std::unique_ptr<ScrapeEngineConfig> config;
    };

    ScrapeEngine(const ScrapeEngine &) = delete;
    ScrapeEngine(ScrapeEngine &&) = delete;
    ScrapeEngine &operator=(const ScrapeEngine &) = delete;
    ScrapeEngine &operator=(ScrapeEngine &&) = delete;

    //! \param external_modules Struct which stores all external objects passed during startup.
    explicit ScrapeEngine(std::unique_ptr<ExternalModules> external_modules);
    ~ScrapeEngine() override = default;

    //! \see interface
    void start() override;

    //! \see interface
    void stop() override;

  private:
    std::unique_ptr<ExternalModules> m_external_modules;

    std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> m_mqtt_message_adapter_interface;
    common::HeartBeat m_heartbeat_timer;
    std::vector<std::pair<std::shared_ptr<PluginInterface>, std::future<void>>> m_plugins;

    bool m_heartbeat_published = false;
    std::mutex m_mqtt_publish_mutex;

    std::mutex m_mqtt_published_mutex;
    std::condition_variable m_mqtt_published_condition_variable;

    //! Callback which will be called if the connection station of the MQTT adapter changed.
    //! \param True if connected to the MQTT broker. False otherwise.
    void connectionStatusChanged(bool connected);

    //! Callback which will be triggered by the plugins.
    //! \param proto Holds the protobuf messages which shall be sent.
    bool publishProtobufMessage(std::vector<char> &serialized_proto,
                                message::ScrapeService proto);

    void publishProtobufCompleted(bool completed, const std::vector<std::string> &topics);
};

} // namespace rpi_utilities::scrape
