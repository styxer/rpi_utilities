/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape_service
*
************************************************/

// local includes

// common includes

// system includes

// forward declarations

//------------------------------------------------------------------------------

namespace rpi_utilities::scrape {

//! This class manages all necessary steps to get smart meter measurements from different platforms.
class ScrapeEngineInterface
{
public:
  ScrapeEngineInterface(const ScrapeEngineInterface &) = delete;
  ScrapeEngineInterface(ScrapeEngineInterface &&) = delete;
  ScrapeEngineInterface &operator=(const ScrapeEngineInterface &) = delete;
  ScrapeEngineInterface &operator=(ScrapeEngineInterface &&) = delete;

  ScrapeEngineInterface() = default;
  virtual ~ScrapeEngineInterface() = default;

  //! Start the while engine.
  virtual void start() = 0;

  //! Stops the whole engine. This is only possible, if the engine has already been started.
  virtual void stop() = 0;
};

} // namespace rpi_utilities::scrape
