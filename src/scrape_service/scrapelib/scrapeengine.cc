/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape_service
*
************************************************/

// local includes
#include "logginghelper.h"
#include "mqttmessageadapter.h"
#include "scrapeengine.h"
#include "plugins/plugininterface.h"

// common includes
#include "commonexception.h"
#include "rpiutilities.h"

// system includes

//-----------------------------------------------------------------------------

namespace rpi_utilities::scrape {

ScrapeEngine::ScrapeEngine(std::unique_ptr<ExternalModules> external_modules)
  : m_external_modules{std::move(external_modules)},
    m_mqtt_message_adapter_interface{
      std::make_shared<mqttmessage::MqttMessageAdapter>(
        nullptr, [this](bool connected) { connectionStatusChanged(connected); },
        [this](bool completed, const std::vector<std::string> &topics) {
          publishProtobufCompleted(completed, topics);
        },
        m_external_modules->config->getMqttBrokerAddress(),
        m_external_modules->config->getMqttClientId())},
    m_heartbeat_timer{m_mqtt_message_adapter_interface,
                      m_external_modules->config->getHeartbeatTopic(),
                      m_external_modules->config->getMqttClientId(),
                      m_external_modules->config->getHeartbeatCycle()}
{
}

//-----------------------------------------------------------------------------

void ScrapeEngine::start()
{
  m_mqtt_message_adapter_interface->connect(m_external_modules->config->getMqttUser(),
                                            m_external_modules->config->getMqttPassword());
}

//-----------------------------------------------------------------------------

void ScrapeEngine::stop()
{
  m_mqtt_message_adapter_interface->disconnect();
  for (const auto &thread : m_plugins)
  {
    thread.first->stop();
    thread.second.wait();
  }
}

//-----------------------------------------------------------------------------

void ScrapeEngine::connectionStatusChanged(bool connected)
{
  if (!connected)
  {
    logging_handler::info(FUNC_PREAMBLE, "Connection lost. Trying to reconnect!");
    m_heartbeat_timer.stop();

    for (const auto &thread : m_plugins)
    {
      thread.first->stop();
      thread.second.wait();
    }

    m_plugins.clear();
    return;
  }

  if (!m_plugins.empty())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Already connected to broker! Ignoring!");
    return;
  }

  logging_handler::info(FUNC_PREAMBLE, "Connected to broker!");

  for (const auto &plugin_config : m_external_modules->config->getPlugins())
  {
    try
    {
      std::shared_ptr<PluginInterface> plugin = loadPlugin<PluginInterface>(plugin_config.first);
      plugin->initialize(
        m_external_modules->http_request_adapter,
        plugin_config.second,
        [this](std::vector<char> &serialized_proto,
               const message::ScrapeService &scrape_service) -> bool {
          return publishProtobufMessage(serialized_proto, scrape_service);
        }
      );

      m_plugins.emplace_back(plugin, std::async(std::launch::async, [plugin]() {
                               plugin->run();
                             }));
    }
    catch (const common::CommonException &ex)
    {
      logging_handler::warning(FUNC_PREAMBLE, "Ignoring plugin '{}': {}", plugin_config.first, ex.what());
    }
  }

  m_heartbeat_timer.start();
}

//-----------------------------------------------------------------------------

bool ScrapeEngine::publishProtobufMessage(std::vector<char> &serialized_proto,
                                          message::ScrapeService proto)
{
  proto.set_node_id(m_external_modules->config->getMqttClientId());

  const size_t message_size = proto.ByteSizeLong();
  serialized_proto = std::vector<char>(message_size);

  try
  {
    serializeProtobufMessage<message::ScrapeService>(
      proto,
      serialized_proto.data(),
      message_size
    );
  }
  catch (const common::CommonException &ex)
  {
    logging_handler::error(FUNC_PREAMBLE, ex.what());
    return false;
  }

  std::scoped_lock<std::mutex> scoped_lock {m_mqtt_publish_mutex};

  m_mqtt_message_adapter_interface->publish(
    *m_external_modules->config->getMqttTopicIds().begin(),
    serialized_proto.data(),
    message_size,
    mqttmessage::QualityOfServiceMode::kExactlyOnce,
    false
  );

  logging_handler::debug(FUNC_PREAMBLE, "Published message with size {}", message_size);

  std::unique_lock guard{m_mqtt_published_mutex};
  m_mqtt_published_condition_variable.wait(guard);

  return true;
}

//-----------------------------------------------------------------------------

void ScrapeEngine::publishProtobufCompleted(bool completed, const std::vector<std::string> &topics)
{
  logging_handler::debug(FUNC_PREAMBLE, "Protobuf publish: completed: {} topics: [{}]", completed,
                         topics);

  if (!completed)
  {
    logging_handler::warning(
      FUNC_PREAMBLE,
      "Could not complete the delivery of the following topics: {}", topics);

    return;
  }

  if (topics.empty())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Topics is empty!");
    return;
  }

  m_heartbeat_published = std::any_of(
    topics.begin(), topics.end(), [this, &completed](const std::string &topic) {
      return topic == m_external_modules->config->getHeartbeatTopic() &&
             completed;
    });

  if (m_heartbeat_published)
  {
    m_heartbeat_timer.heartbeatSuccessfullyPublished();
  }

  bool scrape_engine_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::set<std::string> &mqtt_topic_ids =
        m_external_modules->config->getMqttTopicIds();
      return std::any_of(mqtt_topic_ids.begin(), mqtt_topic_ids.end(),
                         [&](const std::string &configured_mqtt_topic_id) {
                           return published_topic == configured_mqtt_topic_id;
                         });
    });

  if (scrape_engine_topic_published)
  {
    m_mqtt_published_condition_variable.notify_all();

    std::for_each(
      m_plugins.begin(), m_plugins.end(),
      [](const std::pair<std::shared_ptr<PluginInterface>, std::future<void>>
           &plugin) { plugin.first->publishProtobufCompleted(); });
  }
}

} // namespace rpi_utilities
