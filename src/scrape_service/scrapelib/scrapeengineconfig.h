#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: scrape
*
************************************************/

// local includes

// common includes
#include "configbase.h"
#include "rpimacros.h"
#include <toml++/toml.hpp>

// system includes
#include <map>
#include <string>

//-----------------------------------------------------------------------------

using PluginMap = std::map<std::string, toml::table>;

//-----------------------------------------------------------------------------

namespace rpi_utilities::scrape {

class ScrapeEngineConfig : public config::ConfigBase
{
public:
  ScrapeEngineConfig(const ScrapeEngineConfig &) = default;
  ScrapeEngineConfig(ScrapeEngineConfig &&) = default;
  ScrapeEngineConfig &operator=(const ScrapeEngineConfig &) = default;
  ScrapeEngineConfig &operator=(ScrapeEngineConfig &&) = default;

  explicit ScrapeEngineConfig(const std::filesystem::path &log_directory)
    : ConfigBase{log_directory}
  {
    const toml::array *array = m_toml[kPluginsSectionName].as_array();
    array->for_each([this](auto &&elem) {
        if constexpr (!toml::is_table<decltype(elem)>)
        {
          return;
        }

        const toml::table *table = elem.as_table();

        const std::optional<std::string> plugin_path = (*table)["path"].value<std::string>();
        if (!plugin_path.has_value())
        {
          return;
        }

        m_plugins.emplace(plugin_path.value(), *table);
    });
  }

  explicit ScrapeEngineConfig(std::string &&mqtt_broker_address,
                              std::string &&mqtt_client_id,
                              std::string &&mqtt_user,
                              std::string &&mqtt_password,
                              std::set<std::string> &&mqtt_topic_ids,
                              std::string &&heartbeat_topic,
                              std::chrono::milliseconds heartbeat_cycle,
                              std::filesystem::path &&log_directory,
                              logging_handler::LoggingHandlerInterface::LoggingLevel log_level,
                              PluginMap &&plugins)
    : config::ConfigBase{
        std::move(mqtt_broker_address),
        std::move(mqtt_client_id),
        std::move(mqtt_user),
        std::move(mqtt_password),
        std::move(mqtt_topic_ids),
        std::move(heartbeat_topic),
        heartbeat_cycle,
        std::move(log_directory),
        log_level
      },
      m_plugins{std::move(plugins)}

  {}
  ~ScrapeEngineConfig() override = default;

  GET_VAL(Plugins, PluginMap, m_plugins)

  bool operator==(ScrapeEngineConfig &other)
  {
    return other.getMqttBrokerAddress() == getMqttBrokerAddress() &&
           other.getMqttClientId() == getMqttClientId()           &&
           other.getMqttUser() == getMqttUser()                   &&
           other.getMqttPassword() == getMqttPassword()           &&
           other.getMqttTopicIds() == getMqttTopicIds()           &&
           other.getHeartbeatTopic() == getHeartbeatTopic()       &&
           other.getHeartbeatCycle() == getHeartbeatCycle()       &&
           other.getLogDirectory() == getLogDirectory()           &&
           other.getLogLevel() == getLogLevel()                   &&
           other.getPlugins() == getPlugins();
  }

  bool operator!=(ScrapeEngineConfig &other)
  {
    return !(other == *this);
  }

private:
  static constexpr const char *kPluginsSectionName = "plugins";

  PluginMap m_plugins;
};

} // namespace rpi_utilities::scrape
