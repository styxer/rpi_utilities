#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape
*
************************************************/

// local includes
#include "plugininterface.h"
#include "stromnetzegrazapitypes.h"

// common includes
#include "async_timer.h"
#include "commonexception.h"
#include "scrape_service.pb.h"

// system includes
#include <filesystem>
#include <memory>
#include <optional>
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::scrape {

class StromNetzeGrazPlugin : public PluginInterface
{
private:
  struct Config
  {
    public:
      explicit Config(const toml::table &toml)
      {
        const std::optional<std::string> local_plugin_name = toml[kPluginNameName].value<std::string>();
        const std::optional<std::string> local_user = toml[kUserName].value<std::string>();
        const std::optional<std::string> local_password = toml[kPasswordName].value<std::string>();
        const std::optional<std::string> local_login_url = toml[kLoginUrlName].value<std::string>();
        const std::optional<std::string> local_installation_url = toml[kInstallationsUrlName].value<std::string>();
        const std::optional<std::string> local_meter_reading_data_url = toml[kMeterReadingDataUrlName].value<std::string>();
        const std::optional<std::string> local_meter_reading_url = toml[kMeterReadingUrlName].value<std::string>();
        const std::optional<std::string> local_ca_cert_path = toml[kCaCertPathName].value<std::string>();
        const std::optional<std::string> local_cache_directory_ = toml[kLocalCacheDirectoryName].value<std::string>();

        if (!local_plugin_name.has_value() || !local_user.has_value() ||
            !local_password.has_value() || !local_login_url.has_value() ||
            !local_installation_url.has_value() ||
            !local_meter_reading_data_url.has_value() ||
            !local_meter_reading_url.has_value() ||
            !local_ca_cert_path.has_value() ||
            !local_cache_directory_.has_value())
        {
          throw common::CommonException(
            "StromnetzeGraz plugin config is missing 'name', "
            "'user', 'password', 'login_url', 'installation_url', "
            "'meter_reading_data_url', 'meter_reading_url', 'ca_cert_path', 'local_cache_directory'");
        }

        plugin_name = *local_plugin_name;
        user = *local_user;
        password = *local_password;
        login_url = *local_login_url;
        installations_url = *local_installation_url;
        meter_reading_data_url = *local_meter_reading_data_url;
        meter_reading_url = *local_meter_reading_url;
        ca_cert_path = *local_ca_cert_path;
        local_cache_directory = *local_cache_directory_;
      }

      explicit Config(std::string &&plugin_path_,
                      std::string &&user_,
                      std::string &&password_,
                      std::string &&login_url_,
                      std::string &&installations_url_,
                      std::string &&meter_reading_data_url_,
                      std::string &&meter_reading_url_,
                      std::filesystem::path &&ca_cert_path_,
                      std::filesystem::path &&local_cache_directory_)
        : plugin_name{std::move(plugin_path_)},
          user{std::move(user_)},
          password{std::move(password_)},
          login_url{std::move(login_url_)},
          installations_url{std::move(installations_url_)},
          meter_reading_data_url{std::move(meter_reading_data_url_)},
          meter_reading_url{std::move(meter_reading_url_)},
          ca_cert_path{std::move(ca_cert_path_)},
          local_cache_directory{std::move(local_cache_directory_)}
      {}

      std::string plugin_name;
      std::string user;
      std::string password;
      std::string login_url;
      std::string installations_url;
      std::string meter_reading_data_url;
      std::string meter_reading_url;
      std::filesystem::path ca_cert_path;
      std::filesystem::path local_cache_directory;

    private:
      static constexpr const char *kPluginNameName = "name";
      static constexpr const char *kUserName = "user";
      static constexpr const char *kPasswordName = "password";
      static constexpr const char *kLoginUrlName = "login_url";
      static constexpr const char *kInstallationsUrlName = "installations_url";
      static constexpr const char *kMeterReadingDataUrlName = "meter_reading_data_url";
      static constexpr const char *kMeterReadingUrlName = "meter_reading_url";
      static constexpr const char *kCaCertPathName = "ca_cert_path";
      static constexpr const char *kLocalCacheDirectoryName = "local_cache_directory";
  };

public:
  StromNetzeGrazPlugin(const StromNetzeGrazPlugin &) = delete;
  StromNetzeGrazPlugin(StromNetzeGrazPlugin &&) = delete;
  StromNetzeGrazPlugin &operator=(const StromNetzeGrazPlugin &) = delete;
  StromNetzeGrazPlugin &operator=(StromNetzeGrazPlugin &&) = delete;

  StromNetzeGrazPlugin();
  ~StromNetzeGrazPlugin() override = default;

  //! \see interface.
  void initialize(
    std::shared_ptr<rpi_utilities::http_request::HttpRequestAdapter> http_request_adapter,
    const toml::table &toml_payload,
    std::function<bool(std::vector<char> &, const message::ScrapeService &)> mqtt_callback) override;

  //! \see interface
  void run() override;

  //! \see interface
  void stop() override;

  //! \see interface
  void publishProtobufCompleted() override;

  enum class TaskType
  {
    kReadOrUpdateLoginToken,
    kSyncInvalidMeterReadings,
    kGetInstallations,
    kGetMeterReadingsData,
    kGetMeterReadings,
    kPublishProtobufMessage,
    kSaveInvalidReadings,
    kCleanupMembers,
  };

private:
  enum class TaskResult
  {
    kError,
    kNoResult,
    kSuccess,
    kSkipNextTask,
  };

  struct TaskQueueEntry
  {
    TaskType type;
    std::function<TaskResult ()> callback;
  };

  std::vector<TaskQueueEntry> m_task_queue;

  std::shared_ptr<rpi_utilities::http_request::HttpRequestAdapter> m_http_request_adapter = nullptr;
  std::function<bool(std::vector<char> &, const message::ScrapeService &)> m_mqtt_callback;

  constexpr static const char *kTokenFilename = "login_token.token";
  constexpr static const char *kLastUpdateTimestampFilename = "last_update.timestamp";
  constexpr static const char *kInvalidReadingsFilename = "invalid_readings.json";

  constexpr static const char *kInterval = "Daily";
  constexpr static const char *kUnitOfConsump = "W";

  std::unique_ptr<common::AsyncTimer> m_request_timer;
  std::unique_ptr<Config> m_config;

  std::atomic<bool> m_protobuf_publish_pending = false;

  std::condition_variable m_condition_variable;
  std::mutex m_mutex;

  bool m_task_queue_successful = true;
  std::atomic<bool> m_stop_plugin = false;

  std::future<void> m_calculate_next_synchronisation_time_future;
  std::condition_variable m_timer_condition_variable;
  std::mutex m_timer_mutex;

  std::vector<char> m_serialized_protobuf;

  struct InvalidReading
  {
    int meter_id = 0;
    std::string date_time;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(InvalidReading, meter_id, date_time);
  };

  std::vector<InvalidReading> m_invalid_readings;

  std::string m_login_token;
  Installations m_installations;
  MeterReadingsData m_meter_readings_data;
  MeterReadings m_meter_readings;

  //! Updates the login token.
  void updateLoginToken();

  //! Checks if there is a JWT login token stored locally. If there is a local token stored and it
  //! is not expired, this token will be used. If not, a new token will be requested from the API.
  //! \return TaskResult
  TaskResult readOrUpdateLoginToken();

  //! Retrieves all installations for the configured facilities.
  //! \return TaskResult
  TaskResult getInstallations();

  //! Retrieves all available readings.
  //! \return TaskResult
  TaskResult getMeterReadingsData();

  //! Sends a MeterReadingRequest with the given parameters.
  //! \param meter_point_id The ID of the meter point.
  //! \param from_timestamp The starting timestamp.
  //! \param to_timestamp The ending timestamp.
  MeterReadingResponse sendMeterReadingRequest(int meter_point_id,
                                               const std::string &from_timestamp,
                                               const std::string &to_timestamp);

  //! Retrieves the actual meter readings. E.g. The actual [Wh] consumed per day.
  //! \return TaskResult
  TaskResult getMeterReadings();

  //! Converts the internal data structures into the given protobuf message.
  message::ScrapeService createProtobufMessage();

  //! Callback which will be triggered by the configured request timer.
  bool processEventQueue();

  void calculateNextSynchronisationTime();

  //! Tries to request all cached invalid meter readings.
  //! \return TaskResult
  TaskResult syncInvalidMeterReadings();

  //! Helper method which inserts the given MeterReadingResponse into the internal meter readings
  //! cache.
  //! \param meter_id The meter ID to which the MeterReadingResponse belongs to.
  //! \param meter_reading_response The response which should be inserted.
  void insertIntoMeterReadings(int meter_id, MeterReadingResponse meter_reading_response);

  //! Creates and publishes the profotbuf message.
  //! \return TaskResult
  TaskResult publishProtobufMessage();

  //! Saves all invalid readings to the filesystem.
  //! \return TaskResult
  TaskResult saveInvalidReadings();

  //! Resets all members.
  //! \return TaskResult
  TaskResult cleanupMembers();

  //! Helper method which sends a authorized post request.
  //! \param request_url The URL which should be used for the request.
  //! \param payload The payload which shall be send.
  //! \return The parsed response.
  template<class T, class U>
  T sendPostRequest(const std::string &request_url, const U &payload);
};

} // namespace rpi_utilities::scrape

template <>
struct fmt::formatter<rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType>
{
  static constexpr auto parse(format_parse_context& ctx)
  {
    return ctx.end();
  }

  static auto format(const rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType& task_type,
                     fmt::format_context& ctx)
  {
    switch (task_type)
    {
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kSyncInvalidMeterReadings:
        return fmt::format_to(ctx.out(), "SYNC_INVALID_METER_READINGS");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kSaveInvalidReadings:
        return fmt::format_to(ctx.out(), "SAVE_INVALID_READINGS");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kPublishProtobufMessage:
        return fmt::format_to(ctx.out(), "PUBLISH_PROTOBUF_MESSAGE");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kCleanupMembers:
        return fmt::format_to(ctx.out(), "CLEANUP_MEMBERS");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kGetInstallations:
        return fmt::format_to(ctx.out(), "GET_INSTALLATIONS");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kGetMeterReadings:
        return fmt::format_to(ctx.out(), "GET_METER_READINGS");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kGetMeterReadingsData:
        return fmt::format_to(ctx.out(), "GET_METER_READINGS_DATA");
      case rpi_utilities::scrape::StromNetzeGrazPlugin::TaskType::kReadOrUpdateLoginToken:
        return fmt::format_to(ctx.out(), "READ_OR_UPDATE_LOGIN_TOKEN");
    }
  }
};
