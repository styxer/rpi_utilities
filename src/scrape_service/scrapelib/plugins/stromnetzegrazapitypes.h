#pragma once
/************************************************
 *
 * Author: @styxer <styxer@tutanota.com>
 * Year:   2023
 *
 * Module: scrape
 *
 ************************************************/

// local includes

// common includes
#include "nlohmann/json.hpp"
#include "logginghelper.h"

// system includes
#include <map>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

//-----------------------------------------------------------------------------

struct LoginRequest
{
  std::string email;
  std::string password;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(LoginRequest, email, password);
};

//-----------------------------------------------------------------------------

struct CustomStringType
{
  std::optional<std::string> value;
};

namespace nlohmann
{
  template<>
  struct adl_serializer<CustomStringType>
  {
    static void to_json(json& j, const CustomStringType &in) // NOLINT
    {
      j = in.value.has_value() ? *in.value : "null";
    }

    static void from_json(const json& j, CustomStringType &out) // NOLINT
    {
      if (!j.is_string())
      {
        return;
      }

      out.value = j.get<std::string>();
    }
  };
} // namespace nlohmann

//-----------------------------------------------------------------------------

struct LoginResponse
{
  std::string error;
  bool success;
  CustomStringType token;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(LoginResponse, error, success, token)
};

//-----------------------------------------------------------------------------

/*
 *
 * Get installations request data
 * {}
 */

struct InstallationsRequest {};

//-----------------------------------------------------------------------------

/*
 * "optState": {
 *   "currentOptState": "OptMiddle",
 *   "optStateOptions": [
 *     {
 *       "optState": "OptIn",
 *       "applicable": true
 *     },
 *     {
 *       "optState": "OptMiddle",
 *       "applicable": true
 *     },
 *     {
 *       "optState": "OptOut",
 *       "applicable": true
 *     }
 *   ]
 * }
 */

struct OptStateOption
{
  std::string optState;
  bool applicable;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(OptStateOption, optState, applicable);
};

using OptStateOptions = std::vector<OptStateOption>;

struct OptState
{
  std::string currentOptState;
  OptStateOptions optStateOptions;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(OptState, currentOptState, optStateOptions);
};

//-----------------------------------------------------------------------------

/*
 * "displayState": {
 *   "currentDisplayState": "Off",
 *   "displayStateOptions": [
 *     {
 *       "displayState": "On",
 *       "applicable": true
 *     },
 *     {
 *       "displayState": "Off",
 *       "applicable": true
 *     }
 *   ]
 * }
 */

struct DisplayStateOption
{
  std::string displayState;
  bool applicable;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(DisplayStateOption, displayState, applicable);
};

using DisplayStateOptions = std::vector<DisplayStateOption>;

struct DisplayState
{
  std::string currentDisplayState;
  DisplayStateOptions displayStateOptions;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(DisplayState, currentDisplayState,
                                 displayStateOptions);
};

//-----------------------------------------------------------------------------

/*
 * "customerInterfaceState": {
 *   "currentCustomerInterfaceState": "Off",
 *   "customerInterfaceOptions": [
 *     {
 *       "customterInterfaceState": "On",
 *       "applicable": true
 *     },
 *     {
 *       "customterInterfaceState": "Off",
 *       "applicable":true
 *     }
 *   ]
 * }
 */

struct CustomerInterfaceOption
{
  std::string customterInterfaceState;  //< Looks like there is a typo in the
                                        //  official API :P
  bool applicable;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(CustomerInterfaceOption,
                                 customterInterfaceState, applicable);
};

using CustomerInterfaceOptions = std::vector<CustomerInterfaceOption>;

struct CustomerInterfaceState
{
  std::string currentCustomerInterfaceState;
  CustomerInterfaceOptions customerInterfaceOptions;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(CustomerInterfaceState,
                                 currentCustomerInterfaceState,
                                 customerInterfaceOptions);
};

//-----------------------------------------------------------------------------

/*
 * "accountingPeriode": {
 *   "currentAccountingPeriode": "Yearly",
 *   "accountingPeriodeOptions": [
 *     {
 *       "accountingPeriode": "Monthly",
 *       "applicable": true
 *     },
 *     {
 *       "accountingPeriode": "Yearly",
 *       "applicable": true
 *     }
 *   ]
 * }
 */

struct AccountingPeriodeOption
{
  std::string accountingPeriode;
  bool applicable;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(AccountingPeriodeOption, accountingPeriode,
                                 applicable);
};

using AccountingPeriodeOptions = std::vector<AccountingPeriodeOption>;

struct AccountingPeriode
{
  std::string currentAccountingPeriode;
  AccountingPeriodeOptions accountingPeriodeOptions;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(AccountingPeriode, currentAccountingPeriode,
                                 accountingPeriodeOptions);
};

//-----------------------------------------------------------------------------

/*
 * "meterPoints": [
 *   {
 *     "meterPointID": <ID>,
 *     "name": "",
 *     "shortName": "",
 *     "singleInvoiceCustomer": true,
 *     "optState": {},
 *     "displayState": {},
 *     "customerInterfaceState": {},
 *     "accountingPeriode": {}
 *   }
 * ]
 */

struct MeterPoint
{
  int meterPointID;
  std::string name;
  std::string shortName;
  bool singleInvoiceCustomer;
  OptState optState;
  DisplayState displayState;
  CustomerInterfaceState customerInterfaceState;
  AccountingPeriode accountingPeriode;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(MeterPoint, meterPointID, name, shortName,
                                 singleInvoiceCustomer, optState, displayState,
                                 customerInterfaceState, accountingPeriode);
};

using MeterPoints = std::vector<MeterPoint>;

//-----------------------------------------------------------------------------

/*
 * [
 *   {
 *     "installationID": <ID>,
 *     "installationNumber": <NUMBER_ID>,
 *     "customerID": <CUSTOMER_ID>,
 *     "customerNumber": <CUSTOMER_NUMBER>,
 *     "address": "",
 *     "deliveryDirection": "Bezug",
 *     "meterPoints": []
 *   }
 * ]
 */

struct Installation
{
  int installationID;
  int installationNumber;
  int customerID;
  int customerNumber;
  std::string address;
  std::string deliveryDirection;
  MeterPoints meterPoints;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(Installation, installationID,
                                 installationNumber, customerID, customerNumber,
                                 address, deliveryDirection, meterPoints);
};

using Installations = std::vector<Installation>;

//-----------------------------------------------------------------------------

/*
 * Meter Reading data request data
 * {
 *   "meterPointId": <ID>
 * }
 */

struct MeterReadingDataRequest
{
  int meterPointId;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(MeterReadingDataRequest, meterPointId);
};

//-----------------------------------------------------------------------------

/*
 * {
 *   "year": 2022,
 *   "retrieveableQuarters": [2, 3, 4],
 *   "retrieveableMonths": [6, 7, 8, 9, 10, 11, 12],
 *   "retrieveableWeeks":[24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, ...]
 * }
 */

struct RetrieveableReading
{
  int year;
  std::vector<int> retrieveableQuarters;
  std::vector<int> retrieveableMonths;
  std::vector<int> retrieveableWeeks;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(RetrieveableReading, year,
                                 retrieveableQuarters, retrieveableMonths,
                                 retrieveableWeeks);
};

using RetrieveableReadings = std::vector<RetrieveableReading>;

/*
 * {
 *   "readingsAvailableSince": "2022-06-17T14:49:32",
 *   "retrieveableReadings": [ ]
 * }
 */

struct MeterReadingDataResponse
{
  std::string readingsAvailableSince;
  RetrieveableReadings retrieveableReadings;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(MeterReadingDataResponse,
                                 readingsAvailableSince, retrieveableReadings);
};

using MeterReadingsData = std::map<int, MeterReadingDataResponse>;

//-----------------------------------------------------------------------------

/*
 * Meter Reading request data
 * {
 *   "meterPointId": ID,
 *   "fromDate": "2023-01-01T00:00:00.000+01:00",
 *   "toDate": "2024-01-01T00:00:00.000+01:00",
 *   "interval": "Monthly/Daily/QuarterHourly",
 *   "unitOfConsump":"KWH/WH/KW/W"
 * }
 */

struct MeterReadingRequest
{
  int meterPointId;
  std::string fromDate;
  std::string toDate;
  std::string interval;
  std::string unitOfConsump;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(MeterReadingRequest,
                                 meterPointId,
                                 fromDate,
                                 toDate,
                                 interval,
                                 unitOfConsump);
};

//-----------------------------------------------------------------------------

struct CustomNumericalType
{
  std::optional<double> value;
};

namespace nlohmann
{
  template<>
  struct adl_serializer<CustomNumericalType>
  {
    static void to_json(json& j, const CustomNumericalType &in) // NOLINT
    {
      j = in.value.has_value() ? std::to_string(*in.value) : "null";
    }

    static void from_json(const json& j, CustomNumericalType &out) // NOLINT
    {
      if (!j.is_number())
      {
        return;
      }

      out.value = j.get<double>();
    }
  };
} // namespace nlohmann

//-----------------------------------------------------------------------------

/*
 * {
 *   "scale":"ET",
 *   "readingType": "CONSUMP", --> Monthly consumption
 *   "unit": "KWH",
 *   "readingState": "Valid",
 *   "value": 46.288002
 * }
 */

struct ReadingValue
{
  std::string scale;
  std::string readingType;
  std::string unit;
  std::string readingState;
  CustomNumericalType value;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(ReadingValue,
                                 scale,
                                 readingType,
                                 unit,
                                 readingState,
                                 value);
};

using ReadingValues = std::vector<ReadingValue>;

//-----------------------------------------------------------------------------

/*
 * {
 *   "readTime": "2022-12-31T23:00:00Z",
 *   "readingValues": [ ]
 * }
 */

struct Reading
{
  std::string readTime;
  ReadingValues readingValues;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(Reading, readTime, readingValues);
};

using Readings = std::vector<Reading>;

//-----------------------------------------------------------------------------

/*
 * {
 *   "intervalType": "Monthly",
 *   "readings": []
 * }
 */

struct MeterReadingResponse
{
  std::string intervalType;
  Readings readings;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(MeterReadingResponse, intervalType, readings);
};

using MeterReadings = std::map<int, MeterReadingResponse>;
