#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape
*
************************************************/

// local includes
#include "httprequestadapter.h"

// common includes
#include <toml++/toml.hpp>
#include "scrape_service.pb.h"

// system includes
#include <functional>
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::scrape
{

class PluginInterface
{
public:
  PluginInterface(const PluginInterface &) = delete;
  PluginInterface(PluginInterface &&) = delete;
  PluginInterface &operator=(const PluginInterface &) = delete;
  PluginInterface &operator=(PluginInterface &&) = delete;

  PluginInterface() = default;
  virtual ~PluginInterface() = default;

  //! This method is used to initialize all needed members.
  //! NOTE: This method is necessary because of the plugin infrastructure.
  //! \param http_request_adapter Helper class to perform HTTP requests.
  //! \param toml_payload Plugin configuration as JSON.
  //! \param mqtt_callback Callback to the engine to publish messages.
  virtual void initialize(
    std::shared_ptr<rpi_utilities::http_request::HttpRequestAdapter> http_request_adapter,
    const toml::table &toml_payload,
    std::function<bool(std::vector<char> &, const message::ScrapeService&)> mqtt_callback) = 0;

  //! Runs the plugin
  virtual void run() = 0;

  //! Stops the plugin
  virtual void stop() = 0;

  //! Must be called when a protobuf message has been successfully published.
  virtual void publishProtobufCompleted() = 0;
};

} // namespace rpi_utilities::scrape
