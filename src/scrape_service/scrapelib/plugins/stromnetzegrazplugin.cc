/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape
*
************************************************/

// local includes
#include "stromnetzegrazplugin.h"

// common includes
#include "commonexception.h"
#include "date/date.h"
#include "date/tz.h"
#include "httprequestadapter.h"
#include "jwt-cpp/jwt.h"
#include "jwt-cpp/traits/nlohmann-json/traits.h"
#include "logginghelper.h"
#include "rpimacros.h"
#include "rpiutilities.h"

// system includes
#include <chrono>
#include <filesystem>
#include <fstream>
#include <sstream>

//------------------------------------------------------------------------------

constexpr const char *kTimeFormat = "%Y-%m-%dT%T%z";

//------------------------------------------------------------------------------

namespace rpi_utilities::scrape {

DEFINE_PLUGIN(StromNetzeGrazPlugin, PluginAllocator, PluginDeallocator) // NOLINT

constexpr const char *kContentTypeHeader = "Content-Type: application/json";

}

//------------------------------------------------------------------------------

namespace rpi_utilities::scrape
{

StromNetzeGrazPlugin::StromNetzeGrazPlugin() = default;

//------------------------------------------------------------------------------

void StromNetzeGrazPlugin::initialize(
    std::shared_ptr<rpi_utilities::http_request::HttpRequestAdapter> http_request_adapter,
    const toml::table &toml_payload,
    std::function<bool(std::vector<char> &, const message::ScrapeService &)> mqtt_callback)
{
  m_http_request_adapter = http_request_adapter;
  m_mqtt_callback = mqtt_callback;
  m_config = std::make_unique<Config>(toml_payload);

  m_request_timer = std::make_unique<common::AsyncTimer>(
    [this] {
      return processEventQueue();
    }
  );

  m_task_queue = {
      TaskQueueEntry {
        TaskType::kReadOrUpdateLoginToken,
        [this]() -> TaskResult { return readOrUpdateLoginToken(); }
      },
      TaskQueueEntry {
        TaskType::kSyncInvalidMeterReadings,
        [this]() -> TaskResult { return syncInvalidMeterReadings(); }
      },
      TaskQueueEntry {
        TaskType::kGetInstallations,
        [this]() -> TaskResult { return getInstallations(); }
      },
      TaskQueueEntry {
        TaskType::kGetMeterReadingsData,
        [this]() -> TaskResult { return getMeterReadingsData(); }
      },
      TaskQueueEntry {
        TaskType::kGetMeterReadings,
        [this]() -> TaskResult { return getMeterReadings(); }
      },
      TaskQueueEntry {
        TaskType::kPublishProtobufMessage,
        [this]() -> TaskResult { return publishProtobufMessage(); }
      },
      TaskQueueEntry {
        TaskType::kSaveInvalidReadings,
        [this]() -> TaskResult { return saveInvalidReadings(); }
      },
      TaskQueueEntry {
        TaskType::kCleanupMembers,
        [this]() -> TaskResult { return cleanupMembers(); }
      },
  };
}

//------------------------------------------------------------------------------

void StromNetzeGrazPlugin::run()
{
  if (m_request_timer == nullptr)
  {
    logging_handler::warning(FUNC_PREAMBLE, "Request timer not setup correctly!");
    return;
  }

  m_stop_plugin = false;

  processEventQueue();

  m_calculate_next_synchronisation_time_future = std::async(
    std::launch::async, [this]() { calculateNextSynchronisationTime(); });
}

//------------------------------------------------------------------------------

void StromNetzeGrazPlugin::stop()
{
  m_stop_plugin = true;
  m_timer_condition_variable.notify_all();

  m_request_timer->stop().wait();
  m_calculate_next_synchronisation_time_future.wait();
}

//------------------------------------------------------------------------------

void StromNetzeGrazPlugin::publishProtobufCompleted()
{
  if (!m_protobuf_publish_pending)
  {
    return;
  }

  m_protobuf_publish_pending = false;
  m_condition_variable.notify_all();
}

//------------------------------------------------------------------------------

void StromNetzeGrazPlugin::updateLoginToken()
{
  const LoginRequest login_request {
    m_config->user,
    m_config->password
  };

  LoginResponse converted_login_response;
  try
  {
    converted_login_response = sendPostRequest<LoginResponse>(m_config->login_url, login_request);
  }
  catch (const nlohmann::json::exception &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
    return;
  }
  catch (const common::CommonException &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
    return;
  }

  if (!converted_login_response.success || !converted_login_response.token.value.has_value())
  {
    logging_handler::warning(FUNC_PREAMBLE,
                             "Login was not successful: {}",
                             converted_login_response.error);
    return;
  }

  m_login_token = *converted_login_response.token.value;
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::readOrUpdateLoginToken()
{
  std::fstream login_token_file;
  const std::filesystem::path login_token_path = fmt::format("{}{}", m_config->local_cache_directory.string(), kTokenFilename);
  login_token_file.open(login_token_path, std::fstream::in);

  if (!login_token_file.is_open())
  {
    updateLoginToken();
    if (m_login_token.empty())
    {
      return TaskResult::kError;
    }

    login_token_file.open(login_token_path, std::fstream::out);
    login_token_file << m_login_token;

    logging_handler::info(FUNC_PREAMBLE, "Writing login token to cache-file");
  }
  else
  {
    logging_handler::info(FUNC_PREAMBLE, "Reading cached login token");
    login_token_file >> m_login_token;
  }

  login_token_file.close();

  try
  {
    const auto decoded = jwt::decode<jwt::traits::nlohmann_json>(m_login_token);

    const auto expires_timestamp = decoded.get_expires_at().time_since_epoch();
    const auto current_timestamp = std::chrono::system_clock::now().time_since_epoch();

    if (expires_timestamp > current_timestamp)
    {
      return TaskResult::kNoResult;
    }

    updateLoginToken();
    if (m_login_token.empty())
    {
      return TaskResult::kError;
    }

    login_token_file.open(login_token_path, std::fstream::out);
    login_token_file << m_login_token;

    logging_handler::info(FUNC_PREAMBLE, "Updating cached token");
  }
  catch (const std::exception &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "Error while decoding JWT-Token: '{}'", ex.what());
    return TaskResult::kError;
  }

  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::getInstallations()
{
  try
  {
    m_installations
      = sendPostRequest<Installations>(m_config->installations_url, nlohmann::json::object());
  }
  catch (const nlohmann::json::exception &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
    return TaskResult::kError;
  }
  catch (const common::CommonException &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
    return TaskResult::kError;
  }

  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::getMeterReadingsData()
{
  for (const auto& installation : m_installations)
  {
    for (const auto& meter_point : installation.meterPoints)
    {
      MeterReadingDataRequest meter_reading_data_request {
        meter_point.meterPointID
      };

      try
      {
        auto meter_reading_data_response =
          sendPostRequest<MeterReadingDataResponse>(m_config->meter_reading_data_url,
                                                    meter_reading_data_request);

        m_meter_readings_data.emplace(meter_point.meterPointID, meter_reading_data_response);
      }
      catch (const nlohmann::json::exception &ex)
      {
        logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
      }
      catch (const common::CommonException &ex)
      {
        logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
      }
    }
  }

  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

MeterReadingResponse StromNetzeGrazPlugin::sendMeterReadingRequest(int meter_point_id,
                                                                   const std::string &from_timestamp,
                                                                   const std::string &to_timestamp)
{
  // Always request days because it's the smallest interval -> Everything else can be computed
  MeterReadingRequest meter_reading_request {
    meter_point_id,
    from_timestamp,
    to_timestamp,
    kInterval,
    kUnitOfConsump
  };

  return sendPostRequest<MeterReadingResponse>(m_config->meter_reading_url, meter_reading_request);
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::getMeterReadings()
{
  std::string cached_from_timestamp;
  const std::filesystem::path last_update_timestamp_path = fmt::format("{}{}", m_config->local_cache_directory.string(), kLastUpdateTimestampFilename);
  if (std::filesystem::exists(last_update_timestamp_path))
  {
    std::fstream last_update_timestamp_file;
    last_update_timestamp_file.open(last_update_timestamp_path, std::fstream::in);

    last_update_timestamp_file >> cached_from_timestamp;

    last_update_timestamp_file.close();
  }

  auto timestamp
    = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now()) - date::days{1};

  std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> today_as_day {
    std::chrono::floor<date::days>(timestamp)
  };

  std::stringstream in_stream{cached_from_timestamp};
  std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> cached_from_timestamp_in_seconds{};
  in_stream >> date::parse(kTimeFormat, cached_from_timestamp_in_seconds);

  std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> cached_from_timestamp_as_day {
    std::chrono::floor<date::days>(cached_from_timestamp_in_seconds)
  };

  if (cached_from_timestamp_as_day == today_as_day)
  {
    logging_handler::info(FUNC_PREAMBLE, "Already retrieved readings from today. Skipping!");

    if (m_meter_readings.empty())
    {
      return TaskResult::kSkipNextTask;
    }

    return TaskResult::kSuccess;
  }

  constexpr std::chrono::hours kTwentyThreeHours {23};
  constexpr std::chrono::minutes kFiftyNineMinutes {59};
  constexpr std::chrono::seconds kFiftyNineSeconds {59};

  auto to_timestamp = date::make_zoned(
    date::current_zone(),
    date::local_seconds {
      (today_as_day +
      kTwentyThreeHours +
      kFiftyNineMinutes +
      kFiftyNineSeconds).time_since_epoch()
    }
  );

  std::stringstream to_timestamp_stream;
  to_timestamp_stream << date::format(kTimeFormat, to_timestamp);
  std::string to_timestamp_as_string = to_timestamp_stream.str();

  for (const auto &kv : m_meter_readings_data)
  {
    int meter_point_id = kv.first;
    MeterReadingDataResponse meter_reading_data = kv.second;

    std::string from_timestamp
      = cached_from_timestamp.empty() ? meter_reading_data.readingsAvailableSince
                                      : cached_from_timestamp;

    logging_handler::info(FUNC_PREAMBLE,
                          "Requesting data from {} to {} for meter point ID {}",
                          from_timestamp,
                          to_timestamp_as_string,
                          meter_point_id);

    try
    {
      const MeterReadingResponse meter_reading_response = sendMeterReadingRequest(
        meter_point_id,
        from_timestamp,
        to_timestamp_as_string
      );


      std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> tp;

      std::stringstream ss{from_timestamp};
      ss >> date::parse(kTimeFormat, tp);

      auto from_timestamp_utc = date::make_zoned(date::current_zone(), date::floor<date::days>(tp));

      for (std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds>
             start_time_point {
               from_timestamp_utc.get_local_time().time_since_epoch()};
           start_time_point <= today_as_day;
           start_time_point += date::days{1})
      {
        auto from_timestamp_local_floored = date::floor<date::days>(start_time_point);
        bool no_reading_found = std::none_of(meter_reading_response.readings.begin(),
                                             meter_reading_response.readings.end(),
                                             [&](const Reading &reading) {
                                               std::stringstream read_time_ss{reading.readTime};

                                               std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> tp;
                                               read_time_ss >> date::parse("%Y-%m-%dT%TZ", tp);

                                               auto zoned_tp = date::make_zoned(date::current_zone(), tp);

                                               tp = std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> {
                                                 zoned_tp.get_local_time().time_since_epoch()
                                               };

                                               return date::floor<date::days>(tp) == from_timestamp_local_floored;
                                             });

        if (no_reading_found)
        {
          std::stringstream out;
          out << date::format(kTimeFormat, date::make_zoned(date::current_zone(),
                                                            date::local_seconds {
                                                              (from_timestamp_local_floored +
                                                               kTwentyThreeHours +
                                                               kFiftyNineMinutes +
                                                               kFiftyNineSeconds).time_since_epoch()
                                                            }));

          logging_handler::warning(FUNC_PREAMBLE, "Missing reading from '{}'", out.str());
          m_invalid_readings.emplace_back(InvalidReading{meter_point_id, out.str()});
        }
      }

      insertIntoMeterReadings(meter_point_id, meter_reading_response);
    }
    catch (const common::CommonException &ex)
    {
      logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
    }
  }

  if (m_meter_readings.empty())
  {
    return TaskResult::kError;
  }

  std::stringstream ss;
  ss << date::format(kTimeFormat,
                     date::make_zoned(date::current_zone(),
                                      date::local_seconds {
                                        (timestamp + std::chrono::seconds{1}).time_since_epoch()
                                      }));

  std::string last_update_time_as_string = ss.str();

  std::fstream last_update_timestamp_file;
  last_update_timestamp_file.open(last_update_timestamp_path, std::fstream::out);
  last_update_timestamp_file << last_update_time_as_string;
  last_update_timestamp_file.close();

  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

message::ScrapeService StromNetzeGrazPlugin::createProtobufMessage()
{
  message::ScrapeService scrape_service_proto;
  message::StromnetzeGraz *stromnetze_graz_proto =
      scrape_service_proto.mutable_stromnetzegraz();

  ::google::protobuf::Map<long, message::MeterReading> *meter_readings_proto
    = stromnetze_graz_proto->mutable_meter_readings();

  for (const auto &meter_reading : m_meter_readings)
  {
    int meter_point_id = meter_reading.first;
    MeterReadingResponse meter_reading_response = meter_reading.second;

    message::MeterReading meter_reading_proto;
    meter_reading_proto.set_interval_type(meter_reading_response.intervalType);

    const Readings &readings = meter_reading_response.readings;
    for (const auto& reading : readings)
    {
      message::Reading *reading_proto = meter_reading_proto.add_reading();

      std::chrono::time_point<std::chrono::system_clock> tp;
      std::stringstream ss{reading.readTime};
      ss >> date::parse("%Y-%m-%dT%TZ", tp);

      reading_proto->set_read_time(
          std::chrono::duration_cast<std::chrono::seconds>(tp.time_since_epoch()).count());

      for (const auto& reading_value : reading.readingValues)
      {
        if (!reading_value.value.value.has_value())
        {
          logging_handler::warning(FUNC_PREAMBLE,
                                   "Ignoring reading because no valid value has been found:\n"
                                   "  ReadingTime: {}\n"
                                   "  MeterPointId: {}\n"
                                   "  Scale: {}\n"
                                   "  ReadingType: {}\n"
                                   "  ReadingState: {}",
                                   reading.readTime,
                                   meter_point_id,
                                   reading_value.scale,
                                   reading_value.readingType,
                                   reading_value.readingState);

          auto invalid_readings_it = std::find_if(
              m_invalid_readings.begin(), m_invalid_readings.end(),
              [&meter_point_id, &reading](const InvalidReading &entry) {
                return entry.meter_id == meter_point_id && entry.date_time == reading.readTime;
              });

          if (invalid_readings_it == m_invalid_readings.end())
          {
            m_invalid_readings.emplace_back(InvalidReading{meter_point_id, reading.readTime});
          }

          continue;
        }

        message::ReadingValue *reading_value_proto = reading_proto->add_reading_value();
        reading_value_proto->set_scale(reading_value.scale);
        reading_value_proto->set_reading_type(reading_value.readingType);
        reading_value_proto->set_unit(reading_value.unit);
        reading_value_proto->set_reading_state(reading_value.readingState);
        reading_value_proto->set_value(*reading_value.value.value);
      }
    }

    meter_readings_proto->insert({meter_point_id, meter_reading_proto});
  }

  stromnetze_graz_proto->set_timestamp(
      std::chrono::duration_cast<std::chrono::seconds>(
          std::chrono::system_clock::now().time_since_epoch())
          .count());

  return scrape_service_proto;
}

//------------------------------------------------------------------------------

bool StromNetzeGrazPlugin::processEventQueue()
{
  m_task_queue_successful = true;
  for (auto task_it = m_task_queue.begin(); task_it != m_task_queue.end();)
  {
    logging_handler::info(FUNC_PREAMBLE, "Executing task '{}'", task_it->type);
    TaskResult result = task_it->callback();

    if (result == TaskResult::kError)
    {
      logging_handler::error(FUNC_PREAMBLE,
                             "Task '{}' did not run successfully. Aborting!",
                             task_it->type);

      m_task_queue_successful = false;
      break;
    }

    if (result == TaskResult::kSkipNextTask)
    {
      logging_handler::warning(FUNC_PREAMBLE,
                               "Task '{}' signaled to skip next task!",
                               task_it->type);
      task_it += 2;
    }
    else
    {
      logging_handler::info(FUNC_PREAMBLE,
                            "Task '{}' finished successfully",
                            task_it->type);

      ++task_it;
    }
  }

  m_timer_condition_variable.notify_all();
  return true;
}

//-----------------------------------------------------------------------------

void StromNetzeGrazPlugin::calculateNextSynchronisationTime()
{
  while (true)
  {
    std::future<void> &future = m_request_timer->stop();
    if (future.valid())
    {
      future.wait();
    }

    auto current_timestamp
      = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now());

    auto next_request_time =
       m_task_queue_successful
           ? date::floor<date::days>(current_timestamp) +
                 date::days{1} + std::chrono::hours{1}
           : current_timestamp + std::chrono::hours{1};

    auto delta = date::make_zoned(date::current_zone(), next_request_time).get_local_time()
      - date::make_zoned(date::current_zone(), current_timestamp).get_local_time();

    std::stringstream to_timestamp_stream;
    to_timestamp_stream << date::format(
        kTimeFormat, date::make_zoned(date::current_zone(), next_request_time));

    logging_handler::info(FUNC_PREAMBLE, "Next synchronisation time: {}", to_timestamp_stream.str());

    m_request_timer->start(std::chrono::duration_cast<std::chrono::milliseconds>(delta));

    m_task_queue_successful = false;

    std::unique_lock lk{m_timer_mutex};
    m_timer_condition_variable.wait(lk);

    if (m_stop_plugin)
    {
      break;
    }
  }
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::syncInvalidMeterReadings()
{
  const std::filesystem::path invalid_readings_path = fmt::format("{}{}", m_config->local_cache_directory.string(), kInvalidReadingsFilename);
  if (!std::filesystem::exists(invalid_readings_path))
  {
    return TaskResult::kNoResult;
  }

  std::vector<InvalidReading> invalid_readings;
  try
  {
    std::fstream file{invalid_readings_path};
    invalid_readings = nlohmann::json::parse(file)
                           .get<std::vector<InvalidReading>>();
  }
  catch (const nlohmann::json::exception &ex)
  {
    logging_handler::error(FUNC_PREAMBLE, "{}", ex.what());
    return TaskResult::kError;
  }

  for (const auto& invalid_reading : invalid_readings)
  {
    const int meter_id = invalid_reading.meter_id;
    const std::string &invalid_reading_date = invalid_reading.date_time;

    std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> tp;
    std::stringstream ss{invalid_reading_date};
    ss >> date::parse("%Y-%m-%dT%T", tp);

    std::stringstream invalid_reading_date_start;
    invalid_reading_date_start << date::format(kTimeFormat,
                                               date::make_zoned(
                                                 date::current_zone(),
                                                 date::local_seconds{std::chrono::floor<date::days>(tp).time_since_epoch()}
                                               ));

    std::stringstream invalid_reading_date_end;
    invalid_reading_date_end << date::format(kTimeFormat,
                                             date::make_zoned(
                                               date::current_zone(),
                                               date::local_seconds{tp.time_since_epoch()}
                                             ));

    logging_handler::info(FUNC_PREAMBLE,
                          "Syncing invalid reading from {} to {}",
                          invalid_reading_date_start.str(),
                          invalid_reading_date_end.str());

    try
    {
      MeterReadingResponse meter_reading_response
        = sendMeterReadingRequest(
            meter_id,
            invalid_reading_date_start.str(),
            invalid_reading_date_end.str()
          );

      if (meter_reading_response.readings.empty())
      {
        m_invalid_readings.emplace_back(InvalidReading{meter_id, invalid_reading_date});
      }
      else
      {
        insertIntoMeterReadings(meter_id, meter_reading_response);
      }
    }
    catch(const common::CommonException &ex)
    {
      logging_handler::error(FUNC_PREAMBLE, "CURL: {}", ex.what());

      m_invalid_readings.emplace_back(InvalidReading{meter_id, invalid_reading_date});
    }
    catch(const std::exception &ex)
    {
      logging_handler::error(FUNC_PREAMBLE, "{}", ex.what());

      m_invalid_readings.emplace_back(InvalidReading{meter_id, invalid_reading_date});
    }
  }

  std::filesystem::remove(kInvalidReadingsFilename);
  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

void StromNetzeGrazPlugin::insertIntoMeterReadings(int meter_id,
                                                   MeterReadingResponse meter_reading_response)
{
  auto it = m_meter_readings.find(meter_id);
  if (it == m_meter_readings.end())
  {
    m_meter_readings.emplace(meter_id, meter_reading_response);
  }
  else
  {
    it->second.readings.insert(it->second.readings.end(),
                               meter_reading_response.readings.begin(),
                               meter_reading_response.readings.end());
  }
}


//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::publishProtobufMessage()
{
  m_protobuf_publish_pending = true;
  const message::ScrapeService protobuf_message = createProtobufMessage();

  bool success = m_mqtt_callback(m_serialized_protobuf, protobuf_message);
  if (!success)
  {
    return TaskResult::kError;
  }

  std::unique_lock lock{m_mutex};
  m_condition_variable.wait(lock, [this]() -> bool { return !m_protobuf_publish_pending; });

  logging_handler::info(FUNC_PREAMBLE, "Published protobuf message");
  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::saveInvalidReadings()
{
  if (m_invalid_readings.empty())
  {
    return TaskResult::kNoResult;
  }

  nlohmann::json invalid_readings_json = m_invalid_readings;

  std::fstream invalid_readings_file;

  const std::filesystem::path invalid_readings_path = fmt::format("{}{}", m_config->local_cache_directory.string(), kInvalidReadingsFilename);
  invalid_readings_file.open(invalid_readings_path, std::fstream::out);

  invalid_readings_file << invalid_readings_json;

  invalid_readings_file.close();

  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

StromNetzeGrazPlugin::TaskResult StromNetzeGrazPlugin::cleanupMembers()
{
  m_installations.clear();
  m_meter_readings_data.clear();
  m_meter_readings.clear();
  m_invalid_readings.clear();

  return TaskResult::kSuccess;
}

//------------------------------------------------------------------------------

template<class T, class U>
T StromNetzeGrazPlugin::sendPostRequest(const std::string &request_url, const U &payload)
{
  const std::vector<std::string> headers = m_login_token.empty() ?
    std::vector<std::string> {
      kContentTypeHeader,
    } :
    std::vector<std::string> {
      kContentTypeHeader,
      fmt::format("Authorization: Bearer {}", m_login_token)
    };

  constexpr const char *kUserAgent = "Mozilla/5.0 "
                                     "(X11; Linux x86_64; rv:109.0) "
                                     "Gecko/20100101 Firefox/119.0";

  nlohmann::json json_request = payload;

  const std::vector<char> http_response =
    m_http_request_adapter->guard()
                            .defineUserAgent(kUserAgent)
                            .defineRequestCaCertPath(m_config->ca_cert_path)
                            .defineRequestHeaders(headers)
                            .definePostRequestParameters(json_request.dump())
                            .defineRequestUrl(request_url)
                            .send();

  try
  {
    return nlohmann::json::parse(http_response.data()).get<T>();
  }
  catch (const nlohmann::json::exception &ex)
  {
    throw common::CommonException(fmt::format("JSON exception: {} data: {}",
                                              ex.what(), http_response.data()));
  }
}

} // namespace rpi_utilities
