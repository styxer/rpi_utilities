/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: scrape_service
*
************************************************/

// local includes
#include "scrapeengine.h"
#include "scrapeengineconfig.h"

// common includes
#include "commonexception.h"
#include "filelogginghandler.h"
#include "httprequestadapter.h"
#include "logginghandlerinterface.h"
#include "logginghelper.h"
#include "mqttmessageadapter.h"
#include "processstarter.h"
#include "rpimacros.h"

// system includes
#include <format>
#include <fstream>
#include <memory>
#include <span>

//------------------------------------------------------------------------------

void loggingCallback(logging_handler::LoggingHandlerInterface &logging_handler_interface,
                     const logging_handler::LoggingHandlerInterface::LoggingLevel &level,
                     const std::string &msg)
{
  logging_handler_interface.log(level, msg);
}

//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  const std::span<char*> args{argv, static_cast<size_t>(argc)};
  if (argc != 2)
  {
    std::cerr << "Usage: scrape_service <config_file_path>\n";
    return -1;
  }

  std::unique_ptr<rpi_utilities::scrape::ScrapeEngineConfig> config;
  try
  {
    config = std::make_unique<rpi_utilities::scrape::ScrapeEngineConfig>(args[1]);
  }
  catch (const common::CommonException &ex)
  {
    std::cerr << std::format("Error while parsing configuration JSON: {}\n", ex.what());
    return -1;
  }

  logging_handler::FileLoggingHandler file_logging_handler {
    "scrape_service",
    config->getLogDirectory()
  };

  file_logging_handler.setLoggingLevel(config->getLogLevel());

  logging_handler::setLoggingCallback(
    [&](const logging_handler::LoggingHandlerInterface::LoggingLevel &logging_level,
        const std::string &message) {
      loggingCallback(file_logging_handler, logging_level, message);
    }
  );

  std::shared_ptr<rpi_utilities::http_request::HttpRequestAdapter> http_request_adapter =
    std::make_shared<rpi_utilities::http_request::HttpRequestAdapter>();

  auto external_modules = std::make_unique<rpi_utilities::scrape::ScrapeEngine::ExternalModules>(
    http_request_adapter,
    std::move(config)
  );

  rpi_utilities::scrape::ScrapeEngine engine{std::move(external_modules)};

  engine.start();

  while (std::tolower(std::cin.get()) != 'q');

  return 0;
}
