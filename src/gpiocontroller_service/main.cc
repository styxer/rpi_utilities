/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller_service
*
************************************************/

// local includes
#include "gpiocontrollerengine.h"
#include "gpiocontrollerengineconfig.h"

// common includes
#include "commonexception.h"
#include "filelogginghandler.h"
#include "logginghelper.h"

// system includes
#include <iostream>
#include <memory>
#include <span>
#include <unistd.h>

//------------------------------------------------------------------------------

void loggingCallback(logging_handler::LoggingHandlerInterface &logging_handler_interface,
                     const logging_handler::LoggingHandlerInterface::LoggingLevel &log_level,
                     const std::string &msg)
{
  logging_handler_interface.log(log_level, msg);
}

//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  const std::span<char*> args{argv, static_cast<size_t>(argc)};
  if (getuid() != 0)
  {
    std::cerr << "You need to run this service as root!\n";
    return -1;
  }

  if (argc != 2)
  {
    std::cerr << "Usage: gpiocontroller_service <config_file_path>\n";
    return -1;
  }

  std::unique_ptr<rpi_utilities::gpio::GpioControllerEngineConfig> config;
  try
  {
    config = std::make_unique<rpi_utilities::gpio::GpioControllerEngineConfig>(args[1]);
  }
  catch (const common::CommonException &ex)
  {
    std::cerr << std::format("Error while parsing configuration TOML: {}\n", ex.what());
    return -1;
  }

  logging_handler::FileLoggingHandler file_logging_handler {
    "gpio_controller_service",
    config->getLogDirectory()
  };

  file_logging_handler.setLoggingLevel(config->getLogLevel());

  logging_handler::setLoggingCallback(
    [&](const logging_handler::LoggingHandlerInterface::LoggingLevel &logging_level,
        const std::string &message) {
      loggingCallback(file_logging_handler, logging_level, message);
    }
  );

  auto external_modules = std::make_unique<rpi_utilities::gpio::GpioControllerEngine::ExternalModules>(std::move(config));

  rpi_utilities::gpio::GpioControllerEngine gpio_controller_engine {
    std::move(external_modules)
  };

  gpio_controller_engine.start();

  while (std::tolower(std::cin.get()) != 'q');
  return 0;
}
