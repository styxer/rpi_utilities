# GPIO Controller Service

## General

This service is designed to controll the GPIOs of the RPi 4 considering configured conditions.
I used the [wiringpi][1] library for implementation and therefore the numbering of this library needs to be used.

## Pin Out

|                                                               ||||||
|:------:|:---------:|:-------------:|--------:|:----------:|:------:|
|   wPi  |  Name     |          Physical      ||  Name      |  wPi   |
|        |  3.3V     |      1     | 2          |   5V       |        |
|   8    |  SDA.1    |      3     | 4          |   5V       |        |
|   9    |  SCL.1    |      5     | 6          |   GND      |        |
|   7    |  GPIO. 7  |      7     | 8          |   TxD      |   15   |
|        |  GND      |      9     | 10         |   RxD      |   16   |
|   0    |  GPIO. 0  |      11    | 12         |   GPIO. 1  |   1    |
|   2    |  GPIO. 2  |      13    | 14         |   GND      |        |
|   3    |  GPIO. 3  |      15    | 16         |   GPIO. 4  |   4    |
|        |  3.3V     |      17    | 18         |   GPIO. 5  |   5    |
|   12   |  MOSI     |      19    | 20         |   GND      |        |
|   13   |  MISO     |      21    | 22         |   GPIO. 6  |   6    |
|   14   |  SCLK     |      23    | 24         |   CE0      |   10   |
|        |  GND      |      25    | 26         |   CE1      |   11   |
|   30   |  SDA. 0   |      27    | 28         |   SCL. 0   |   32   |
|   21   |  GPIO. 21 |      29    | 30         |   GND      |        |
|   22   |  GPIO. 22 |      31    | 32         |   GPIO. 26 |   26   |
|   23   |  GPIO. 23 |      33    | 34         |   GND      |        |
|   24   |  GPIO. 24 |      35    | 36         |   GPIO. 27 |   27   |
|   25   |  GPIO. 25 |      37    | 38         |   GPIO .28 |   28   |
|        |  GND      |      39    | 40         |   GPIO. 29 |   29   |

## Configuration

As already mentioned, it is possible to configure the service. Please have a look [here][1].

[1]: ../../config_templates/gpiocontroller_service.toml
