#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes

// common includes
#include "commonexception.h"
#include "configbase.h"
#include "rpimacros.h"
#include <toml++/toml.hpp>

// system includes
#include <map>
#include <string>
#include <utility>
#include <variant>
#include <vector>

//-----------------------------------------------------------------------------

using GpioPluginType = std::map<std::string,
                                std::variant<size_t, double, std::string>
                               >;
using GpioPluginsType = std::vector<GpioPluginType>;

//-----------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioControllerEngineConfig : public config::ConfigBase
{
public:
  GpioControllerEngineConfig(const GpioControllerEngineConfig &) = default;
  GpioControllerEngineConfig(GpioControllerEngineConfig &&) = default;
  GpioControllerEngineConfig &operator=(const GpioControllerEngineConfig &) = default;
  GpioControllerEngineConfig &operator=(GpioControllerEngineConfig &&) = default;

  explicit GpioControllerEngineConfig(const std::filesystem::path &config_file_path)
    : ConfigBase{config_file_path}
  {
    const toml::array *plugins_array = m_toml[kGpioPluginsName].as_array();
    plugins_array->for_each([this](auto &&element) {
        if constexpr (!toml::is_table<decltype(element)>)
        {
          return;
        }

        const toml::table *table = element.as_table();

        const std::optional<std::string> mqtt_topic = (*table)[kPluginsMqttTopicName].value<std::string>();
        const std::optional<size_t> gpio_number = (*table)[kPluginsGpioNumberName].value<size_t>();
        const std::optional<double> threshold = (*table)[kPluginsThresholdName].value<double>();
        const std::optional<std::string> plugin_path = (*table)[kPluginsPluginPathName].value<std::string>();
        const std::optional<std::string> plugin_name = (*table)[kPluginsPluginNameName].value<std::string>();

        m_gpio_plugins.emplace_back(GpioPluginType {
          {kPluginsMqttTopicName, *mqtt_topic},
          {kPluginsGpioNumberName, *gpio_number},
          {kPluginsThresholdName, *threshold},
          {kPluginsPluginPathName, *plugin_path},
          {kPluginsPluginNameName, *plugin_name}
        });
    });
  }

  explicit GpioControllerEngineConfig(std::string &&mqtt_broker_address,
                                      std::string &&mqtt_client_id,
                                      std::string &&mqtt_user,
                                      std::string &&mqtt_password,
                                      std::set<std::string> &&mqtt_topic_ids,
                                      std::string &&heartbeat_topic,
                                      std::chrono::milliseconds heartbeat_cycle,
                                      std::filesystem::path &&log_directory,
                                      logging_handler::LoggingHandlerInterface::LoggingLevel log_level,
                                      GpioPluginsType &&gpio_conditions)
      : config::ConfigBase {
          std::move(mqtt_broker_address),
          std::move(mqtt_client_id),
          std::move(mqtt_user),
          std::move(mqtt_password),
          std::move(mqtt_topic_ids),
          std::move(heartbeat_topic),
          heartbeat_cycle,
          std::move(log_directory),
          log_level
        },
        m_gpio_plugins{std::move(gpio_conditions)}
  {}
  ~GpioControllerEngineConfig() override = default;

  GET_VAL(GpioPlugins, GpioPluginsType, m_gpio_plugins)

  bool operator==(GpioControllerEngineConfig &other)
  {
    return other.getMqttBrokerAddress() == getMqttBrokerAddress() &&
           other.getMqttClientId() == getMqttClientId()           &&
           other.getMqttUser() == getMqttUser()                   &&
           other.getMqttPassword() == getMqttPassword()           &&
           other.getMqttTopicIds() == getMqttTopicIds()           &&
           other.getHeartbeatTopic() == getHeartbeatTopic()       &&
           other.getHeartbeatCycle() == getHeartbeatCycle()       &&
           other.getGpioPlugins() == getGpioPlugins()             &&
           other.getLogDirectory() == getLogDirectory()           &&
           other.getLogLevel() == getLogLevel();
  }

  bool operator!=(GpioControllerEngineConfig &other)
  {
    return !(other == *this);
  }

private:
  static constexpr const char *kGpioSectionName = "gpio";
  static constexpr const char *kGpioPluginsName = "plugins";

  static constexpr const char *kPluginsMqttTopicName = "mqtt_topic";
  static constexpr const char *kPluginsGpioNumberName = "gpio_number";
  static constexpr const char *kPluginsThresholdName = "threshold";
  static constexpr const char *kPluginsPluginPathName = "path";
  static constexpr const char *kPluginsPluginNameName = "name";

  GpioPluginsType m_gpio_plugins;
};

} // namespace rpi_utilities::gpio
