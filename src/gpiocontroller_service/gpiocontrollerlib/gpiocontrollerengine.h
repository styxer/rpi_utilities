#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpiocontrollerengineinterface.h"
#include "gpiocontrollerengineconfig.h"
#include "controllers/gpiocontrollerinterface.h"
#include "gpioadapterinterface.h"

// common includes
#include "heartbeat.h"

// system includes
#include <chrono>
#include <cstdlib>
#include <map>
#include <memory>
#include <string>
#include <variant>

//------------------------------------------------------------------------------

namespace mqttmessage
{

class MqttMessageAdapterInterface;

}

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioControllerEngine : public GpioControllerEngineInterface
{
public:
  struct ExternalModules
  {
    std::unique_ptr<GpioControllerEngineConfig> config;
  };

  GpioControllerEngine(const GpioControllerEngine&) = delete;
  GpioControllerEngine(GpioControllerEngine &&) = delete;
  GpioControllerEngine &operator=(const GpioControllerEngine &) = delete;
  GpioControllerEngine &operator=(GpioControllerEngine &&) = delete;

  explicit GpioControllerEngine(std::unique_ptr<ExternalModules> external_modules);
  ~GpioControllerEngine() override = default;

  //! \see interface
  void start() override;

  //! \see interface
  void stop() override;

private:
  std::unique_ptr<ExternalModules> m_external_modules;

  std::map<std::string, std::pair<std::map<std::string,
                                           std::variant<size_t,
                                                        double,
                                                        std::string>
                                          >,
                        std::shared_ptr<GpioControllerInterface>>> m_subscribed_topics; //< key: mqtt_topic ID

  std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> m_mqtt_message_adapter;
  common::HeartBeat m_heartbeat_timer;

  bool m_engine_stopped = false;

  std::shared_ptr<GpioAdapterInterface> m_gpio_adapter_interface;

  std::vector<char> m_serialized_output;

  std::mutex m_gpio_pin_state_mutex;
  std::condition_variable m_gpio_pin_state_condition;

  void messageReceived(const std::string &topic, const char *payload, size_t size);
  void connectionStatusChanged(bool connected);
  void changedPinState(const size_t &pin_number,
                       const GpioAdapterInterface::GpioState &state,
                       const std::chrono::seconds &timestamp);
  void protobufPublishCompleted(bool completed, const std::vector<std::string> &topics);
};

} // namespace rpi_utilities::gpio
