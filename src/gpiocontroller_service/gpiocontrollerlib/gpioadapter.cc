/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpioadapter.h"

// common includes
#include "logginghelper.h"

// system includes
#include "wiringPi.h"
#include <chrono>
#include <utility>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

GpioAdapter::GpioAdapter(std::function<void (size_t,
                                             GpioAdapterInterface::GpioState,
                                             std::chrono::seconds)> pin_state_changed_callback)
  : m_pin_state_changed_callback{std::move(pin_state_changed_callback)}
{
  wiringPiSetup();
}

//------------------------------------------------------------------------------

void GpioAdapter::setPinMode(const size_t &pin_number, const GpioMode &gpio_mode)
{
  auto pin_mode_mapping_it = m_pin_mode_mapping.find(pin_number);
  if (pin_mode_mapping_it != m_pin_mode_mapping.cend() && pin_mode_mapping_it->second == gpio_mode)
  {
    return;
  }

  pinMode(static_cast<int>(pin_number), gpio_mode);
  m_pin_mode_mapping.insert({pin_number, gpio_mode});
}

//------------------------------------------------------------------------------

bool GpioAdapter::enablePin(const size_t &pin_number)
{
  if (!verifyPinMode(pin_number, GpioMode::kOutput))
  {
    return false;
  }

  const std::chrono::seconds timestamp
    = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());

  m_pin_state_changed_callback(pin_number, GpioState::kHigh, timestamp);
  digitalWrite(static_cast<int>(pin_number), GpioState::kHigh);

  return true;
}

//------------------------------------------------------------------------------

bool GpioAdapter::disablePin(const size_t &pin_number)
{
  if (!verifyPinMode(pin_number, GpioMode::kOutput))
  {
    return false;
  }

  const std::chrono::seconds timestamp
    = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());

  m_pin_state_changed_callback(pin_number, GpioState::kLow, timestamp);
  digitalWrite(static_cast<int>(pin_number), GpioState::kLow);

  return true;
}

//------------------------------------------------------------------------------

GpioAdapter::GpioState GpioAdapter::readPin(const size_t &pin_number) const
{
  if (!verifyPinMode(pin_number, GpioMode::kInput))
  {
    return GpioState::kError;
  }

  return static_cast<GpioState>(digitalRead(static_cast<int>(pin_number)));
}

//------------------------------------------------------------------------------

bool GpioAdapter::verifyPinMode(const size_t &pin_number, const GpioMode &expected_mode) const
{
  auto pin_mode_mapping_it = m_pin_mode_mapping.find(pin_number);
  if (pin_mode_mapping_it != m_pin_mode_mapping.cend() &&
      pin_mode_mapping_it->second == expected_mode)
  {
    return true;
  }

  logging_handler::warning(
    FUNC_PREAMBLE,
    "GPIO with number {} has not been configured or it is not in {} mode",
    std::to_string(pin_number), std::to_string(expected_mode));

  return false;
}

} // namespace rpi_utilities::gpio
