#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes

// common includes

// system includes
#include <cstdlib>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioControllerEngineInterface
{
public:
  GpioControllerEngineInterface(const GpioControllerEngineInterface &) = delete;
  GpioControllerEngineInterface(GpioControllerEngineInterface &&) = delete;
  GpioControllerEngineInterface &operator=(const GpioControllerEngineInterface &) = delete;
  GpioControllerEngineInterface &operator=(GpioControllerEngineInterface &&) = delete;

  GpioControllerEngineInterface() = default;
  virtual ~GpioControllerEngineInterface() = default;

  //! Starts the engine.
  virtual void start() = 0;

  //! Stops the engine.
  virtual void stop() = 0;
};

} // namespace rpi_utilities::gpio
