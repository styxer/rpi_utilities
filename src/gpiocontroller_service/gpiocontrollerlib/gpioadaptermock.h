#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpioadapterinterface.h"

// common includes

// system includes
#include <cstdlib>
#include <map>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioAdapterMock : public GpioAdapterInterface
{
public:
  GpioAdapterMock(const GpioAdapterMock &) = delete;
  GpioAdapterMock(GpioAdapterMock &&) = delete;
  GpioAdapterMock &operator=(const GpioAdapterMock &) = delete;
  GpioAdapterMock &operator=(GpioAdapterMock &&) = delete;

  explicit GpioAdapterMock(const GpioState &read_pin_value);
   ~GpioAdapterMock() override = default;

  void setPinMode(const size_t &pin_number, const GpioMode &gpio_mode) override;

  bool enablePin(const size_t &pin_number) override;
  bool disablePin(const size_t &pin_number) override;

  GpioState readPin(const size_t &pin_number) const override;

  bool lastOperationSuccessful() const;

private:
  bool verifyPinMode(const size_t &pin_number, const GpioMode &expected_mode) const;

  std::map<size_t, GpioMode> m_pin_mode_mapping; //< key: pin number
  const GpioState           &m_read_pin_value;

  mutable bool m_last_operation_successful = false;
};

} // namespace rpi_utilities::gpio
