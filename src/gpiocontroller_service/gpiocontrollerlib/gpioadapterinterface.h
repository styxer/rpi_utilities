#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes

// common includes

// system includes
#include <cstdlib>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioAdapterInterface
{
public:
  enum GpioState
  {
    kLow = 0,
    kHigh = 1,
    kError = -1
  };

  enum GpioMode
  {
    kInput = 0,
    kOutput = 1
  };

  GpioAdapterInterface(const GpioAdapterInterface &) = delete;
  GpioAdapterInterface(GpioAdapterInterface &&) = delete;
  GpioAdapterInterface &operator=(const GpioAdapterInterface &) = delete;
  GpioAdapterInterface &operator=(GpioAdapterInterface &&) = delete;

  GpioAdapterInterface() = default;
  virtual ~GpioAdapterInterface() = default;

  virtual void setPinMode(const size_t &pin_number, const GpioMode &gpio_mode) = 0;

  virtual bool enablePin(const size_t &pin_number) = 0;
  virtual bool disablePin(const size_t &pin_number) = 0;

  [[nodiscard]] virtual GpioState readPin(const size_t &pin_number) const = 0;
};

} // namespace rpi_utilities::gpio
