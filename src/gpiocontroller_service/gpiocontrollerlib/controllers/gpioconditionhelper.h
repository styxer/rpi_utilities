#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpiocontrollerengineconfig.h"

// common includes

// system includes
#include <set>
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

bool verifyGpioCondition(const GpioPluginType &gpio_condition,
                         const std::set<std::string> &required_keys);

} // namespace rpi_utilities
