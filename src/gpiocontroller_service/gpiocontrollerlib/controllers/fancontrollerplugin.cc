/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "fancontrollerplugin.h"
#include "gpioconditionhelper.h"

// common includes
#include "cputemperature.pb.h"
#include "logginghelper.h"
#include "rpimacros.h"

// system includes
#include <set>

//------------------------------------------------------------------------------
namespace rpi_utilities::gpio {

DEFINE_PLUGIN(FanControllerPlugin, PluginAllocator, PluginDeallocator) // NOLINT

}

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

void FanControllerPlugin::initialize(std::shared_ptr<GpioAdapterInterface> gpio_adatper)
{
  m_gpio_adapter = gpio_adatper;
}

//------------------------------------------------------------------------------

void FanControllerPlugin::evaluateGpioCondition(const GpioPluginType &gpio_condition,
                                                const char *message,
                                                size_t size)
{
  if (!m_gpio_adapter)
  {
    logging_handler::info(FUNC_PREAMBLE, "Plugin needs to be initialized at first!");
    return;
  }

  const std::set<std::string> required_keys = {"mqtt_topic",
                                               "gpio_number",
                                               "threshold",
                                               "path"};

  if (!verifyGpioCondition(gpio_condition, required_keys))
  {
    return;
  }

  message::CpuTemperature cpu_temperature_message;
  if (!cpu_temperature_message.ParseFromArray(message, static_cast<int>(size)))
  {
    logging_handler::warning(FUNC_PREAMBLE, "Could not parse protobuf message.");
    return;
  }

  const size_t gpio_number = std::get<size_t>(gpio_condition.at("gpio_number"));
  const double threshold   = std::get<double>(gpio_condition.at("threshold"));

  m_gpio_adapter->setPinMode(gpio_number, GpioAdapterInterface::GpioMode::kOutput);

  if (cpu_temperature_message.cpu_temperature() >= threshold)
  {
    logging_handler::info(FUNC_PREAMBLE,
                          "CPU temperature of {} °C exceeded threshold of {} °C. Enabling fan!",
                          cpu_temperature_message.cpu_temperature(),
                          threshold);

    m_gpio_adapter->enablePin(gpio_number);
  }
  else
  {
    logging_handler::info(FUNC_PREAMBLE,
                          "CPU temperature of {} °C did not exceed threshold of {} °C. Disabling fan!",
                          cpu_temperature_message.cpu_temperature(),
                          threshold);

    m_gpio_adapter->disablePin(gpio_number);
  }
}

} // namespace rpi_utilities::gpio
