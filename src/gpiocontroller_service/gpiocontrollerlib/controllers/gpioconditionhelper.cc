/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpioconditionhelper.h"

// common includes
#include "logginghelper.h"

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

bool verifyGpioCondition(const GpioPluginType &gpio_condition,
                         const std::set<std::string> &required_keys)
{
  std::string func_preamble = FUNC_PREAMBLE;
  return std::all_of(required_keys.cbegin(), required_keys.cend(),
                     [&gpio_condition, &func_preamble](const std::string &required_key) {
                       if (!gpio_condition.contains(required_key))
                       {
                         logging_handler::warning(
                             func_preamble,
                             "Missing '{}' key within GPIO condition.",
                             required_key);
                         return false;
                       }

                       return true;
                     });
}

} // namespace rpi_utilities
