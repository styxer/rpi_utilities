#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpioadapterinterface.h"
#include "gpiocontrollerengineconfig.h"

// common includes

// system includes
#include <memory>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioControllerInterface
{
public:
  GpioControllerInterface(const GpioControllerInterface &) = delete;
  GpioControllerInterface(GpioControllerInterface &&) = delete;
  GpioControllerInterface& operator=(const GpioControllerInterface &) = delete;
  GpioControllerInterface& operator=(GpioControllerInterface &&) = delete;

  GpioControllerInterface() = default;
  virtual ~GpioControllerInterface() = default;

  //! This method is used to initialize all needed members.
  //! NOTE: This method is necessary because of the plugin infrastructure.
  //! \param gpio_adapter The GPIO adapter which will be used to controll the gpios.
  virtual void initialize(std::shared_ptr<GpioAdapterInterface> gpio_adapter) = 0;

  //! This method evaluates the given GPIO condition and decides if the given
  //! GPIO should be enabled or disabled.
  //! NOTE: The GPIO condition needs to meet specific requirements!
  //! \param gpio_condition The GPIO condition which should be evaluated.
  //! \param message        The received protobuf message.
  //! \param size           The size of the protobuf message.
  virtual void evaluateGpioCondition(const GpioPluginType &gpio_condition,
                                     const char *message,
                                     size_t size) = 0;

protected:
  std::shared_ptr<GpioAdapterInterface> m_gpio_adapter; // NOLINT
};

} // namespace rpi_utilities
