#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpiocontrollerinterface.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class FanControllerPlugin : public GpioControllerInterface
{
public:
  FanControllerPlugin(const FanControllerPlugin &) = delete;
  FanControllerPlugin(FanControllerPlugin &&) = delete;
  FanControllerPlugin& operator=(const FanControllerPlugin &) = delete;
  FanControllerPlugin& operator=(FanControllerPlugin &&) = delete;

  explicit FanControllerPlugin() = default;
  ~FanControllerPlugin() override = default;

  //! \see interface
  void initialize(std::shared_ptr<GpioAdapterInterface> gpio_adatper) override;

  //! \see interface
  void evaluateGpioCondition(const GpioPluginType &gpio_condition,
                             const char *message,
                             size_t size) override;
};

} // namespace rpi_utilities::gpio
