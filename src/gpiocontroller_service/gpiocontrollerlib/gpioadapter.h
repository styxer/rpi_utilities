#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpioadapterinterface.h"

// common includes

// system includes
#include <chrono>
#include <cstdlib>
#include <functional>
#include <map>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

class GpioAdapter : public GpioAdapterInterface
{
public:
  GpioAdapter(const GpioAdapter &) = delete;
  GpioAdapter(GpioAdapter &&) = delete;
  GpioAdapter &operator=(const GpioAdapter &) = delete;
  GpioAdapter &operator=(GpioAdapter &&) = delete;

  explicit GpioAdapter(std::function<void (size_t,
                                           GpioAdapterInterface::GpioState,
                                           std::chrono::seconds)> pin_state_changed_callback);
   ~GpioAdapter() override = default;

  void setPinMode(const size_t &pin_number, const GpioMode &gpio_mode) override;

  bool enablePin(const size_t &pin_number) override;
  bool disablePin(const size_t &pin_number) override;

  [[nodiscard]] GpioState readPin(const size_t &pin_number) const override;

private:
  [[nodiscard]] bool verifyPinMode(const size_t &pin_number, const GpioMode &expected_mode) const;

  std::map<size_t, GpioMode> m_pin_mode_mapping; //< key: pin number
  std::function<void (size_t,
                      GpioAdapterInterface::GpioState,
                      std::chrono::seconds)> m_pin_state_changed_callback;
};

} // namespace rpi_utilities::gpio
