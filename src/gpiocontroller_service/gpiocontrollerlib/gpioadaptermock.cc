/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpioadaptermock.h"

// common includes
#include "rpimacros.h"

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

GpioAdapterMock::GpioAdapterMock(const GpioState &read_pin_value)
  : m_read_pin_value{read_pin_value}
{
}

//------------------------------------------------------------------------------

void GpioAdapterMock::setPinMode(const size_t &pin_number, const GpioMode &gpio_mode)
{
  auto pin_mode_mapping_it = m_pin_mode_mapping.find(pin_number);
  if (pin_mode_mapping_it == m_pin_mode_mapping.cend() || pin_mode_mapping_it->second != gpio_mode)
  {
    m_pin_mode_mapping.insert({pin_number, gpio_mode});
  }
}

//------------------------------------------------------------------------------

bool GpioAdapterMock::enablePin(const size_t &pin_number)
{
  if (!verifyPinMode(pin_number, GpioMode::kOutput))
  {
    m_last_operation_successful = false;
    return m_last_operation_successful;
  }

  m_last_operation_successful = true;
  return m_last_operation_successful;
}

//------------------------------------------------------------------------------

bool GpioAdapterMock::disablePin(const size_t &pin_number)
{
  if (!verifyPinMode(pin_number, GpioMode::kOutput))
  {
    m_last_operation_successful = false;
    return m_last_operation_successful;
  }

  m_last_operation_successful = true;
  return m_last_operation_successful;
}

//------------------------------------------------------------------------------

GpioAdapterMock::GpioState GpioAdapterMock::readPin(const size_t &pin_number) const
{
  if (!verifyPinMode(pin_number, GpioMode::kInput))
  {
    return GpioState::kError;
  }

  return m_read_pin_value;
}

//------------------------------------------------------------------------------

bool GpioAdapterMock::lastOperationSuccessful() const
{
  return m_last_operation_successful;
}

//------------------------------------------------------------------------------

bool GpioAdapterMock::verifyPinMode(const size_t &pin_number, const GpioMode &expected_mode) const
{
  auto pin_mode_mapping_it = m_pin_mode_mapping.find(pin_number);
  return pin_mode_mapping_it != m_pin_mode_mapping.cend() &&
         pin_mode_mapping_it->second == expected_mode;
}

} // namespace rpi_utilities::gpio
