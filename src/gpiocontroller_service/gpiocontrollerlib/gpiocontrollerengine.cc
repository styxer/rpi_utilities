/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontroller
*
************************************************/

// local includes
#include "gpiocontrollerengine.h"
#include "gpioadapter.h"

// common includes
#include "commonexception.h"
#include "gpiocontrollerengineconfig.h"
#include "gpiopinstate.pb.h"
#include "logginghelper.h"
#include "rpiutilities.h"

// system includes
#include <chrono>
#include <memory>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

GpioControllerEngine::GpioControllerEngine(
  std::unique_ptr<ExternalModules> external_modules)
  : m_external_modules{std::move(external_modules)},
    m_mqtt_message_adapter{std::make_shared<mqttmessage::MqttMessageAdapter>(
      [this](const std::string &topic, const char *payload, size_t size) {
        messageReceived(topic, payload, size);
      },
      [this](bool connected) { connectionStatusChanged(connected); },
      [this](bool completed, const std::vector<std::string> &topics) {
        protobufPublishCompleted(completed, topics);
      },
      m_external_modules->config->getMqttBrokerAddress(),
      m_external_modules->config->getMqttClientId())},
    m_heartbeat_timer{
      m_mqtt_message_adapter,
      m_external_modules->config->getHeartbeatTopic(),
      m_external_modules->config->getMqttClientId(),
      m_external_modules->config->getHeartbeatCycle()
    },
    m_gpio_adapter_interface{std::make_shared<GpioAdapter>(
      [this](const size_t &pin_number,
             const GpioAdapterInterface::GpioState &state,
             const std::chrono::seconds &timestamp) {
        changedPinState(pin_number, state, timestamp);
      })}
{
}

//------------------------------------------------------------------------------

void GpioControllerEngine::start()
{
  m_mqtt_message_adapter->connect(m_external_modules->config->getMqttUser(),
                                  m_external_modules->config->getMqttPassword());
}

//------------------------------------------------------------------------------

void GpioControllerEngine::stop()
{
  for (const auto &subscribed_topic : m_subscribed_topics)
  {
    m_mqtt_message_adapter->unsubscribe(subscribed_topic.first);
  }

  m_mqtt_message_adapter->disconnect();
  m_engine_stopped = true;
}

//------------------------------------------------------------------------------

void GpioControllerEngine::messageReceived(const std::string &topic,
                                           const char *payload,
                                           const size_t size)
{
  logging_handler::debug(FUNC_PREAMBLE, "received message for topic: {}", topic);
  auto subscribed_topics_it = m_subscribed_topics.find(topic);
  if (subscribed_topics_it == m_subscribed_topics.cend())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Did not find config for this topic, discarding.");
    return;
  }

  const std::map<std::string, std::variant<size_t, double, std::string>>
    &gpio_condition = subscribed_topics_it->second.first;

  std::shared_ptr<GpioControllerInterface> gpio_controller_plugin
    = subscribed_topics_it->second.second;

  gpio_controller_plugin->evaluateGpioCondition(gpio_condition, payload, size);
}

//------------------------------------------------------------------------------

void GpioControllerEngine::connectionStatusChanged(const bool connected)
{
  if (connected)
  {
    logging_handler::info(FUNC_PREAMBLE, "Connected to broker!");

    const GpioPluginsType gpio_plugins
      = m_external_modules->config->getGpioPlugins();

    logging_handler::info(FUNC_PREAMBLE, "{} gpio conditions have been registered.", gpio_plugins.size());

    for (const auto &gpio_condition : gpio_plugins)
    {
      auto gpio_condition_it = gpio_condition.find("mqtt_topic");
      if (gpio_condition_it == gpio_condition.cend())
      {
        logging_handler::warning(FUNC_PREAMBLE, "GPIO condition config does not contain key 'mqtt_topic' discarding.");
        continue;
      }

      if (!gpio_condition.contains("path"))
      {
        logging_handler::warning(FUNC_PREAMBLE, "The responsible plugin for this topic has not been configured, discarding");
        continue;
      }

      const std::string mqtt_topic = std::get<std::string>(gpio_condition_it->second);
      const std::string plugin_path = std::get<std::string>(gpio_condition.at("path"));

      std::shared_ptr<GpioControllerInterface> gpio_controller_plugin;
      try
      {
        gpio_controller_plugin = loadPlugin<GpioControllerInterface>(plugin_path);
      }
      catch (const common::CommonException &ex)
      {
        logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
        continue;
      }

      gpio_controller_plugin->initialize(m_gpio_adapter_interface);

      logging_handler::info(FUNC_PREAMBLE, "subscribing to topic: {}", mqtt_topic);

      m_mqtt_message_adapter->subscribe(mqtt_topic,
                                        mqttmessage::QualityOfServiceMode::kExactlyOnce);

      m_subscribed_topics.insert({mqtt_topic,
                                  std::pair<std::map<std::string,
                                                     std::variant<size_t,
                                                                  double,
                                                                  std::string>>,
                                            std::shared_ptr<GpioControllerInterface>>{gpio_condition,
                                                                                      gpio_controller_plugin}
                                 });
    }

    m_heartbeat_timer.start();
  }
  else
  {
    logging_handler::info(FUNC_PREAMBLE, "Connection lost. Trying to reconnect!");
    m_heartbeat_timer.stop();
    m_subscribed_topics.clear();
  }
}

//-----------------------------------------------------------------------------

void GpioControllerEngine::changedPinState(const size_t &pin_number,
                                           const GpioAdapterInterface::GpioState &state,
                                           const std::chrono::seconds &timestamp)
{
  try
  {
    message::GpioPinState gpio_state_message;
    gpio_state_message.set_pin_number(pin_number);
    gpio_state_message.set_pin_state(state);
    gpio_state_message.set_timestamp(timestamp.count());
    gpio_state_message.set_node_id(m_external_modules->config->getMqttClientId());

    m_serialized_output.clear();
    m_serialized_output.resize(gpio_state_message.ByteSizeLong());
    serializeProtobufMessage<message::GpioPinState>(gpio_state_message,
                                                    m_serialized_output.data(),
                                                    gpio_state_message.ByteSizeLong());

    m_mqtt_message_adapter->publish(*m_external_modules->config->getMqttTopicIds().begin(),
                                    m_serialized_output.data(),
                                    gpio_state_message.ByteSizeLong(),
                                    mqttmessage::QualityOfServiceMode::kExactlyOnce,
                                    true);

    std::unique_lock guard{m_gpio_pin_state_mutex};
    m_gpio_pin_state_condition.wait(guard);
  }
  catch(const common::CommonException &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
  }
}

//-----------------------------------------------------------------------------

void GpioControllerEngine::protobufPublishCompleted(bool completed, const std::vector<std::string> &topics)
{
  logging_handler::debug(FUNC_PREAMBLE, "Protobuf publish completed: {} [{}]",
                         completed, topics);

  if (!completed)
  {
    logging_handler::warning(
      FUNC_PREAMBLE,
      "Could not complete the delivery of the following topics: {}", topics);

    return;
  }

  if (topics.empty())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Topics is empty!");
    return;
  }

  bool gpio_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::set<std::string> &mqtt_topic_ids =
        m_external_modules->config->getMqttTopicIds();
      return std::any_of(mqtt_topic_ids.begin(), mqtt_topic_ids.end(),
                         [&](const std::string &configured_mqtt_topic_id) {
                           return published_topic == configured_mqtt_topic_id;
                         });
    });

  if (gpio_topic_published)
  {
    m_gpio_pin_state_condition.notify_all();
  }

  bool heartbeat_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::string &heartbeat_mqtt_topic =
        m_external_modules->config->getHeartbeatTopic();
      return published_topic == heartbeat_mqtt_topic;
    });

  if (heartbeat_topic_published)
  {
    m_heartbeat_timer.heartbeatSuccessfullyPublished();
  }
}

} // namespace rpi_utilities::gpio
