#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: config
*
************************************************/

// local includes

// common includes
#include "logginghandlerinterface.h"
#include <toml++/toml.hpp>
#include "rpimacros.h"

// system includes
#include <chrono>
#include <filesystem>
#include <set>
#include <string>

//------------------------------------------------------------------------------

namespace config
{

class ConfigBase
{
public:
  explicit ConfigBase(const std::filesystem::path &config_file_path);
  ConfigBase(const ConfigBase &) = default;
  ConfigBase(ConfigBase &&) = default;
  ConfigBase &operator=(const ConfigBase &) = default;
  ConfigBase &operator=(ConfigBase &&) = default;

  explicit ConfigBase(std::string &&mqtt_broker_address,
                      std::string &&mqtt_client_id,
                      std::string &&mqtt_user,
                      std::string &&mqtt_password,
                      std::set<std::string> &&mqtt_topic_ids,
                      std::string &&heartbeat_topic,
                      std::chrono::milliseconds heartbeat_cycle,
                      std::filesystem::path &&log_directory,
                      logging_handler::LoggingHandlerInterface::LoggingLevel log_level)
      : m_mqtt_broker_address{std::move(mqtt_broker_address)},
        m_mqtt_client_id{std::move(mqtt_client_id)},
        m_mqtt_user{std::move(mqtt_user)},
        m_mqtt_password{std::move(mqtt_password)},
        m_mqtt_topic_ids{std::move(mqtt_topic_ids)},
        m_heartbeat_topic{std::move(heartbeat_topic)},
        m_heartbeat_cycle{heartbeat_cycle},
        m_log_directory{std::move(log_directory)},
        m_log_level{log_level}
  {}

  GET_CONST_REF(MqttBrokerAddress, std::string, m_mqtt_broker_address)
  GET_CONST_REF(MqttClientId, std::string, m_mqtt_client_id)
  GET_CONST_REF(MqttUser, std::string, m_mqtt_user)
  GET_CONST_REF(MqttPassword, std::string, m_mqtt_password)
  GET_CONST_REF(MqttTopicIds, std::set<std::string>, m_mqtt_topic_ids)
  GET_CONST_REF(HeartbeatTopic, std::string, m_heartbeat_topic)
  GET_CONST_REF(HeartbeatCycle, std::chrono::milliseconds, m_heartbeat_cycle)
  GET_CONST_REF(LogDirectory, std::filesystem::path, m_log_directory)
  GET_CONST_REF(LogLevel, logging_handler::LoggingHandlerInterface::LoggingLevel, m_log_level)

  virtual ~ConfigBase() = default;

private:
  std::string m_mqtt_broker_address;
  std::string m_mqtt_client_id;
  std::string m_mqtt_user;
  std::string m_mqtt_password;
  std::set<std::string> m_mqtt_topic_ids;

  std::string m_heartbeat_topic;
  std::chrono::milliseconds m_heartbeat_cycle;

  std::filesystem::path m_log_directory;
  logging_handler::LoggingHandlerInterface::LoggingLevel m_log_level;

protected:
  toml::table m_toml;
};

} // namespace config
