/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: config
*
************************************************/

// local includes
#include "configbase.h"
#include "commonexception.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace
{
  const std::map<std::string, logging_handler::LoggingHandlerInterface::LoggingLevel> kLoggingLevelMapping {
    { "debug", logging_handler::LoggingHandlerInterface::LoggingLevel::kDebug },
    { "info", logging_handler::LoggingHandlerInterface::LoggingLevel::kInfo },
    { "error", logging_handler::LoggingHandlerInterface::LoggingLevel::kError },
    { "warning", logging_handler::LoggingHandlerInterface::LoggingLevel::kWarning },
    { "critical", logging_handler::LoggingHandlerInterface::LoggingLevel::kCritical },
  };

  // MQTT config
  constexpr const char *kMqttSectionName = "mqtt";
  constexpr const char *kMqttBrokerAddressName = "broker_address";
  constexpr const char *kMqttClientIdName = "client_id";
  constexpr const char *kMqttUserName = "user";
  constexpr const char *kMqttPasswordName = "password";
  constexpr const char *kMqttTopicIdsName = "topic_ids";

  // Log config
  constexpr const char *kLogSectionName = "log";
  constexpr const char *kLogDirectoryName = "directory";
  constexpr const char *kLogLevelName = "level";

  // Heartbeat config
  constexpr const char *kHeartbeatSectionName = "heartbeat";
  constexpr const char *kHeartbeatTopicName = "topic";
  constexpr const char *kHeartbeatCycleName = "cycle";
}

namespace config
{

ConfigBase::ConfigBase(const std::filesystem::path &config_file_path)
  : m_heartbeat_cycle{}
{
  m_toml = toml::parse_file(config_file_path.string());

  std::optional<std::string> mqtt_broker_address =
      m_toml[kMqttSectionName][kMqttBrokerAddressName].value<std::string>();

  std::optional<std::string> mqtt_client_id =
      m_toml[kMqttSectionName][kMqttClientIdName].value<std::string>();

  std::optional<std::string> mqtt_user =
      m_toml[kMqttSectionName][kMqttUserName].value<std::string>();

  std::optional<std::string> mqtt_password =
      m_toml[kMqttSectionName][kMqttPasswordName].value<std::string>();

  const auto &mqtt_topic_id_node = m_toml[kMqttSectionName][kMqttTopicIdsName];
  if (mqtt_topic_id_node.is_string())
  {
    m_mqtt_topic_ids.insert(mqtt_topic_id_node.value<std::string>().value());
  }
  else if (mqtt_topic_id_node.is_array())
  {
    mqtt_topic_id_node.as_array()->for_each([this](auto &&elem) {
      m_mqtt_topic_ids.insert(elem.as_string()->get());
    });
  }
  else
  {
    throw common::CommonException("No string nor array");
  }

  std::optional<std::string> heartbeat_topic =
      m_toml[kHeartbeatSectionName][kHeartbeatTopicName].value<std::string>();

  std::optional<int> heartbeat_cycle =
      m_toml[kHeartbeatSectionName][kHeartbeatCycleName].value<int>();

  std::optional<std::filesystem::path> log_directory =
      m_toml[kLogSectionName][kLogDirectoryName].value<std::string>();

  std::optional<std::string> log_level =
      m_toml[kLogSectionName][kLogLevelName].value<std::string>();

  if (!mqtt_broker_address.has_value() || !mqtt_client_id.has_value() ||
      !mqtt_user.has_value() || !mqtt_password.has_value() ||
      m_mqtt_topic_ids.empty() || !log_directory.has_value() ||
      !log_level.has_value())
  {
    throw common::CommonException{"One of the required config entries could not be read. "
        "Check 'mqtt_broker_address', 'mqtt_client_id', 'mqtt_user', "
        "'mqtt_password', 'mqtt_topic_id', 'log_directory', 'log_level'"};
  }

  m_mqtt_broker_address = *mqtt_broker_address;
  m_mqtt_client_id = *mqtt_client_id;
  m_mqtt_user = *mqtt_user;
  m_mqtt_password = *mqtt_password;
  m_heartbeat_topic = *heartbeat_topic;
  m_heartbeat_cycle = std::chrono::milliseconds{*heartbeat_cycle};
  m_log_directory = *log_directory;

  auto logging_level_mapping_it = kLoggingLevelMapping.find(*log_level);
  if (logging_level_mapping_it == kLoggingLevelMapping.end())
  {
    throw common::CommonException("Wrong log level! Supported values: debug, "
                                  "info, warning, error, critical");
  }

  m_log_level = logging_level_mapping_it->second;
}

} // namespace config
