/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: httprequest
*
************************************************/

// local includes
#include "httprequestadapter.h"

// common includes
#include "commonexception.h"

// system includes
#include <format>
#include <span>

//------------------------------------------------------------------------------

namespace
{

//! Callback which will be called from CURL.
//! \param bufptr Pointer to a buffer containing data.
//! \param size The size of one entry within bufptr.
//! \param nitems The amount of items stored within bufptr.
//! \param userp Pointer to internal data structure which will be filled with the retrieved data.
size_t curlWriteCallback(char *bufptr, size_t size, size_t nitems, void *userp)
{
  size_t bytes_to_copy = size * nitems;
  auto *chunk = static_cast<rpi_utilities::http_request::HttpRequestAdapter::CurlMemoryChunk*>(userp);

  std::span<char> span{bufptr, bytes_to_copy};
  chunk->data.insert(chunk->data.end(), span.begin(), span.end());

  return bytes_to_copy;
}

}

//------------------------------------------------------------------------------

namespace rpi_utilities::http_request
{

HttpRequestAdapter::HttpRequestAdapter()
  : m_curl_handle{curl_easy_init()}
{
  if (m_curl_handle == nullptr)
  {
    return;
  }

  curl_easy_setopt(
    m_curl_handle,
    CURLOPT_WRITEFUNCTION,
    curlWriteCallback
  );

  curl_easy_setopt(
    m_curl_handle,
    CURLOPT_WRITEDATA,
    (void *)&m_curl_memory_chunk
  );

  curl_easy_setopt(
    m_curl_handle,
    CURLOPT_ERRORBUFFER,
    m_curl_errbuf.data()
  );
}

//------------------------------------------------------------------------------

HttpRequestAdapter::~HttpRequestAdapter()
{
  if (m_curl_handle == nullptr)
  {
    return;
  }

  curl_easy_cleanup(m_curl_handle);
}

//------------------------------------------------------------------------------

HttpRequestAdapter& HttpRequestAdapter::guard()
{
  m_mutex.lock();
  m_is_thread_safe = true;

  return *this;
}

//------------------------------------------------------------------------------
HttpRequestAdapter& HttpRequestAdapter::defineUserAgent(const std::string &user_agent_string)
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  curl_easy_setopt(m_curl_handle, CURLOPT_USERAGENT, user_agent_string.c_str());
  return *this;
}

//------------------------------------------------------------------------------

HttpRequestAdapter& HttpRequestAdapter::defineProxy(const std::string &proxy_address)
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  curl_easy_setopt(m_curl_handle, CURLOPT_PROXY, proxy_address.c_str());
  return *this;
}

//------------------------------------------------------------------------------

HttpRequestAdapter& HttpRequestAdapter::defineRequestUrl(const std::string &request_url)
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  curl_easy_setopt(m_curl_handle, CURLOPT_URL, request_url.c_str());
  return *this;
}

//------------------------------------------------------------------------------

HttpRequestAdapter& HttpRequestAdapter::defineRequestCaCertPath(const std::filesystem::path &ca_cert_path)
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  curl_easy_setopt(m_curl_handle, CURLOPT_CAINFO, ca_cert_path.c_str());
  return *this;
}

//------------------------------------------------------------------------------
HttpRequestAdapter& HttpRequestAdapter::defineRequestHeaders(const std::vector<std::string> &headers)
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  curl_slist *curl_headers = nullptr;
  for (const auto &header : headers)
  {
    curl_headers = curl_slist_append(curl_headers, header.c_str());
  }

  curl_easy_setopt(m_curl_handle, CURLOPT_HTTPHEADER, curl_headers);

  return *this;
}

//------------------------------------------------------------------------------

HttpRequestAdapter& HttpRequestAdapter::definePostRequestParameters(const std::string &post_payload)
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  curl_easy_setopt(m_curl_handle, CURLOPT_POSTFIELDS, post_payload.c_str());

  return *this;
}

//------------------------------------------------------------------------------

std::vector<char> HttpRequestAdapter::send()
{
  if (!m_is_thread_safe)
  {
    throw common::CommonException("Call is not thread safe! Need to call guard() first.");
  }

  if (m_curl_handle == nullptr)
  {
    throw common::CommonException("CURL handle has not been initialized correctly!");
  }

  m_curl_memory_chunk.data.clear();
  const CURLcode res = curl_easy_perform(m_curl_handle);
  if (res != CURLE_OK)
  {
    throw common::CommonException(
        std::format("CURL request was not successful: '{}'", m_curl_errbuf.data()));
  }

  m_curl_memory_chunk.data.emplace_back(0x00);

  m_mutex.unlock();

  return m_curl_memory_chunk.data;
}

} // namespace rpi_utilities::http_request
