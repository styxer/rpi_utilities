#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: httprequest
*
************************************************/

// local includes

// common includes
#include "curl/curl.h"

// system includes
#include <filesystem>
#include <mutex>
#include <string>
#include <vector>

//------------------------------------------------------------------------------

namespace rpi_utilities::http_request
{

class HttpRequestAdapter
{
  public:
    struct CurlMemoryChunk
    {
      std::vector<char> data;
    };

    HttpRequestAdapter();
    HttpRequestAdapter(const HttpRequestAdapter &) = delete;
    HttpRequestAdapter(HttpRequestAdapter &&) = delete;
    HttpRequestAdapter &operator=(const HttpRequestAdapter &) = delete;
    HttpRequestAdapter &operator=(HttpRequestAdapter &&) = delete;

    virtual ~HttpRequestAdapter();

    HttpRequestAdapter& guard();

    //! Helper method which defines the user agent.
    //! \param user_agent_string The user agent which will be used.
    HttpRequestAdapter& defineUserAgent(const std::string &user_agent_string);

    //! Helper method which defines the proxy address.
    //! \param proxy_address The proxy address which shall be used.
    HttpRequestAdapter& defineProxy(const std::string &proxy_address);

    //! Helper method which defines the URL of the HTTP request.
    //! \param request_url The URL which will be used for the request.
    HttpRequestAdapter& defineRequestUrl(const std::string &request_url);

    //! Helper method which defines the path to the CA cert file.
    //! \param ca_cert_path The path to the CA Cert file.
    HttpRequestAdapter& defineRequestCaCertPath(const std::filesystem::path &ca_cert_path);

    //! Helper method which defines the header fields of the HTTP request.
    //! \param headers The HTTP headers which shall be used.
    HttpRequestAdapter& defineRequestHeaders(const std::vector<std::string> &headers);

    //! Helper method which defines the HTTP POST data for the HTTP request.
    //! \param post_payload Payload which shall be sent to the API.
    HttpRequestAdapter& definePostRequestParameters(const std::string &post_payload);

    //! Sends the actual HTTP request to the configured server
    std::vector<char> send();

  private:
    bool m_is_thread_safe = false;
    std::mutex m_mutex;

    CURL *m_curl_handle = nullptr;
    CurlMemoryChunk m_curl_memory_chunk;
    std::array<char, CURL_ERROR_SIZE> m_curl_errbuf{};
};

}
