#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes

// common includes

// system includes
#include <exception>
#include <string>

//------------------------------------------------------------------------------

namespace common {

class CommonException : public std::exception
{
public:
  CommonException(const CommonException &) = delete;
  CommonException(CommonException &&) = delete;
  CommonException &operator=(const CommonException &) = delete;
  CommonException &operator=(CommonException &&) = delete;

  explicit CommonException(std::string msg);
   ~CommonException() override = default;

  [[nodiscard]] const char* what() const noexcept override;

private:
  const std::string m_msg;
};

} // namespace common
