/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "processstarter.h"
#include "commonexception.h"
#include "rpimacros.h"

// common includes

// system includes
#include <array>
#include <cstdio>
#include <format>
#include <iterator>
#include <sstream>

// forward declarations

//------------------------------------------------------------------------------

namespace common {

std::string ProcessStarter::startProcess(const std::string &process_name,
                                         const std::vector<std::string> &argument_list) const
{
  // NOTE: This is needed since std::ranges::view::join_with is not implemented in libc++ :(
  constexpr const char *kDelimiter = " ";
  std::stringstream joined_argument_list_stream;
  std::copy(argument_list.begin(), argument_list.end(),
            std::ostream_iterator<std::string>(joined_argument_list_stream,
                                               kDelimiter));

  const std::string process_command = std::vformat(
    "{} {}2>&1",
    std::make_format_args(process_name, joined_argument_list_stream.str()));

  FILE *command_pipe = popen(process_command.c_str(), "r");

  if (command_pipe == nullptr)
  {
    throw common::CommonException(std::format("Can't execute command: {}", process_command));
  }

  std::string process_output;

  constexpr int64_t kBufferSize = 12;
  std::array<char, kBufferSize> buffer{};

  while(feof(command_pipe) == 0)
  {
    if (fgets(buffer.data(), kBufferSize, command_pipe) != nullptr)
    {
      process_output += buffer.data();
    }
  }

  pclose(command_pipe);
  return process_output;
}

} // namespace common
