#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "commonexception.h"

// common includes
#include "mqttmessageadapter.h"

// system includes
#include <dlfcn.h>
#include <format>
#include <memory>
#include <string>

#if __cplusplus <= 202002L
#include <fmt/format.h>
#endif

//------------------------------------------------------------------------------

static constexpr const char *kPluginAllocatorSymbol   = "PluginAllocator";
static constexpr const char *kPluginDeallocatorSymbol = "PluginDeallocator";

//------------------------------------------------------------------------------

template <class T>
std::shared_ptr<T> loadPlugin(const std::string &plugin_name)
{
  if (plugin_name.empty())
  {
    throw common::CommonException("Plugin name MUST NOT be empty");
  }

  void *handle = dlopen(plugin_name.c_str(), RTLD_NOW | RTLD_LAZY);
  if (handle == nullptr)
  {

#if __cplusplus > 202002L
    throw common::CommonException(
      std::format("Could not open plugin from path '{}'.\nGot error: {}",
                  plugin_name, dlerror()));
#else
    throw common::CommonException(
      fmt::format("Could not open plugin from path '{}'.\nGot error: {}",
                  plugin_name, dlerror()));
#endif
  }

  using allocClass  = T *(*)();
	using deleteClass = void (*)(T *);

	auto alloc_func  = reinterpret_cast<allocClass>(dlsym(handle, kPluginAllocatorSymbol));    // NOLINT
	auto delete_func = reinterpret_cast<deleteClass>(dlsym(handle, kPluginDeallocatorSymbol)); // NOLINT

	if (!alloc_func || !delete_func)
  {
    if (dlclose(handle) != 0)
    {
      throw common::CommonException("Could not close plugin handle!");
    }

#if __cplusplus > 202002L
    throw common::CommonException(std::format("Could not load plugin '{}'", plugin_name));
#else
    throw common::CommonException(fmt::format("Could not load plugin '{}'", plugin_name));
#endif
	}

	return std::shared_ptr<T>(alloc_func(), [delete_func](T *pointer){ delete_func(pointer); });
}

//------------------------------------------------------------------------------

template <class T>
void serializeProtobufMessage(const T &protobuf_message, char *serialized_message, size_t size)
{
  if (!protobuf_message.SerializeToArray(serialized_message, size))
  {
#if __cplusplus > 202002L
    throw common::CommonException(
      std::format("Message of type '{}' could not be serialized.",
                  std::string{typeid(T).name()}));
#else
    throw common::CommonException(
      fmt::format("Message of type '{}' could not be serialized.",
                  std::string{typeid(T).name()}));
#endif
  }
}

//------------------------------------------------------------------------------

template <class T>
T parseProtobufMessage(const char *raw_message, const size_t size)
{
  T parsed_protobuf_message;
  if (!parsed_protobuf_message.ParseFromArray(raw_message, size))
  {

#if __cplusplus > 202002L
    throw common::CommonException(
      std::format("Message of type '{}' could not be parsed.",
                  std::string{typeid(T).name()}));
#else
    throw common::CommonException(
      fmt::format("Message of type '{}' could not be parsed.",
                  std::string{typeid(T).name()}));
#endif

  }

  return parsed_protobuf_message;
}
