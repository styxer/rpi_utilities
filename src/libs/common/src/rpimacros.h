#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes

// common includes

// system includes

//------------------------------------------------------------------------------

#define GET_VAL(NAME, TYPE, MEMBER_NAME) \
TYPE get##NAME() const                   \
{                                        \
  return MEMBER_NAME;                    \
}                                        \
TYPE get##NAME()                         \
{                                        \
  return MEMBER_NAME;                    \
}

//------------------------------------------------------------------------------

#define GET_REF(NAME, TYPE, MEMBER_NAME) \
const TYPE& get##NAME() const            \
{                                        \
  return MEMBER_NAME;                    \
}                                        \
TYPE& get##NAME()                        \
{                                        \
  return MEMBER_NAME;                    \
}

//------------------------------------------------------------------------------

#define GET_CONST_REF(NAME, TYPE, MEMBER_NAME) \
const TYPE& get##NAME() const                  \
{                                              \
  return MEMBER_NAME;                          \
}                                              \

//------------------------------------------------------------------------------

#define SET_VAL(NAME, TYPE, MEMBER_NAME) \
void set##NAME(const TYPE val)           \
{                                        \
  MEMBER_NAME = val;                     \
}

//------------------------------------------------------------------------------

#define SET_REF(NAME, TYPE, MEMBER_NAME) \
void set##NAME(const TYPE &val)          \
{                                        \
  MEMBER_NAME = val;                     \
}

//------------------------------------------------------------------------------

#define DEFINE_PLUGIN(TYPE, ALLOCATOR_NAME, DEALLOCATOR_NAME) \
extern "C"                                                    \
{                                                             \
  TYPE* ALLOCATOR_NAME()                                      \
  {                                                           \
    return new TYPE();                                        \
  }                                                           \
                                                              \
  void DEALLOCATOR_NAME(TYPE *ptr)                            \
  {                                                           \
    delete ptr;                                               \
  }                                                           \
}
