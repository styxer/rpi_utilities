#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "processstarterinterface.h"

// common includes

// system includes

// forward declarations

//------------------------------------------------------------------------------

namespace common {

class ProcessStarter : public ProcessStarterInterface
{
public:
  ProcessStarter(const ProcessStarter &) = delete;
  ProcessStarter(ProcessStarter &&) = delete;
  ProcessStarter &operator=(const ProcessStarter &) = delete;
  ProcessStarter &operator=(ProcessStarter &&) = delete;

  explicit ProcessStarter() = default;
  ~ProcessStarter() override = default;

  //! \see interface
  [[nodiscard]] std::string startProcess(const std::string &process_name, const std::vector<std::string> &argument_list) const override;
};

} // namespace common
