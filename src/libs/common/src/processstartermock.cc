/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "processstartermock.h"

// common includes

// system includes

// forward declarations

//------------------------------------------------------------------------------

namespace common {

ProcessStarterMock::ProcessStarterMock(std::string return_value)
  : m_return_value{std::move(return_value)}
{}

//------------------------------------------------------------------------------

std::string ProcessStarterMock::startProcess(const std::string &, const std::vector<std::string> &) const
{
  return m_return_value;
}

} // namespace common
