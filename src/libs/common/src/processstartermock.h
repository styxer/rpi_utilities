#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "processstarterinterface.h"

// common includes

// system includes
#include <string>
#include <vector>

// forward declarations

//------------------------------------------------------------------------------

namespace common {

class ProcessStarterMock : public ProcessStarterInterface
{
public:
  ProcessStarterMock(const ProcessStarterMock &) = delete;
  ProcessStarterMock(ProcessStarterMock &&) = delete;
  ProcessStarterMock &operator=(const ProcessStarterMock &) = delete;
  ProcessStarterMock &operator=(ProcessStarterMock &&) = delete;

  explicit ProcessStarterMock(std::string return_value);
  ~ProcessStarterMock() override = default;

  //! \see interface
  std::string startProcess(const std::string &process_name, const std::vector<std::string> &argument_list) const override;

  const std::string m_return_value;
};

} // namespace common
