#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes

// common includes

// system includes
#include <string>
#include <vector>

// forward declarations

//------------------------------------------------------------------------------

namespace common {

class ProcessStarterInterface
{
public:
  ProcessStarterInterface(const ProcessStarterInterface &) = delete;
  ProcessStarterInterface(ProcessStarterInterface &&) = delete;
  ProcessStarterInterface &operator=(const ProcessStarterInterface &) = delete;
  ProcessStarterInterface &operator=(ProcessStarterInterface &&) = delete;

  ProcessStarterInterface() = default;
  virtual ~ProcessStarterInterface() = default;

  //! Helper method which starts the given process with the given arguments and returns the output.
  //! \param process_name  The name of the process, which should be started.
  //! \param argument_list The list of arguments, which should be passed to the process.
  //! \return The output of the executed process with the given command.
  [[nodiscard]] virtual std::string startProcess(const std::string &process_name,
                                                 const std::vector<std::string> &argument_list) const = 0;
};

} // namespace common
