#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2024
*
* Module: common
*
************************************************/

// local includes

// common includes

// system includes
#include <chrono>
#include <memory>
#include <mutex>
#include <vector>

//------------------------------------------------------------------------------

namespace mqttmessage
{

class MqttMessageAdapterInterface;

}

//------------------------------------------------------------------------------

namespace common
{

class AsyncTimer;

class HeartBeat
{
  public:
    HeartBeat(std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> mqtt_adapter_interface,
              std::string heartbeat_topic,
              std::string node_id,
              std::chrono::milliseconds interval);
    HeartBeat(const HeartBeat &) = delete;
    HeartBeat(HeartBeat &&) = delete;
    HeartBeat &operator=(const HeartBeat &) = delete;
    HeartBeat &operator=(HeartBeat &&) = delete;
    ~HeartBeat();

    void start();
    void stop();

    //! Method which MUST be called after the heartbeat topic has been published successfully.
    void heartbeatSuccessfullyPublished();

  private:
    std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> m_mqtt_adapter_interface;
    std::string m_heartbeat_topic;
    std::string m_node_id;
    std::chrono::milliseconds m_interval;

    std::vector<char> m_serialized_heartbeat_message;

    std::unique_ptr<common::AsyncTimer> m_heartbeat_timer;

    std::mutex m_heartbeat_mutex;
    std::condition_variable m_heartbeat_condition;

    //! Callback which sends the HeartbeatMessage.
    //! \return False since it should be called periodically.
    bool sendHeartbeatMessage();
};

}
