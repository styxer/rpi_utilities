/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2024
*
* Module: common
*
************************************************/

// local includes
#include "async_timer.h"

// common includes
#include "heartbeat.h"
#include "heartbeat.pb.h"
#include "mqttmessageadapterinterface.h"
#include "rpiutilities.h"

#include <utility>

// system includes

//------------------------------------------------------------------------------

namespace common
{

HeartBeat::HeartBeat(std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> mqtt_adapter_interface,
                     std::string heartbeat_topic,
                     std::string node_id,
                     std::chrono::milliseconds interval)
  : m_mqtt_adapter_interface{std::move(mqtt_adapter_interface)},
    m_heartbeat_topic{std::move(heartbeat_topic)},
    m_node_id{std::move(node_id)},
    m_interval{interval},
    m_heartbeat_timer{std::make_unique<AsyncTimer>([&]() -> bool { return sendHeartbeatMessage(); })}
{}

//------------------------------------------------------------------------------

HeartBeat::~HeartBeat() = default;

//------------------------------------------------------------------------------

void HeartBeat::start()
{
  m_heartbeat_timer->start(m_interval);
}

//------------------------------------------------------------------------------

void HeartBeat::stop()
{
  m_heartbeat_condition.notify_all();
  m_heartbeat_timer->stop();
}

//------------------------------------------------------------------------------

void HeartBeat::heartbeatSuccessfullyPublished()
{
  m_heartbeat_condition.notify_all();
}

//------------------------------------------------------------------------------

bool HeartBeat::sendHeartbeatMessage()
{
  rpi_utilities::message::Heartbeat heartbeat;
  heartbeat.set_heartbeat_timestamp(
    static_cast<int64_t>(std::chrono::duration_cast<std::chrono::seconds>(
                           std::chrono::system_clock::now().time_since_epoch())
                           .count()));
  heartbeat.set_node_id(m_node_id);

  m_serialized_heartbeat_message = std::vector<char>(heartbeat.ByteSizeLong());

  serializeProtobufMessage<rpi_utilities::message::Heartbeat>(heartbeat,
                                                              m_serialized_heartbeat_message.data(),
                                                              m_serialized_heartbeat_message.size());

  m_mqtt_adapter_interface->publish(m_heartbeat_topic,
                                    m_serialized_heartbeat_message.data(),
                                    m_serialized_heartbeat_message.size(),
                                    mqttmessage::QualityOfServiceMode::kExactlyOnce,
                                    false);

  std::unique_lock lock{m_heartbeat_mutex};
  m_heartbeat_condition.wait(lock);

  return false;
}

}
