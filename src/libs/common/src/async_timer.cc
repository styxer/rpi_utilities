/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "async_timer.h"

// common includes

// system includes
#include <future>
#include <chrono>
#include <utility>

//------------------------------------------------------------------------------

namespace common {

#if __cplusplus > 202002L

bool AsyncTimer::AsyncTimerAwaitable::await_ready()
{
  return false;
}

//------------------------------------------------------------------------------

void AsyncTimer::AsyncTimerAwaitable::await_suspend(std::coroutine_handle<> coroutine_handle)
{
  *m_future = std::async(std::launch::async, [this, coroutine_handle](std::chrono::milliseconds interval) {
    while (true)
    {
      std::unique_lock lk{*m_mutex};
      m_condition_variable->wait_for(lk, interval);

      if (!*m_is_running)
      {
        break;
      }

      bool cancel = (*m_timer_triggered_callback)();
      if (cancel)
      {
        break;
      }
    }

    coroutine_handle.resume();
  },
  interval);
}

//------------------------------------------------------------------------------

void AsyncTimer::AsyncTimerAwaitable::await_resume()
{
}

#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

AsyncTimer::AsyncTimer(std::function<bool()> timer_triggered_callback)
  : m_timer_triggered_callback{std::move(timer_triggered_callback)}
{
}

//------------------------------------------------------------------------------

void AsyncTimer::start(const std::chrono::milliseconds &msecs_to_wait)
{
  if (m_is_running)
  {
    return;
  }

  m_is_running = true;

#if __cplusplus > 202002L
  startCoroutine(msecs_to_wait);
#else
  m_future = std::async(std::launch::async, [this, coroutine_handle](std::chrono::milliseconds interval) {
    while (true)
    {
      std::unique_lock lk{m_mutex};
      m_condition_variable->wait_for(lk, interval);

      if (!m_is_running)
      {
        break;
      }

      bool cancel = (m_timer_triggered_callback)();
      if (cancel)
      {
        break;
      }
    }
  },
  interval);
#endif
}

//------------------------------------------------------------------------------

std::future<void>& AsyncTimer::stop()
{
  if (!m_is_running)
  {
    return m_future;
  }

  m_is_running = false;

  m_condition_variable.notify_all();
  return m_future;
}

//------------------------------------------------------------------------------

#if __cplusplus > 202002L

AsyncTimer::AsyncTimerTask AsyncTimer::startCoroutine(std::chrono::milliseconds msecs_to_wait)
{
  co_await AsyncTimer::AsyncTimerAwaitable {
    msecs_to_wait,
    &m_timer_triggered_callback,
    &m_is_running,
    &m_mutex,
    &m_condition_variable,
    &m_future
  };
}

#endif

} // namespace common
