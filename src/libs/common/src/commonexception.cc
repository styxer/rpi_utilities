/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes
#include "commonexception.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace common {

CommonException::CommonException(std::string msg) :
  m_msg{std::move(msg)}
{
}

//------------------------------------------------------------------------------

const char* CommonException::what() const noexcept
{
  return m_msg.c_str();
}

} // namespace common
