#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: common
*
************************************************/

// local includes

// common includes

// system includes
#include <atomic>

#if __cplusplus > 202002L
#include <coroutine>
#endif

#include <functional>
#include <future>

//------------------------------------------------------------------------------

namespace common {

class AsyncTimer
{
public:
  AsyncTimer(const AsyncTimer &) = delete;
  AsyncTimer(AsyncTimer &&) = delete;
  AsyncTimer &operator=(const AsyncTimer &) = delete;
  AsyncTimer &operator=(AsyncTimer &&) = delete;

  explicit AsyncTimer(std::function<bool()> timer_triggered_callback);
  virtual ~AsyncTimer() = default;

  void start(const std::chrono::milliseconds &msecs_to_wait);
  std::future<void>& stop();

private:

#if __cplusplus > 202002L
  struct AsyncTimerTask
  {
    struct PromiseType
    {
      AsyncTimerTask get_return_object() { return {}; }
      std::suspend_never initial_suspend() { return {}; }
      std::suspend_always final_suspend() noexcept { return {}; }
      void return_void() {}
      void unhandled_exception() {}
    };

    using promise_type = PromiseType;
  };

  struct AsyncTimerAwaitable
  {
    std::chrono::milliseconds interval;

    std::function<bool()>* m_timer_triggered_callback;
    std::atomic<bool>* m_is_running;
    std::mutex* m_mutex;
    std::condition_variable* m_condition_variable;
    std::future<void>* m_future;

    static bool await_ready();
    void await_suspend(std::coroutine_handle<> coroutine_handle);
    void await_resume();
  };
#endif

  std::function<bool()> m_timer_triggered_callback;
  std::atomic<bool> m_is_running = false;
  std::mutex m_mutex;
  std::condition_variable m_condition_variable;

  std::future<void> m_future;

#if __cplusplus > 202002L
  AsyncTimerTask startCoroutine(std::chrono::milliseconds msecs_to_wait);
#endif
};

} // namespace common
