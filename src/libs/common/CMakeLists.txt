set(HEADERS
    src/async_timer.h
    src/commonexception.h
    src/heartbeat.h
    src/processstarter.h
    src/processstarterinterface.h
    src/processstartermock.h
    src/rpimacros.h
    src/rpiutilities.h
)

set(SOURCES
    src/async_timer.cc
    src/commonexception.cc
    src/heartbeat.cc
    src/processstarter.cc
    src/processstartermock.cc
)

add_library(commonlib SHARED ${SOURCES} ${HEADERS})
target_include_directories(commonlib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src)

target_link_libraries(commonlib
                      messagelib
                      mqttmessaginglib
                      dl)

install(
  TARGETS commonlib
  COMPONENT commonlib
  LIBRARY DESTINATION ${INSTALL_PATH}
  DESTINATION ${INSTALL_PATH}
)

if (${WITH_TESTS})
  add_library(pluginmock SHARED
              unittests/pluginmockinterface.h
              unittests/pluginmock.h
              unittests/pluginmock.cc)

  target_link_libraries(pluginmock commonlib)

  set (UNITTEST_NAME_1 tstrpiutilities)

  set (SOURCES_1
       unittests/pluginmockinterface.h
       unittests/tstrpiutilities.cc
  )

  add_executable(${UNITTEST_NAME_1} ${SOURCES_1})

  target_link_libraries(${UNITTEST_NAME_1}
                        commonlib
                        GTest::gtest_main)

  add_custom_command(
    TARGET ${UNITTEST_NAME_1} POST_BUILD
    COMMAND make pluginmock
    VERBATIM
  )

  add_test(
    NAME ${UNITTEST_NAME_1}
    COMMAND ${UNITTEST_NAME_1}
  )

  set (UNITTEST_NAME_2 tstprocessstarter)

  set (SOURCES_2
       unittests/tstprocessstarter.cc
  )

  add_executable(${UNITTEST_NAME_2} ${SOURCES_2})

  target_link_libraries(${UNITTEST_NAME_2}
                        commonlib
                        GTest::gtest_main)

  add_test(
    NAME ${UNITTEST_NAME_2}
    COMMAND ${UNITTEST_NAME_2}
  )

  include(GoogleTest)
  gtest_discover_tests(${UNITTEST_NAME_1}
                       ${UNITTEST_NAME_2})
endif()

