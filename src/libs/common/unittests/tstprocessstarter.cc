/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: common
*
************************************************/

// local includes
#include "processstarter.h"

// common includes
#include <gtest/gtest.h>

// system includes

//------------------------------------------------------------------------------

TEST (ProcessStarter, Echo) // NOLINT
{
  try
  {
    const std::string process_expected_output = "ProcessStarter Unit Test";

    common::ProcessStarter process_starter;
    const std::string process_output
      = process_starter.startProcess("echo",
                                     std::vector<std::string>{process_expected_output});

    EXPECT_EQ(process_output, process_expected_output + "\n");
  }
  catch (std::exception &ex)
  {
    FAIL() << ex.what();
  }
}
