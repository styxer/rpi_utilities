/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: common
*
************************************************/

// local includes
#include "pluginmock.h"
#include "rpimacros.h"

// common includes

// system includes

DEFINE_PLUGIN(PluginMock, PluginAllocator, PluginDeallocator) // NOLINT

std::string PluginMock::message() const
{
  return "PluginMock::message";
}
