#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: common
*
************************************************/

// local includes

// common includes

// system includes
#include <string>

class PluginMockInterface
{
  public:
    PluginMockInterface(const PluginMockInterface &) = delete;
    PluginMockInterface(PluginMockInterface &&) = delete;
    PluginMockInterface &operator=(const PluginMockInterface &) = delete;
    PluginMockInterface &operator=(PluginMockInterface &&) = delete;

    PluginMockInterface() = default;
    virtual ~PluginMockInterface() = default;

    [[nodiscard]] virtual std::string message() const = 0;
};

