/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: common
*
************************************************/

// local includes
#include "rpiutilities.h"
#include "pluginmockinterface.h"

// common includes
#include <gtest/gtest.h>

// system includes
#include <iostream>

//------------------------------------------------------------------------------

TEST (RpiUtilities, PluginLoader) // NOLINT
{
  try
  {
    std::shared_ptr<PluginMockInterface> plugin
      = loadPlugin<PluginMockInterface>("libpluginmock.so");
  }
  catch (std::exception &ex)
  {
    FAIL() << ex.what();
  }
}

//------------------------------------------------------------------------------

TEST (RpiUtilities, PluginLoaderMessage) // NOLINT
{
  try
  {
    std::shared_ptr<PluginMockInterface> plugin
      = loadPlugin<PluginMockInterface>("libpluginmock.so");

    EXPECT_EQ(plugin->message(), "PluginMock::message");
  }
  catch (std::exception &ex)
  {
    FAIL() << ex.what();
  }
}

//------------------------------------------------------------------------------
