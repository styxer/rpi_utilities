#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: common
*
************************************************/

// local includes
#include "pluginmockinterface.h"

// common includes

// system includes

class PluginMock : public PluginMockInterface
{
  public:
    PluginMock(const PluginMock &) = delete;
    PluginMock(PluginMock &&) = delete;
    PluginMock &operator=(const PluginMock &) = delete;
    PluginMock &operator=(PluginMock &&) = delete;

    PluginMock() = default;
    ~PluginMock() override = default;

    [[nodiscard]] std::string message() const override;
};

