/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: logginghandler
*
************************************************/

// local includes
#include "filelogginghandler.h"

// common includes
#include "spdlog/sinks/rotating_file_sink.h"

// system includes
#include <chrono>
#include <format>

//------------------------------------------------------------------------------

namespace
{

constexpr int32_t kFileSize1Mb = 1048576 * 1;
constexpr size_t  kOneRotatingFile = 1;

}

//------------------------------------------------------------------------------

namespace logging_handler
{

FileLoggingHandler::FileLoggingHandler(std::string &&process_name,
                                       const std::filesystem::path &log_directory)
  : m_process_name{std::move(process_name)}
{
  m_file_logger = spdlog::create_async<spdlog::sinks::rotating_file_sink_mt>(
    "file_logger",
    std::format("{}{}.log", log_directory.string(), m_process_name),
    kFileSize1Mb,
    kOneRotatingFile
  );

  m_file_logger->set_pattern("[%Y.%m.%d %H:%M:%S][%l][%t]: %v");

  spdlog::flush_every(std::chrono::seconds(1));
}

//------------------------------------------------------------------------------

void FileLoggingHandler::log(const LoggingLevel &logging_level, const std::string &msg)
{
  switch(logging_level)
  {
    case LoggingHandlerInterface::kDebug:
      m_file_logger->debug(msg);
      break;
    case LoggingHandlerInterface::kInfo:
      m_file_logger->info(msg);
      break;
    case LoggingHandlerInterface::kWarning:
      m_file_logger->warn(msg);
      break;
    case LoggingHandlerInterface::kError:
      m_file_logger->error(msg);
      break;
    case LoggingHandlerInterface::kCritical:
      m_file_logger->critical(msg);
      break;
    default:
      break;
  }
}

//------------------------------------------------------------------------------

void FileLoggingHandler::setLoggingLevel(const LoggingLevel &logging_level)
{
  auto logging_level_it = m_logging_level_mapping.find(logging_level);
  if (logging_level_it == m_logging_level_mapping.end())
  {
    return;
  }

  m_file_logger->set_level(logging_level_it->second);
}

} // namespace logging_handler
