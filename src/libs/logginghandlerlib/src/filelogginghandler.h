#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: logginghandler
*
************************************************/

// local includes
#include "logginghandlerinterface.h"

// common includes
#include "spdlog/async.h"

// system includes
#include <filesystem>
#include <string>

//------------------------------------------------------------------------------

namespace logging_handler
{

class FileLoggingHandler : public LoggingHandlerInterface
{
public:
  FileLoggingHandler(const FileLoggingHandler &) = delete;
  FileLoggingHandler(FileLoggingHandler &&) = delete;
  FileLoggingHandler &operator=(const FileLoggingHandler &) = delete;
  FileLoggingHandler &operator=(FileLoggingHandler &&) = delete;

  explicit FileLoggingHandler(std::string &&process_name,
                              const std::filesystem::path &log_directory = "/var/log/");

  ~FileLoggingHandler() override = default;

  void log(const LoggingLevel &logging_level, const std::string &msg) override;
  void setLoggingLevel(const LoggingLevel &logging_level) override;

private:
  std::string m_process_name;

  std::shared_ptr<spdlog::logger> m_file_logger = nullptr;
};

} // namespace logging_handler
