/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: logginghandler
*
************************************************/

// local includes
#include "logginghelper.h"

// common includes

// system includes
#include <regex>

//------------------------------------------------------------------------------

std::string prettyFunction(const std::string &pretty_function)
{
  const std::regex function_name_regex{"(.* )+((.*)(\\(.*\\)){1})"};
  std::smatch regex_match;

  std::string function_name;
  while (std::regex_search(pretty_function, regex_match, function_name_regex))
  {
    for (const auto &regex_group : regex_match)
    {
      if (regex_group.str().find(' ') != std::string::npos ||
          regex_group.str().find_last_of(':') == regex_group.str().size() - 1 ||
          regex_group.str().find_last_of(')') == regex_group.str().size() - 1)
      {
        continue;
      }

      function_name += regex_group;
      break;
    }
    break;
  }

  return std::format("{}: ", function_name);
}

//------------------------------------------------------------------------------

namespace logging_handler
{

void LoggingHelper::setLoggingCallback(std::function<void(const LoggingHandlerInterface::LoggingLevel &,
                                                          const std::string&)> logging_callback)
{
  m_logging_callback = std::move(logging_callback);
}

//-----------------------------------------------------------------------------

void LoggingHelper::log(const LoggingHandlerInterface::LoggingLevel &logging_level,
                        const std::string &msg)
{
  if (m_logging_callback == nullptr)
  {
    return;
  }

  m_logging_callback(logging_level, msg);
}

//-----------------------------------------------------------------------------

/*static*/ std::shared_ptr<LoggingHelper> LoggingHelper::instance()
{
  static std::shared_ptr<LoggingHelper> instance;
  if (instance == nullptr)
  {
    instance = std::make_shared<LoggingHelper>();
  }

  return instance;
}

} // namespace logging_handler
