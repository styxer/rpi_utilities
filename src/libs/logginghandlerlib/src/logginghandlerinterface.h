#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: logginghandler
*
************************************************/

// local includes

// common includes
#include "spdlog/spdlog.h"

// system includes
#include <map>
#include <string>

//------------------------------------------------------------------------------

namespace logging_handler {

class LoggingHandlerInterface
{
public:
  enum LoggingLevel : unsigned {
    kDebug = 0,
    kInfo = 1,
    kWarning = 2,
    kError = 3,
    kCritical = 4
  };

  LoggingHandlerInterface() = default;
  LoggingHandlerInterface(const LoggingHandlerInterface &) = delete;
  LoggingHandlerInterface(LoggingHandlerInterface &&) = delete;
  LoggingHandlerInterface &operator=(const LoggingHandlerInterface &) = delete;
  LoggingHandlerInterface &operator=(LoggingHandlerInterface &&) = delete;
  virtual ~LoggingHandlerInterface() = default;

  //! Helper method which logs the given message with the given log level.
  //! \param logging_level Specifies the severity of the log message.
  //! \param msg The message which should be logged.
  virtual void log(const LoggingLevel &logging_level, const std::string &msg) = 0;

  //! Sets the logging level.
  //! \param logging_level The logging level which should be considered by the logger.
  virtual void setLoggingLevel(const LoggingLevel &logging_level) = 0;

protected:
  std::map<LoggingLevel, spdlog::level::level_enum> m_logging_level_mapping { // NOLINT
    {LoggingLevel::kDebug,    spdlog::level::debug},
    {LoggingLevel::kInfo,     spdlog::level::info},
    {LoggingLevel::kWarning,  spdlog::level::warn},
    {LoggingLevel::kError,    spdlog::level::err},
    {LoggingLevel::kCritical, spdlog::level::critical}
  };
};

} // namespace logging_handler
