#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: logginghandler
*
************************************************/

// local includes
#include "logginghandlerinterface.h"

// common includes

// system includes
#include <functional>
#include <memory>

#if __cplusplus > 202002L
#include <format>
#include <source_location>
#endif

//------------------------------------------------------------------------------

//! Helper function which prettyfies the method name of the calling method.
//! \param pretty_function The string which is returned from std::source_location::current().function_name()
//! \return The prettyfied function/method name.
std::string prettyFunction(const std::string &pretty_function);

//------------------------------------------------------------------------------

#if __cplusplus <= 202002L

template <typename S>
std::string createLogMessage(const std::string &function_preamble,
                             const S &msg)
{
  return fmt::format("{}{}", function_preamble, msg);
}

//------------------------------------------------------------------------------

template <typename... Args>
std::string createLogMessage(const std::string &function_preamble,
                             std::string_view format,
                             Args&&... args)
{
  return fmt::format("{}", function_preamble) +
         fmt::format(format,
                     std::forward<Args>(args)...);
}

#else

template <typename S>
constexpr std::string createLogMessage(const std::string &function_preamble,
                                       const S &msg)
{
  return std::format("{}{}", function_preamble, msg);
}

//------------------------------------------------------------------------------

template <typename... Args>
constexpr std::string createLogMessage(const std::string &function_preamble,
                                       std::string_view format,
                                       Args&&... args)
{
  return std::format("{}", function_preamble) +
         std::vformat(format,
                      std::make_format_args(std::forward<Args>(args)...));
}

#endif

//------------------------------------------------------------------------------

namespace logging_handler
{

//! Helper class which is used to enable global logging macros.
class LoggingHelper
{
public:
  explicit LoggingHelper() = default;
  explicit LoggingHelper(std::function<void(const LoggingHandlerInterface::LoggingLevel &,
                         const std::string &)>) = delete;
  LoggingHelper(const LoggingHelper &) = delete;
  LoggingHelper(LoggingHelper &&) = delete;
  LoggingHelper &operator=(const LoggingHelper &) = delete;
  LoggingHelper &operator=(LoggingHelper &&) = delete;
  virtual ~LoggingHelper() = default;

  //! Sets the logging callback which should be invoked from the logging macros.
  //! \param logging_callback The callback which should be invoked.
  void setLoggingCallback(std::function<void(const LoggingHandlerInterface::LoggingLevel &,
                                             const std::string &)> logging_callback);

  //! Invokes the logging_callback with the given logging level and message.
  //! \param logging_level The logging level.
  //! \param msg The message which should be logged.
  void log(const LoggingHandlerInterface::LoggingLevel &logging_level,
           const std::string &msg);

  //! Singleton method.
  //! \return Singleton object of this class.
  static std::shared_ptr<LoggingHelper> instance();

private:
  std::function<void(const LoggingHandlerInterface::LoggingLevel &,
                     const std::string&)> m_logging_callback;
};

//------------------------------------------------------------------------------

#ifdef __cpp_lib_source_location
  #define FUNC_PREAMBLE prettyFunction(std::source_location::current().function_name()) // NOLINT
#else
  #define FUNC_PREAMBLE prettyFunction(__PRETTY_FUNCTION__) // NOLINT
#endif

//------------------------------------------------------------------------------

template<typename... Args>
constexpr void setLoggingCallback(Args&&... args)
{
  logging_handler::LoggingHelper::instance()->setLoggingCallback(std::forward<Args>(args)...);
}

//------------------------------------------------------------------------------

template<typename... Args>
constexpr void debug(const std::string &preamble, const std::string &formatter, Args&&... args)
{
  logging_handler::LoggingHelper::instance()->log(
    logging_handler::LoggingHandlerInterface::LoggingLevel::kDebug,
    createLogMessage(preamble, formatter, std::forward<Args>(args)...)
  );
}

//------------------------------------------------------------------------------

template<typename... Args>
constexpr void info(const std::string &preamble, const std::string &formatter, Args&&... args)
{
  logging_handler::LoggingHelper::instance()->log(
    logging_handler::LoggingHandlerInterface::LoggingLevel::kInfo,
    createLogMessage(preamble, formatter, std::forward<Args>(args)...)
  );
}

//------------------------------------------------------------------------------

template<typename... Args>
constexpr void warning(const std::string &preamble, const std::string &formatter, Args&&... args)
{
  logging_handler::LoggingHelper::instance()->log(
    logging_handler::LoggingHandlerInterface::LoggingLevel::kWarning,
    createLogMessage(preamble, formatter, std::forward<Args>(args)...)
  );
}

//------------------------------------------------------------------------------

template<typename... Args>
constexpr void error(const std::string &preamble, const std::string &formatter, Args&&... args)
{
  logging_handler::LoggingHelper::instance()->log(
    logging_handler::LoggingHandlerInterface::LoggingLevel::kError,
    createLogMessage(preamble, formatter, std::forward<Args>(args)...)
  );
}

//------------------------------------------------------------------------------

template<typename... Args>
constexpr void critical(const std::string &preamble, const std::string &formatter, Args&&... args)
{
  logging_handler::LoggingHelper::instance()->log(
    logging_handler::LoggingHandlerInterface::LoggingLevel::kCritical,
    createLogMessage(preamble, formatter, std::forward<Args>(args)...)
  );
}

//------------------------------------------------------------------------------

} // namespace logging_handler
