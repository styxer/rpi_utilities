/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: mqttmessage
*
************************************************/

// local includes
#include "mqttmessageadaptermock.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace mqttmessage {

MqttMessageAdapterMock::MqttMessageAdapterMock()
    : m_is_connected{false}
{
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::connect(const std::string &, const std::string &)
{
  m_is_connected = true;
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::connect(const std::string &, const std::string &,
                                     const std::filesystem::path &,
                                     const std::filesystem::path &)
{
  m_is_connected = true;
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::connect(const std::string &, const std::string &, const LastWill &)
{
  m_is_connected = true;
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::disconnect()
{
  m_is_connected = false;
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::publish(const std::string &, const char *,
                                     const size_t &,
                                     const QualityOfServiceMode &, const bool &)
{
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::subscribe(const std::string &, const QualityOfServiceMode &)
{
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::unsubscribe(const std::string &)
{
}

//------------------------------------------------------------------------------

void MqttMessageAdapterMock::setConnectionStatus(const bool &is_connected)
{
  m_is_connected = is_connected;
}

} // namespace mqttmessage
