#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: mqttmessage
*
************************************************/

// local includes
#include "mqttmessageadapterinterface.h"

// common includes

// system includes
#include <functional>
#include <memory>
#include <mqtt/async_client.h>
#include <mqtt/iaction_listener.h>
#include <string>

//------------------------------------------------------------------------------

namespace mqttmessage {

constexpr int64_t kMaxConnectionTimeoutInSeconds = 30;
constexpr int kMqttVersion = 4;

//! This class implements a MQTT adapter, which enables the communication with an MQTT broker.
class MqttMessageAdapter : public MqttMessageAdapterInterface
{
  //! Helper class, which implements the callback for the paho mqtt library.
  //! Handles connection state changes and emits a signal if a message has been received.
  class ConnectionCallback : public mqtt::callback
  {
    public:
      ConnectionCallback(const ConnectionCallback &) = delete;
      ConnectionCallback(ConnectionCallback &&) = delete;
      ConnectionCallback &operator=(const ConnectionCallback &) = delete;
      ConnectionCallback &operator=(ConnectionCallback &&) = delete;

      //! Custom constructor, to pass a referemce to the MqttMessageAdapter implementation.
      //! \param mqtt_message_adapter The reference to the MqttMessageAdapter
      explicit ConnectionCallback(MqttMessageAdapter &mqtt_message_adapter);
      ~ConnectionCallback() override = default;

      //! Will be called, if the connection to the broker has been successfull.
      void connected(const std::string &cause) override;

      //! Will be called, if the connection to the broker has been lost.
      void connection_lost(const std::string &cause) override;

      //! Will be called if a message arrived. E.g. message to a subscribed topic.
      void message_arrived(mqtt::const_message_ptr msg) override;

      //! This method is not implemented.
      void delivery_complete(mqtt::delivery_token_ptr tok) override;

    private:
      MqttMessageAdapter &m_mqtt_message_adapter;
  };

friend class ConnectionCallback;

public:
  MqttMessageAdapter(const MqttMessageAdapter &) = delete;
  MqttMessageAdapter(MqttMessageAdapter &&) = delete;
  MqttMessageAdapter &operator=(const MqttMessageAdapter &) = delete;
  MqttMessageAdapter &operator=(MqttMessageAdapter &&) = delete;

  //! \param message_received_callback The callback which will be triggerd when a new message has
  //!                                  been received.
  //! \param connection_status_changed_callback The callback which will be triggered when the
  //!                                           connection status changed.
  //! \param broker_url The URL of the MQTT broker.
  //! \param client_id  The ID of the client. E.g. MQTT_CLIENT_1.
  //! \param max_connection_retries The maximal amount of connection attempts.
  explicit MqttMessageAdapter(
      std::function<void(const std::string&, const char*, const size_t)> message_received_callback,
      std::function<void(const bool&)> connection_status_changed_callback,
      std::function<void(const bool&, const std::vector<std::string>&)> delivery_complete_callback,
      const std::string &broker_url,
      const std::string &client_id);
  ~MqttMessageAdapter() override;

  //! \see interface
  void connect(const std::string &user, const std::string &password) override;

  //! \see interface
  void connect(const std::string &user, const std::string &password,
               const std::filesystem::path &root_ca_path,
               const std::filesystem::path &certificate_key_path) override;

  //! \see interface
  void connect(const std::string &user, const std::string &password,
               const LastWill &last_will) override;

  //! This method disconnects from the MQTT broker.
  void disconnect() override;

  //! Helper method which publishes a message for the given topic.
  //! \param topic    The topic on which the message will be published.
  //! \param payload  The actual message body.
  //! \param size     The size of the payload.
  //! \param qos_mode The QoS mode which should be used.
  //! \param retain   If this flag is set to true, the message will be retained by the broker.
  void publish(const std::string &topic, const char *payload,
               const size_t &size, const QualityOfServiceMode &qos_mode,
               const bool &retain) override;

  //! \see interface
  void subscribe(const std::string &topic, const QualityOfServiceMode &qos_mode) override;

  //! Helper method which unsubscribes from the given topic.
  void unsubscribe(const std::string &topic) override;

private:
  //! Helper method which sets the status of the internal connection state cache.
  void setConnectionStatus(const bool &is_connected) override;

  //! Helper method which triggeres the message received callback.
  void messageReceived(const std::string &topic,
                       const char *message,
                       size_t size) const;

  void deliveryComplete(bool complete, const std::vector<std::string>& topic) const;

  std::unique_ptr<mqtt::async_client> m_mqtt_client;
  mqtt::token_ptr m_connection_token;
  std::unique_ptr<ConnectionCallback> m_connection_callback;

  std::function<void(const std::string&, const char*, const size_t)> m_message_received_callback;
  std::function<void(const bool&)> m_connection_status_changed_callback;
  std::function<void(const bool&, const std::vector<std::string>&)> m_delivery_complete_callback;

  bool m_is_connected; //< Indicates if the client is connected or not.

  //! Helper method which connects to the MQTT broker.
  //! \param connect_options The connection options.
  bool connect(const mqtt::connect_options &connect_options);
};

} // namespace mqttmessage
