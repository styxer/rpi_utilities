/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: mqttmessage
*
************************************************/

// local includes
#include "mqttmessaginglibexception.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace mqttmessage {

MqttMessagingLibException::MqttMessagingLibException(std::string msg) :
  m_msg{std::move(msg)}
{
}

const char* MqttMessagingLibException::what() const noexcept
{
  return m_msg.c_str();
}

} // namespace mqttmessage
