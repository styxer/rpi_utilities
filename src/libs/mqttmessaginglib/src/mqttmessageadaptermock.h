#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: mqttmessage
*
************************************************/

// local includes
#include "mqttmessageadapterinterface.h"

// common includes

// system includes
#include <string>

//------------------------------------------------------------------------------

namespace mqttmessage {

//! This class implements a MQTT adapter, which enables the communication with an MQTT broker.
class MqttMessageAdapterMock : public MqttMessageAdapterInterface
{
public:
  MqttMessageAdapterMock(const MqttMessageAdapterMock &) = delete;
  MqttMessageAdapterMock(MqttMessageAdapterMock &&) = delete;
  MqttMessageAdapterMock &operator=(const MqttMessageAdapterMock &) = delete;
  MqttMessageAdapterMock &operator=(MqttMessageAdapterMock &&) = delete;

  explicit MqttMessageAdapterMock();
  ~MqttMessageAdapterMock() override = default;

  //! \see interface
  void connect(const std::string &user,
               const std::string &password) override;

  //! \see interface
  void connect(const std::string &user,
               const std::string &password,
               const std::filesystem::path &root_ca_path,
               const std::filesystem::path &certificate_key_path) override;

  //! \see interface
  void connect(const std::string &user,
               const std::string &password,
               const LastWill &last_will) override;

  //! This method disconnects from the MQTT broker.
  void disconnect() override;

  //! Helper method which publishes a message for the given topic.
  //! \param topic    The topic on which the message will be published.
  //! \param payload  The actual message body.
  //! \param size     The size of the payload.
  //! \param qos_mode The QoS mode which should be used.
  //! \param retain   If this flag is set to true, the message will be retained by the broker.
  void publish(const std::string          &topic,
               const char                 *payload,
               const size_t               &size,
               const QualityOfServiceMode &qos_mode,
               const bool                 &retain) override;

  //! \see interface
  void subscribe(const std::string &topic, const QualityOfServiceMode &qos_mode) override;

  //! Helper method which unsubscribes from the given topic.
  void unsubscribe(const std::string &topic) override;

protected:
  //! Helper method which sets the status of the internal connection state cache.
  void setConnectionStatus(const bool &is_connected) override;

private:
  bool m_is_connected; //< Indicates if the client is connected or not.
};

} // namespace mqttmessage
