#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2021
*
* Module: mqttmessage
*
************************************************/

// local includes

// common includes

// system includes
#include <exception>
#include <string>

//------------------------------------------------------------------------------

namespace mqttmessage {

class MqttMessagingLibException : public std::exception
{
public:
  MqttMessagingLibException(const MqttMessagingLibException &) = delete;
  MqttMessagingLibException(MqttMessagingLibException &&) = delete;
  MqttMessagingLibException &operator=(const MqttMessagingLibException &) = delete;
  MqttMessagingLibException &operator=(MqttMessagingLibException &&) = delete;

  explicit MqttMessagingLibException(std::string msg);
  ~MqttMessagingLibException() override = default;

  const char* what() const noexcept override;

private:
  const std::string m_msg;
};

} // namespace mqttmessage
