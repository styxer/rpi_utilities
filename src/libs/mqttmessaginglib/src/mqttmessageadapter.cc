/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: mqttmessage
*
************************************************/

// local includes
#include "mqttmessageadapter.h"
#include "mqttmessaginglibexception.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace mqttmessage {

MqttMessageAdapter::ConnectionCallback::ConnectionCallback(MqttMessageAdapter &mqtt_message_adapter)
    : m_mqtt_message_adapter{mqtt_message_adapter}
{
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::ConnectionCallback::connected(const std::string &)
{
  m_mqtt_message_adapter.setConnectionStatus(true);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::ConnectionCallback::connection_lost(const std::string &)
{
  m_mqtt_message_adapter.setConnectionStatus(false);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::ConnectionCallback::message_arrived(mqtt::const_message_ptr msg)
{
  if (msg == nullptr)
  {
    return;
  }

  m_mqtt_message_adapter.messageReceived(msg->get_topic(),
                                         msg->get_payload_ref().data(),
                                         msg->get_payload_ref().size());
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::ConnectionCallback::delivery_complete(mqtt::delivery_token_ptr tok)
{
  auto topics = tok->get_topics();

  std::vector<std::string> topics_vec;
  for (size_t i = 0; i != topics->size(); ++i)
  {
    topics_vec.push_back((*topics)[i]);
  }

  m_mqtt_message_adapter.deliveryComplete(tok->is_complete(), topics_vec);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

MqttMessageAdapter::MqttMessageAdapter(
    std::function<void(const std::string&, const char*, const size_t)> message_received_callback,
    std::function<void(const bool&)> connection_status_changed_callback,
    std::function<void(const bool&, const std::vector<std::string>&)> delivery_complete_callback,
    const std::string &broker_url,
    const std::string &client_id)
    : m_mqtt_client{std::make_unique<mqtt::async_client>(broker_url, client_id)},
      m_connection_token{nullptr},
      m_connection_callback{std::make_unique<ConnectionCallback>(*this)},
      m_message_received_callback{std::move(message_received_callback)},
      m_connection_status_changed_callback{std::move(connection_status_changed_callback)},
      m_delivery_complete_callback{std::move(delivery_complete_callback)},
      m_is_connected{false}
{
  m_mqtt_client->set_callback(*m_connection_callback);
}

//------------------------------------------------------------------------------

MqttMessageAdapter::~MqttMessageAdapter()
{
  /* NOTE: throwing exceptions from destructor is not good!
  *        therefore the exception will be ignored.
  */
  try
  {
    m_connection_token = m_mqtt_client->disconnect();
    if (m_connection_token != nullptr)
    {
      m_connection_token->wait();
    }
  }
  catch (mqtt::exception &)
  {
  }
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::connect(const std::string &user, const std::string &password)
{
  if (m_is_connected)
  {
    throw MqttMessagingLibException("Already connected. This method should only "
                                    "be called if no connection to the broker exists.");
  }

  mqtt::connect_options connection_options {user, password};

  connection_options.set_mqtt_version(kMqttVersion);
  connection_options.set_automatic_reconnect(true);

  m_is_connected = connect(connection_options);
  if (!m_is_connected)
  {
    throw MqttMessagingLibException("Could not connect to broker!");
  }
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::connect(const std::string &user,
                                 const std::string &password,
                                 const std::filesystem::path &root_ca_path,
                                 const std::filesystem::path &certificate_key_path)
{
  if (m_is_connected)
  {
    throw MqttMessagingLibException("Already connected. This method should only "
                                    "be called if no connection to the broker exists.");
  }

  const mqtt::ssl_options ssl_options = mqtt::ssl_options_builder()
                                              .trust_store(root_ca_path)
                                              .key_store(certificate_key_path)
                                              .verify()
                                              .enable_server_cert_auth(true)
                                              .finalize();

  mqtt::connect_options connection_options {user, password};

  connection_options.set_ssl(ssl_options);
  connection_options.set_mqtt_version(kMqttVersion);
  connection_options.set_automatic_reconnect(true);

  m_is_connected = connect(connection_options);

  if (!m_is_connected)
  {
    throw MqttMessagingLibException("Could not connect to broker!");
  }
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::connect(const std::string &user,
                                 const std::string &password,
                                 const LastWill &last_will)
{
  if (m_is_connected)
  {
    throw MqttMessagingLibException("Already connected. This method should only be called if no connection to the broker exists.");
  }

  mqtt::will_options last_will_options{last_will.topic, last_will.payload,
                                       last_will.payload_size,
                                       last_will.qos_mode, true};

  mqtt::connect_options connection_options {user, password};

  connection_options.set_will(last_will_options);
  connection_options.set_connect_timeout(kMaxConnectionTimeoutInSeconds);
  connection_options.set_mqtt_version(kMqttVersion);
  connection_options.set_automatic_reconnect(true);

  m_is_connected = connect(connection_options);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::disconnect()
{
  if (!m_is_connected)
  {
    throw MqttMessagingLibException("Not connected to broker. It's not usefull to disconnect a non-existing connection.");
  }

  m_connection_token = m_mqtt_client->disconnect();
  if (m_connection_token != nullptr)
  {
    m_connection_token->wait();
  }
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::publish(const std::string &topic, const char *payload,
                                 const size_t &size,
                                 const QualityOfServiceMode &qos_mode,
                                 const bool &retain)
{
  if (!m_is_connected)
  {
    throw MqttMessagingLibException("It is not possible to publish messages without a connection to the broker.");
  }

  mqtt::message_ptr message = mqtt::make_message(topic,
                                                 payload,
                                                 size,
                                                 qos_mode,
                                                 retain);
  m_mqtt_client->publish(message);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::subscribe(const std::string &topic, const QualityOfServiceMode &qos_mode)
{
  if (!m_is_connected)
  {
    throw MqttMessagingLibException("It is not possible to subscribe to a topic without a connection to the broker.");
  }

  m_mqtt_client->subscribe(topic, qos_mode);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::unsubscribe(const std::string &topic)
{
  if (!m_is_connected)
  {
      throw MqttMessagingLibException("It is not possible to un-subscribe to a topic without a connection to the broker.");
  }

  m_mqtt_client->unsubscribe(topic);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::setConnectionStatus(const bool &is_connected)
{
  if (m_connection_status_changed_callback == nullptr)
  {
    return;
  }

  m_is_connected = is_connected;
  m_connection_status_changed_callback(m_is_connected);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::messageReceived(const std::string &topic,
                                         const char *message, size_t size) const
{
  if (m_message_received_callback == nullptr)
  {
    return;
  }

  m_message_received_callback(topic, message, size);
}

//------------------------------------------------------------------------------

void MqttMessageAdapter::deliveryComplete(bool complete, const std::vector<std::string>& topic) const
{
  if (m_delivery_complete_callback == nullptr)
  {
    return;
  }

  m_delivery_complete_callback(complete, topic);
}

//------------------------------------------------------------------------------

bool MqttMessageAdapter::connect(const mqtt::connect_options &connect_options)
{
  m_connection_token = m_mqtt_client->connect(connect_options);

  if (m_connection_token != nullptr)
  {
    m_connection_token->wait();
    return true;
  }

  return false;
}

} // namespace mqttmessage
