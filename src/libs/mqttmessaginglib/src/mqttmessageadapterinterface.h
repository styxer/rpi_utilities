#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: mqttmessage
*
************************************************/

// local includes

// common includes

// system includes
#include <filesystem>
#include <string>

//------------------------------------------------------------------------------

namespace mqttmessage {

enum QualityOfServiceMode : int
{
  kAtMostOnce,
  kAtLeastOnce,
  kExactlyOnce
};

//------------------------------------------------------------------------------

struct LastWill
{
  std::string topic;
  char *payload;
  size_t payload_size;
  QualityOfServiceMode qos_mode;
};

//------------------------------------------------------------------------------

//! This class implements a MQTT adapter, which enables the communication with an MQTT broker.
class MqttMessageAdapterInterface
{
public:
  MqttMessageAdapterInterface(const MqttMessageAdapterInterface &) = delete;
  MqttMessageAdapterInterface(MqttMessageAdapterInterface &&) = delete;
  MqttMessageAdapterInterface &operator=(const MqttMessageAdapterInterface &) = delete;
  MqttMessageAdapterInterface &operator=(MqttMessageAdapterInterface &&) = delete;

  MqttMessageAdapterInterface() = default;
  virtual ~MqttMessageAdapterInterface() = default;

  //! This method tries to connect to the MQTT broker with TLS encryption.
  //! \param user     The username which is used for the authentication.
  //! \param password The password which is used for the authentication.
  virtual void connect(const std::string &user,
                       const std::string &password) = 0;

  //! This method tries to connect to the MQTT broker with TLS encryption.
  //! \param user                 The username which is used for the authentication.
  //! \param password             The password which is used for the authentication.
  //! \param root_ca_path         The path to the root CA.
  //! \param certificate_key_path The path to the file which contains
  //!                             (Client cert | Client key | CA cert)
  virtual void connect(const std::string &user,
                       const std::string &password,
                       const std::filesystem::path &root_ca_path,
                       const std::filesystem::path &certificate_key_path) = 0;

  //! This method tries to connect to the MQTT broker with the LWT set.
  //! \param user               The username which is used for the authentication.
  //! \param password           The password which is used for the authentication.
  //! \param last_will          The LWT which shall be published.
  virtual void connect(const std::string &user, const std::string &password,
                       const LastWill &last_will) = 0;

  //! This method disconnects from the MQTT broker.
  virtual void disconnect() = 0;

  //! Helper method which publishes a message for the given topic.
  //! \param topic    The topic on which the message will be published.
  //! \param payload  The actual message body.
  //! \param size     The size of the payload.
  //! \param qos_mode The QoS mode which should be used.
  //! \param retain   If this flag is set to true, the message will be retained by the broker.
  virtual void publish(const std::string &topic, const char *payload,
                       const size_t &size, const QualityOfServiceMode &qos_mode,
                       const bool &retain) = 0;

  //! Helper method which subscribes to a given topic.
  //! \param topic    Topic on which the the client will subscribe.
  //! \param qos_mode The QoS mode which should be used.
  virtual void subscribe(const std::string &topic, const QualityOfServiceMode &qos_mode) = 0;

  //! Helper method which unsubscribes from the given topic.
  virtual void unsubscribe(const std::string &topic) = 0;

protected:
  //! Helper method which sets the status of the internal connection state cache.
  virtual void setConnectionStatus(const bool &is_connected) = 0;
};

} // namespace mqttmessage
