cmake_minimum_required (VERSION 3.13)

project(cputemperature_service)

add_subdirectory(cputemperaturelib)

set (SOURCES
  main.cc
)

add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME}
  cputemperaturelib
  commonlib
  logginghandlerlib
)

install(
  TARGETS cputemperature_service
  COMPONENT cputemperature_service
  RUNTIME DESTINATION ${INSTALL_PATH}
  DESTINATION ${INSTALL_PATH}
)


