# CpuTemperature Service

## General

This services measures the CPU temperature of the RPi with the help of **vcgencmd** in a configured **measurement cycle**. The measured temperature will be published via **MQTT**. Thus it is possible to process the data within another service.
By design the **MQTT** message will be published wit QoS level 2. Since this service will most certainly only be executed on RPi's which are not running on battery this overhead is acceptable compared to the fact, that the **MQTT** message will be delivered for sure.

## Configuration

As already mentioned, it is possible to configure the service. Please have a look [here][1].

[1]: ../../config_templates/cputemperature_service.toml
