/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureadapter.h"
#include "cputemperatureadapterinterface.h"

// common includes
#include "logginghelper.h"

// system includes
#include <chrono>
#include <regex>
#include <string>

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service {

CpuTemperatureAdapter::CpuTemperatureAdapter(const common::ProcessStarterInterface &process_starter)
  : m_process_starter{process_starter}
{}

//------------------------------------------------------------------------------

std::optional<CpuTemperatureAdapterInterface::MeasurementResult> CpuTemperatureAdapter::measureCpuTemperature()
{
  const std::chrono::seconds timestamp
    = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());

  const std::string command_output = m_process_starter.startProcess("vcgencmd", {"measure_temp"});

  const std::regex regex{"temp=([0-9]+([.][0-9]*)?)'C"};
  std::smatch regex_match;

  std::string function_name;
  if (!std::regex_search(command_output, regex_match, regex))
  {
    logging_handler::warning(FUNC_PREAMBLE, "Regex did not match output: '{}'", command_output);
    return std::nullopt;
  }

  double cpu_temperature = std::stod(regex_match[1].str());

  logging_handler::info(FUNC_PREAMBLE,
                        "\nTimestamp: {}\nCurrent CPU temperature is: {}",
                        timestamp, cpu_temperature);

  return CpuTemperatureAdapterInterface::MeasurementResult{timestamp, cpu_temperature};
}

} // namespace rpi_utilities::cputemperature_service
