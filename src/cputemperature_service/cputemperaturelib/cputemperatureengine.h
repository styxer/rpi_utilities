#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureengineinterface.h"
#include "cputemperatureengineconfig.h"

// common includes
#include "async_timer.h"
#include "heartbeat.h"
#include "mqttmessageadapterinterface.h"

// system includes
#include <memory>
#include <mutex>

// forward declarations

namespace rpi_utilities::cputemperature_service
{

class CpuTemperatureAdapterInterface;

}

namespace mqttmessage
{

class MqttMessageAdapterInterface;

}

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

class CpuTemperatureEngine : public CpuTemperatureEngineInterface
{

public:
  struct ExternalModules
  {
    std::unique_ptr<CpuTemperatureEngineConfig> config;
    std::unique_ptr<CpuTemperatureAdapterInterface> cpu_temperature_adapter_interface;
  };

  CpuTemperatureEngine(const CpuTemperatureEngine &) = delete;
  CpuTemperatureEngine(CpuTemperatureEngine &&) = delete;
  CpuTemperatureEngine &operator=(const CpuTemperatureEngine &) = delete;
  CpuTemperatureEngine &operator=(CpuTemperatureEngine &&) = delete;

  explicit CpuTemperatureEngine(std::unique_ptr<ExternalModules> external_modules);
  ~CpuTemperatureEngine() override = default;

  //! \see interface
  void start() override;

  //! \see interface
  void stop() override;

  [[nodiscard]] bool isStarted() const;

private:
  std::unique_ptr<ExternalModules> m_external_modules;
  std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> m_mqtt_adapter_interface;

  common::AsyncTimer m_cpu_temperature_measurement_cycle_timer;
  common::HeartBeat m_heartbeat_timer;

  bool m_engine_started;
  bool m_connected_to_mqtt_broker;

  std::vector<char> m_serialized_message;

  std::mutex m_cpu_temperature_mutex;
  std::condition_variable m_cpu_condition_variable;

  void cpuTemperatureMeasurementCycleTimerTriggered();
  void measuredCpuTemperature(const std::chrono::seconds &timestamp, const double &cpu_temperature);
  void connectionStatusChanged(bool connected);
  void protobufPublishCompleted(bool completed, const std::vector<std::string> &topics);
};

} // namespace rpi_utilities::cputemperature_service
