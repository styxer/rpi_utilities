#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureadapterinterface.h"

// common includes
#include "processstarterinterface.h"

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

class CpuTemperatureAdapter : public CpuTemperatureAdapterInterface
{
public:
  CpuTemperatureAdapter(const CpuTemperatureAdapter &) = delete;
  CpuTemperatureAdapter(CpuTemperatureAdapter &&) = delete;
  CpuTemperatureAdapter &operator=(const CpuTemperatureAdapter &) = delete;
  CpuTemperatureAdapter &operator=(CpuTemperatureAdapter &&) = delete;

  //! \param process_starter Helper which is able to start a process and
  //! retrieve it's output.
  explicit CpuTemperatureAdapter(const common::ProcessStarterInterface &process_starter);
  ~CpuTemperatureAdapter() override = default;

  //! \see interface
  std::optional<CpuTemperatureAdapterInterface::MeasurementResult> measureCpuTemperature() override;

private:
  const common::ProcessStarterInterface &m_process_starter;
};

} // namespace rpi_utilities::cputemperature_service
