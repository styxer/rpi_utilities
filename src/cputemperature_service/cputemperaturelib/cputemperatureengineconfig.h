#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes

// common includes
#include "commonexception.h"
#include "configbase.h"
#include "rpimacros.h"

// system includes
#include <toml++/toml.hpp>

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

class CpuTemperatureEngineConfig : public config::ConfigBase
{
public:
  explicit CpuTemperatureEngineConfig(const std::filesystem::path &config_file_path)
    : config::ConfigBase{config_file_path}, m_measurement_cycle{}
  {
    std::optional<int> measurement_cycle =
        m_toml[kMeasurementSectionName][kMeasurementCycleName].value<int>();

    if (!measurement_cycle.has_value())
    {
      throw common::CommonException("Config entry 'cycle' is missing within section 'measurement'");
    }

    m_measurement_cycle = std::chrono::milliseconds{*measurement_cycle};
  }

  CpuTemperatureEngineConfig(const CpuTemperatureEngineConfig &) = default;
  CpuTemperatureEngineConfig(CpuTemperatureEngineConfig &&) = default;
  CpuTemperatureEngineConfig &operator=(const CpuTemperatureEngineConfig &) = default;
  CpuTemperatureEngineConfig &operator=(CpuTemperatureEngineConfig &&) = default;
  ~CpuTemperatureEngineConfig() override = default;

  GET_VAL(MeasurementCycle, std::chrono::milliseconds, m_measurement_cycle)

  bool operator==(CpuTemperatureEngineConfig &other)
  {
    return other.getMeasurementCycle() == getMeasurementCycle()   &&
           other.getMqttBrokerAddress() == getMqttBrokerAddress() &&
           other.getMqttClientId() == getMqttClientId()           &&
           other.getMqttTopicIds() == getMqttTopicIds()           &&
           other.getMqttUser() == getMqttUser()                   &&
           other.getMqttPassword() == getMqttPassword()           &&
           other.getHeartbeatTopic() == getHeartbeatTopic()       &&
           other.getHeartbeatCycle() == getHeartbeatCycle()       &&
           other.getLogDirectory() == getLogDirectory()           &&
           other.getLogLevel() == getLogLevel();
  }

  bool operator!=(CpuTemperatureEngineConfig &other)
  {
    return !(other == *this);
  }

private:
  static constexpr const char *kMeasurementSectionName = "measurement";
  static constexpr const char *kMeasurementCycleName = "cycle";

  std::chrono::milliseconds m_measurement_cycle;
};

} // namespace rpi_utilities::cputemperature_service
