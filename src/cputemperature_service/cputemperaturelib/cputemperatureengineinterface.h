/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes

// common includes

// system includes

// forward declarations

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

//! This class manages all necessary steps to measure the CPU temperature of the RPi.
class CpuTemperatureEngineInterface
{
public:
  CpuTemperatureEngineInterface(const CpuTemperatureEngineInterface &) = delete;
  CpuTemperatureEngineInterface(CpuTemperatureEngineInterface &&) = delete;
  CpuTemperatureEngineInterface &operator=(const CpuTemperatureEngineInterface &) = delete;
  CpuTemperatureEngineInterface &operator=(CpuTemperatureEngineInterface &&) = delete;

  CpuTemperatureEngineInterface() = default;
  virtual ~CpuTemperatureEngineInterface() = default;

  //! Starts a AsyncTimer which performs periodical measurements.
  virtual void start() = 0;

  //! Stops the whole engine. This is only possible, if the engine has already been started.
  virtual void stop() = 0;
};

} // namespace rpi_utilities::cputemperature_service
