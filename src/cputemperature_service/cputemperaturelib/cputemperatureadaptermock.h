#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureadapterinterface.h"

// common includes

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

class CpuTemperatureAdapterMock : public CpuTemperatureAdapterInterface
{
public:
  CpuTemperatureAdapterMock(const CpuTemperatureAdapterMock &) = delete;
  CpuTemperatureAdapterMock(CpuTemperatureAdapterMock &&) = delete;
  CpuTemperatureAdapterMock &operator=(const CpuTemperatureAdapterMock &) = delete;
  CpuTemperatureAdapterMock &operator=(CpuTemperatureAdapterMock &&) = delete;

  explicit CpuTemperatureAdapterMock(const double &cpu_temperature);
  ~CpuTemperatureAdapterMock() override;

  //! \see interface
  std::optional<CpuTemperatureAdapterInterface::MeasurementResult> measureCpuTemperature() override;

  double m_cpu_temperature;
};

} // namespace rpi_utilities::cputemperature_service
