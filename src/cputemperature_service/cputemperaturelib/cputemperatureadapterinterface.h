#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes

// common includes

// system includes
#include <chrono>
#include <optional>

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

//! This class performs the actual CPU temperature measurement.
class CpuTemperatureAdapterInterface
{
public:
  CpuTemperatureAdapterInterface() = default;
  CpuTemperatureAdapterInterface(const CpuTemperatureAdapterInterface &) = delete;
  CpuTemperatureAdapterInterface(CpuTemperatureAdapterInterface &&) = delete;
  CpuTemperatureAdapterInterface &operator=(const CpuTemperatureAdapterInterface &) = default;
  CpuTemperatureAdapterInterface &operator=(CpuTemperatureAdapterInterface &&) = delete;

  struct MeasurementResult
  {
    std::chrono::seconds timestamp;
    double temperature;
  };

  virtual ~CpuTemperatureAdapterInterface() = default;

  //! Method which measures the temperature of the CPU.
  virtual std::optional<MeasurementResult> measureCpuTemperature() = 0;
};

} // namespace rpi_utilities::cputemperature_service
