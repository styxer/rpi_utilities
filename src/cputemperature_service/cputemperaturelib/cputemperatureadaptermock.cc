/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureadaptermock.h"
#include "cputemperatureadapterinterface.h"

// common includes

// system includes
#include <cstdio>
#include <memory>

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

CpuTemperatureAdapterMock::CpuTemperatureAdapterMock(const double &cpu_temperature)
  : m_cpu_temperature{cpu_temperature}
{
}

//------------------------------------------------------------------------------

CpuTemperatureAdapterMock::~CpuTemperatureAdapterMock() = default;

//------------------------------------------------------------------------------

std::optional<CpuTemperatureAdapterInterface::MeasurementResult> CpuTemperatureAdapterMock::measureCpuTemperature()
{
  return CpuTemperatureAdapterInterface::MeasurementResult{0, m_cpu_temperature};
}

} // namespace rpi_utilities::cputemperature_service
