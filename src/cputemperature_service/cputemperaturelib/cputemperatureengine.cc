/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureengine.h"
#include "commonexception.h"

// common includes
#include "cputemperature.pb.h"
#include "cputemperatureadapterinterface.h"
#include "logginghelper.h"
#include "mqttmessageadapter.h"
#include "mqttmessageadapterinterface.h"
#include "rpiutilities.h"

// system includes
#include <chrono>
#include <memory>
#include <optional>
#include <vector>

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service
{

CpuTemperatureEngine::CpuTemperatureEngine(std::unique_ptr<ExternalModules> external_modules)
  : m_external_modules{std::move(external_modules)},
    m_mqtt_adapter_interface{std::make_shared<mqttmessage::MqttMessageAdapter>(
        nullptr,
        [this](auto connected) {
          connectionStatusChanged(connected);
        },
        [this](bool completed, const std::vector<std::string> &topics) {
          protobufPublishCompleted(completed, topics);
        },
        m_external_modules->config->getMqttBrokerAddress(),
        m_external_modules->config->getMqttClientId()
      )
    },
    m_cpu_temperature_measurement_cycle_timer {
      [this]() -> bool {
        cpuTemperatureMeasurementCycleTimerTriggered();
        return false;
      }
    },
    m_heartbeat_timer {
      m_mqtt_adapter_interface,
      m_external_modules->config->getHeartbeatTopic(),
      m_external_modules->config->getMqttClientId(),
      m_external_modules->config->getHeartbeatCycle()
    },
    m_engine_started{false},
    m_connected_to_mqtt_broker{false}
{
}

//------------------------------------------------------------------------------

void CpuTemperatureEngine::start()
{
  m_engine_started = true;

  m_mqtt_adapter_interface->connect(m_external_modules->config->getMqttUser(),
                                    m_external_modules->config->getMqttPassword());
}

//------------------------------------------------------------------------------

void CpuTemperatureEngine::stop()
{
  if (!m_engine_started)
  {
    logging_handler::warning(FUNC_PREAMBLE, "Can't stop engine which has never been started!");
    return;
  }

  m_cpu_temperature_measurement_cycle_timer.stop();
  m_heartbeat_timer.stop();
  m_engine_started = false;
}

//------------------------------------------------------------------------------

bool CpuTemperatureEngine::isStarted() const
{
  return m_engine_started;
}

//------------------------------------------------------------------------------

void CpuTemperatureEngine::cpuTemperatureMeasurementCycleTimerTriggered()
{
  const std::optional<CpuTemperatureAdapterInterface::MeasurementResult> result
    = m_external_modules->cpu_temperature_adapter_interface->measureCpuTemperature();

  if (result == std::nullopt)
  {
    return;
  }

  measuredCpuTemperature(result->timestamp, result->temperature);
}

//------------------------------------------------------------------------------

void CpuTemperatureEngine::measuredCpuTemperature(const std::chrono::seconds &timestamp,
                                                  const double &cpu_temperature)
{
  try
  {
    message::CpuTemperature cpu_temperature_message;
    cpu_temperature_message.set_measurement_timestamp(timestamp.count());
    cpu_temperature_message.set_node_id(m_external_modules->config->getMqttClientId());
    cpu_temperature_message.set_cpu_temperature(cpu_temperature);

    m_serialized_message.clear();
    m_serialized_message.resize(cpu_temperature_message.ByteSizeLong());

    serializeProtobufMessage<message::CpuTemperature>(cpu_temperature_message,
                                                      m_serialized_message.data(),
                                                      m_serialized_message.size());

    m_mqtt_adapter_interface->publish(*m_external_modules->config->getMqttTopicIds().begin(),
                                      m_serialized_message.data(),
                                      m_serialized_message.size(),
                                      mqttmessage::QualityOfServiceMode::kExactlyOnce,
                                      true);

    std::unique_lock guard{m_cpu_temperature_mutex};
    m_cpu_condition_variable.wait(guard);
  }
  catch(const common::CommonException &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, ex.what());
  }
}

//------------------------------------------------------------------------------

void CpuTemperatureEngine::connectionStatusChanged(const bool connected)
{
  m_connected_to_mqtt_broker = connected;
  if (!m_connected_to_mqtt_broker)
  {
    logging_handler::info(FUNC_PREAMBLE, "Connection lost. Trying to reconnect!");

    m_cpu_temperature_measurement_cycle_timer.stop();
    m_heartbeat_timer.stop();

    return;
  }

  logging_handler::info(FUNC_PREAMBLE, "Connected to broker!");

  m_cpu_temperature_measurement_cycle_timer.start(m_external_modules->config->getMeasurementCycle());
  m_heartbeat_timer.start();
}

//------------------------------------------------------------------------------

void CpuTemperatureEngine::protobufPublishCompleted(bool completed, const std::vector<std::string> &topics)
{
  logging_handler::debug(FUNC_PREAMBLE,
                         "Protobuf sent completed: {} [{}]",
                         completed,
                         topics);

  if (!completed)
  {
    logging_handler::warning(
      FUNC_PREAMBLE,
      "Could not complete the delivery of the following topics: {}", topics);

    return;
  }

  if (topics.empty())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Topics is empty!");
    return;
  }

  bool cpu_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::set<std::string> &mqtt_topic_ids =
        m_external_modules->config->getMqttTopicIds();
      return std::any_of(mqtt_topic_ids.begin(), mqtt_topic_ids.end(),
                         [&](const std::string &configured_mqtt_topic_id) {
                           return published_topic == configured_mqtt_topic_id;
                         });
    });

  if (cpu_topic_published)
  {
    m_cpu_condition_variable.notify_all();
  }

  bool heartbeat_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::string &heartbeat_mqtt_topic =
        m_external_modules->config->getHeartbeatTopic();
      return published_topic == heartbeat_mqtt_topic;
    });

  if (heartbeat_topic_published)
  {
    m_heartbeat_timer.heartbeatSuccessfullyPublished();
  }
}

} // namespace rpi_utilities::cputemperature_service
