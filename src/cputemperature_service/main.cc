/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperature_service
*
************************************************/

// local includes
#include "cputemperatureadapter.h"
#include "cputemperatureengine.h"
#include "cputemperatureengineconfig.h"
#include "filelogginghandler.h"
#include "logginghandlerinterface.h"
#include "logginghelper.h"

// common includes
#include "mqttmessageadapter.h"
#include "processstarter.h"
#include "rpimacros.h"

// system includes
#include <fstream>
#include <format>
#include <memory>
#include <optional>
#include <span>

//------------------------------------------------------------------------------

void loggingCallback(logging_handler::LoggingHandlerInterface &logging_handler_interface,
                     const logging_handler::LoggingHandlerInterface::LoggingLevel &level,
                     const std::string &msg)
{
  logging_handler_interface.log(level, msg);
}

//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  const std::span<char*> args {argv, static_cast<size_t>(argc)};
  if (argc != 2)
  {
    std::cerr << "Usage: cputemperature_service <config_file_path>\n";
    return -1;
  }

  std::unique_ptr<rpi_utilities::cputemperature_service::CpuTemperatureEngineConfig> config;
  try
  {
    config = std::make_unique<rpi_utilities::cputemperature_service::CpuTemperatureEngineConfig>(args[1]);
  }
  catch (const common::CommonException &ex)
  {
    std::cerr << std::format("Error while parsing configuration TOML: {}\n", ex.what());
    return -1;
  }

  logging_handler::FileLoggingHandler file_logging_handler {
    "cputemperature_service",
    config->getLogDirectory()
  };

  file_logging_handler.setLoggingLevel(config->getLogLevel());

  logging_handler::setLoggingCallback([&](const auto &level, const auto &msg) {
    loggingCallback(file_logging_handler, level, msg);
  });

  common::ProcessStarter process_starter;
  auto cpu_temperature_adapter = std::make_unique<
    rpi_utilities::cputemperature_service::CpuTemperatureAdapter>(
    process_starter);

  auto cpu_temperature_engine_external_modules =
    std::make_unique<rpi_utilities::cputemperature_service::CpuTemperatureEngine::ExternalModules>(std::move(config), std::move(cpu_temperature_adapter));

  rpi_utilities::cputemperature_service::CpuTemperatureEngine
      cpu_temperature_engine{std::move(cpu_temperature_engine_external_modules)};

  cpu_temperature_engine.start();

  while (std::tolower(std::cin.get()) != 'q');
  return 0;
}
