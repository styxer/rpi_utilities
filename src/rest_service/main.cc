/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: rest_service
*
************************************************/

// local includes

// common includes
#include "filelogginghandler.h"
#include "logginghandlerinterface.h"
#include "logginghelper.h"
#include "restengine.h"
#include "restengineconfig.h"

// system includes
#include "gsl/span"
#include <fstream>
#include <iostream>

//------------------------------------------------------------------------------

void loggingCallback(logging_handler::LoggingHandlerInterface &logging_handler_interface,
                     const logging_handler::LoggingHandlerInterface::LoggingLevel &level,
                     const std::string &msg)
{
  logging_handler_interface.log(level, msg);
}

//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  const gsl::span<char*> args {argv, static_cast<size_t>(argc)};
  if (argc != 2)
  {
    std::cerr << "Usage: rest_service <config_file_path>\n";
    return -1;
  }

  std::unique_ptr<rpi_utilities::rest_service::RestEngineConfig> config;
  try
  {
    config = std::make_unique<rpi_utilities::rest_service::RestEngineConfig>(args[1]);
  }
  catch (const nlohmann::json::exception &ex)
  {
    std::cerr << fmt::format("Error while parsing configuration TOML: {}\n", ex.what());
    return -1;
  }

  logging_handler::FileLoggingHandler file_logging_handler {
    "rest_service",
    config->getLogDirectory()
  };

  file_logging_handler.setLoggingLevel(config->getLogLevel());

  logging_handler::setLoggingCallback(
    [&](const logging_handler::LoggingHandlerInterface::LoggingLevel &logging_level,
        const std::string &message) {
      loggingCallback(file_logging_handler, logging_level, message);
    }
  );

  rpi_utilities::rest_service::RestEngine engine{std::move(config)};
  engine.start();

  while (std::tolower(std::cin.get()) != 'q');
  return 0;
}
