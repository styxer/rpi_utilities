# Rest Service

## General

This service implements a REST service. Actually this implements a REST endpoint to create
grocery entries (See **website** directory in the root of this repository).

## Configuration

As already mentioned, it is possible to configure the service. Please have a look [here][1].

[1](../../config_templates/rest_service.toml)
