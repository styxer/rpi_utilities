/************************************************
 *
 * Author: @styxer <styxer@tutanota.com>
 * Year:   2023
 *
 * Module: rest_service
 *
 ************************************************/

// local includes
#include "restengine.h"
#include "restengineconfig.h"

// common includes
#include "grocery_entry.pb.h"
#include "logginghelper.h"
#include "mqttmessageadapter.h"
#include "mqttmessaginglibexception.h"
#include "rpiutilities.h"

// system includes

//-----------------------------------------------------------------------------

namespace rpi_utilities::rest_service
{

RestEngine::RestEngine(std::unique_ptr<RestEngineConfig> config)
  : m_config{std::move(config)},
    m_mqtt_message_adapter{std::make_shared<mqttmessage::MqttMessageAdapter>(
      nullptr,
      [this](const bool &connected) {
        connectionStatusChangedCallback(connected);
      },
      [](bool completed, const std::vector<std::string> &topics) {
        protobufPublishCompleted(completed, topics);
      },
      m_config->getMqttBrokerAddress(), m_config->getMqttClientId())},
    m_heartbeat_timer{std::make_unique<common::AsyncTimer>([this]() -> bool {
      heartbeatTimerTriggered();
      return false;
    })}
{
}

//-----------------------------------------------------------------------------

void RestEngine::start()
{
  m_mqtt_message_adapter->connect(m_config->getMqttUser(),
                                  m_config->getMqttPassword());
}

//-----------------------------------------------------------------------------

void RestEngine::stop() { m_mqtt_message_adapter->disconnect(); }

//-----------------------------------------------------------------------------

void RestEngine::connectionStatusChangedCallback(const bool &connected)
{
  m_connected_to_broker = connected;
  if (!connected)
  {
    logging_handler::info(FUNC_PREAMBLE, "Disconnected from broker!");

    m_heartbeat_timer->stop();
    restinio::initiate_shutdown(*m_server);
    m_future.wait();

    return;
  }

  logging_handler::info(FUNC_PREAMBLE, "Connected to broker!");

  m_heartbeat_timer->start(m_config->getHeartbeatCycle());
  heartbeatTimerTriggered();

  auto router = std::make_unique<restinio::router::express_router_t<>>();
  router->http_get(
    m_config->getAllowedShopNamesPath(),
    [this](const restinio::request_handle_t &req,
           restinio::router::route_params_t) {
      const nlohmann::json &j = m_config->getAllowedShopNames();

      auto resp =
        req->create_response()
          .append_header(restinio::http_field::access_control_allow_origin, "*")
          .append_header(restinio::http_field::access_control_allow_methods,
                         "POST, GET, OPTIONS")
          .append_header(restinio::http_field::access_control_allow_headers,
                         "X-PINGOTHER, Content-Type")
          .set_body(j.dump());

      return resp.done();
    });

  router->http_post(
    m_config->getCreateRequestPath(),
    [this](const restinio::request_handle_t &req, const auto &) {
      restinio::request_handling_status_t status = createRequestHandler(req);

      auto resp =
        req->create_response()
          .append_header(restinio::http_field::access_control_allow_origin, "*")
          .append_header(restinio::http_field::access_control_allow_methods,
                         "POST, GET, OPTIONS")
          .append_header(restinio::http_field::access_control_allow_headers,
                         "X-PINGOTHER, Content-Type")
          .append_header_date_field();

      if (status == restinio::request_handling_status_t::rejected)
      {
        resp.connection_close();
      }
      else
      {
        resp.done();
      }

      return status;
    });

  router->non_matched_request_handler(
    [](const restinio::request_handle_t &req) {
      return req->create_response()
        .append_header(restinio::http_field::access_control_allow_origin, "*")
        .append_header(restinio::http_field::access_control_allow_methods,
                       "POST, GET, OPTIONS")
        .append_header(restinio::http_field::access_control_allow_headers,
                       "X-PINGOTHER, Content-Type")
        .done();
    });

  m_future = std::async(
    std::launch::async,
    [this](std::unique_ptr<restinio::router::express_router_t<>> router) {
      m_server = std::make_unique<restinio::http_server_t<MyServerTraits>>(
        restinio::own_io_context(), [&router, this](auto &settings) {
          settings.port(m_config->getRestServicePort());
          settings.request_handler(std::move(router));
        });

      restinio::run(restinio::on_thread_pool(
        4, restinio::skip_break_signal_handling(), *m_server));
    },
    std::move(router));
}

//-----------------------------------------------------------------------------

restinio::request_handling_status_t
RestEngine::createRequestHandler(const restinio::request_handle_t &req)
{
  try
  {
    nlohmann::json parsed_json = nlohmann::json::parse(req->body());
    CreateRequest create_request = parsed_json.get<RestEngine::CreateRequest>();

    constexpr int64_t kMaxMonth = 12;

    const std::set<std::string> &allowed_shop_names =
      m_config->getAllowedShopNames();
    if (allowed_shop_names.find(create_request.shop_name) ==
          allowed_shop_names.end() ||
        create_request.month > kMaxMonth)
    {
      logging_handler::warning(FUNC_PREAMBLE,
                               "Create request contained invalid data!");
      return restinio::request_not_handled();
    }

    message::GroceryEntry grocery_entry;
    grocery_entry.set_shop_name(create_request.shop_name);
    grocery_entry.set_amount(create_request.amount);
    grocery_entry.set_day(create_request.day);
    grocery_entry.set_month(create_request.month);
    grocery_entry.set_year(create_request.year);

    m_serialized_message.clear();
    m_serialized_message.resize(grocery_entry.ByteSizeLong());
    serializeProtobufMessage<message::GroceryEntry>(
      grocery_entry, m_serialized_message.data(), grocery_entry.ByteSizeLong());

    m_mqtt_message_adapter->publish(*m_config->getMqttTopicIds().begin(),
                                    m_serialized_message.data(),
                                    grocery_entry.ByteSizeLong(),
                                    mqttmessage::kExactlyOnce,
                                    false);

    return restinio::request_accepted();
  }
  catch (const nlohmann::json::exception &ex)
  {
    logging_handler::error(FUNC_PREAMBLE, "{}", ex.what());
    return restinio::request_rejected();
  }
}

//-----------------------------------------------------------------------------

void RestEngine::heartbeatTimerTriggered()
{
  try
  {
    sendHeartbeatMessage(*m_mqtt_message_adapter,
                         std::chrono::duration_cast<std::chrono::seconds>(
                           std::chrono::system_clock::now().time_since_epoch())
                           .count(),
                         m_config->getHeartbeatTopic(),
                         m_config->getMqttClientId());
  }
  catch (const common::CommonException &exception)
  {
    logging_handler::warning(FUNC_PREAMBLE, "Could not send heartbeat: {}",
                             exception.what());
  }
  catch (const mqttmessage::MqttMessagingLibException &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
  }
}

//-----------------------------------------------------------------------------

/*static*/ void RestEngine::protobufPublishCompleted(bool completed,
                                                     const std::vector<std::string> &topics)
{
  logging_handler::debug(FUNC_PREAMBLE, "Protobuf publish completed: {} [{}]",
                         completed, fmt::join(topics, ","));
}

} // namespace rpi_utilities::rest_service
