#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: rest_service
*
************************************************/

// local includes

// common includes
#include "commonexception.h"
#include "configbase.h"
#include "rpimacros.h"

// system includes
#include <set>
#include <string>

//-----------------------------------------------------------------------------

namespace rpi_utilities::rest_service {

class RestEngineConfig : public config::ConfigBase
{
public:
  RestEngineConfig(const RestEngineConfig &) = default;
  RestEngineConfig(RestEngineConfig &&) = default;
  RestEngineConfig &operator=(const RestEngineConfig &) = default;
  RestEngineConfig &operator=(RestEngineConfig &&) = default;

  //! \brief Default constructor. Is needed because of the nlohmann::json conversion
  explicit RestEngineConfig(const std::filesystem::path &config_path)
    : ConfigBase{config_path}
  {
    const std::optional<std::string> create_request_path = m_toml[kRestSectionName][kCreateRequestPathName].value<std::string>();
    const std::optional<std::string> allowed_shop_names_path = m_toml[kRestSectionName][kAllowedShopNamesPathName].value<std::string>();
    const std::optional<uint> service_port = m_toml[kRestSectionName][kServicePortName].value<uint>();
    const toml::array *allowed_shop_names = m_toml[kRestSectionName][kAllowedShopNamesName].as_array();

    if (!create_request_path.has_value() || !allowed_shop_names_path.has_value() ||
        !service_port.has_value() || allowed_shop_names == nullptr)
    {
      throw common::CommonException("Missing 'create_request_path', 'allowed_shop_names_path', "
                                    "'service_port', 'allowed_shop_names' within config TOML.");
    }

    m_create_request_path = *create_request_path;
    m_allowed_shop_names_path = *allowed_shop_names_path;
    m_service_port = *service_port;

    allowed_shop_names->for_each([this](auto &&elem) {
        if constexpr (!toml::is_string<decltype(elem)>)
        {
          return;
        }

        m_allowed_shop_names.insert(elem.as_string()->get());
    });
  }

  //! \param mqtt_broker_address_ The address of the MQTT Broker
  //! \param mqtt_client_id_ The MQTT client ID
  //! \param mqtt_user_ Ther username which is needed for authentication
  //! \param mqtt_password_ The password which is needed for authentication
  //! \param mqtt_topic_id The MQTT topic ID which will be used to publish messages
  //! \param heartbeat_topic_ The MQTT heartbeat topic ID which will be used to publish heartbeat
  //!                         messages
  //! \param log_directory_ The directory where the logfiles should be written
  //! \param heartbeat_cycle_ The intervall in which heartbeat messages should be sent
  //! \param create_request_path_ The API path for the CreateRequest
  //! \param allowed_shop_names_path_ The path for the AllowedShopNamesRequest
  //! \param rest_service_port_ The port on which the RestEngine should listen
  //! \param allowed_shop_names_ The names of all allowed shops
  RestEngineConfig(std::string &&mqtt_broker_address_,
                   std::string &&mqtt_client_id_,
                   std::string &&mqtt_user_,
                   std::string &&mqtt_password_,
                   std::set<std::string> &&mqtt_topic_ids_,
                   std::string &&heartbeat_topic_,
                   std::chrono::milliseconds heartbeat_cycle_,
                   std::filesystem::path &&log_directory_,
                   logging_handler::LoggingHandlerInterface::LoggingLevel log_level_,
                   std::string &&create_request_path_,
                   std::string &&allowed_shop_names_path_,
                   uint rest_service_port_,
                   std::set<std::string> &&allowed_shop_names_)
      : config::ConfigBase {
          std::move(mqtt_broker_address_),
          std::move(mqtt_client_id_),
          std::move(mqtt_user_),
          std::move(mqtt_password_),
          std::move(mqtt_topic_ids_),
          std::move(heartbeat_topic_),
          heartbeat_cycle_,
          std::move(log_directory_),
          log_level_
        },
        m_create_request_path{std::move(create_request_path_)},
        m_allowed_shop_names_path{std::move(allowed_shop_names_path_)},
        m_service_port{rest_service_port_},
        m_allowed_shop_names{std::move(allowed_shop_names_)}
  {}
  ~RestEngineConfig() override = default;

  GET_CONST_REF(CreateRequestPath, std::string, m_create_request_path)
  GET_CONST_REF(AllowedShopNamesPath, std::string, m_allowed_shop_names_path)
  GET_CONST_REF(RestServicePort, uint, m_service_port)
  GET_CONST_REF(AllowedShopNames, std::set<std::string>, m_allowed_shop_names)

  //! \brief Comparison operator
  //! \param other The other object which should be compared
  bool operator==(const RestEngineConfig &other) const
  {
    return other.getMqttBrokerAddress() == getMqttBrokerAddress()       &&
           other.getMqttClientId() == getMqttClientId()                 &&
           other.getMqttUser() == getMqttUser()                         &&
           other.getMqttPassword() == getMqttPassword()                 &&
           other.getMqttTopicIds() == getMqttTopicIds()                 &&
           other.getHeartbeatTopic() == getHeartbeatTopic()             &&
           other.getHeartbeatCycle() == getHeartbeatCycle()             &&
           other.getLogDirectory() == getLogDirectory()                 &&
           other.getLogLevel() == getLogLevel()                         &&
           other.getCreateRequestPath() == getCreateRequestPath()       &&
           other.getAllowedShopNamesPath() == getAllowedShopNamesPath() &&
           other.getRestServicePort() == getRestServicePort()           &&
           other.getAllowedShopNames() == getAllowedShopNames();
  }

  //! \brief Comparison operator
  //! \param other The other object which should be compared
  bool operator!=(const RestEngineConfig &other) const
  {
    return !(other == *this);
  }

private:
  static constexpr const char *kRestSectionName = "rest";
  static constexpr const char *kCreateRequestPathName = "create_request_path";
  static constexpr const char *kAllowedShopNamesPathName = "allowed_shop_names_path";
  static constexpr const char *kServicePortName = "service_port";
  static constexpr const char *kAllowedShopNamesName = "allowed_shop_names";

  std::string m_create_request_path;
  std::string m_allowed_shop_names_path;
  uint m_service_port = 0;
  std::set<std::string> m_allowed_shop_names;
};

} // namespace rpi_utilities::rest_service
