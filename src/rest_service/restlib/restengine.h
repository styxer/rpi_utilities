#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: rest_service
*
************************************************/

// local includes
#include "restengineinterface.h"

// common includes
#include "async_timer.h"
#include "nlohmann/json.hpp"
#include "restinio/all.hpp"

// system includes
#include <chrono>
#include <future>
#include <memory>
#include <string>

namespace mqttmessage {
  class MqttMessageAdapter;
}

namespace rpi_utilities::rest_service {
  class RestEngineConfig;
}

//-----------------------------------------------------------------------------

namespace rpi_utilities::rest_service {

class RestEngine : public RestEngineInterface
{
public:
  //! Helper struct which represents a CreateRequest
  struct CreateRequest
  {
    std::string shop_name;

    double amount;
    int day;
    int month;
    int year;

    //! \brief Comparison operator
    //! \param other Ther other object which should be compared
    bool operator==(const CreateRequest &other) const
    {
      return shop_name == other.shop_name &&
             amount == other.amount &&
             day == other.day &&
             month == other.month &&
             year == other.year;
    }
  };

  //! \param config The configuration for the RestEngine
  explicit RestEngine(std::unique_ptr<RestEngineConfig> config);

  //! \see interface
  void start() override;

  //! \see interface
  void stop() override;

private:
  constexpr static const char *kHttpPostMethod    = "POST";
  constexpr static const char *kHttpOptionsMethod = "OPTIONS";
  constexpr static const char *kHttpGetMethod     = "GET";

  std::unique_ptr<RestEngineConfig> m_config;
  std::shared_ptr<mqttmessage::MqttMessageAdapter> m_mqtt_message_adapter;
  std::unique_ptr<common::AsyncTimer> m_heartbeat_timer;

  bool m_connected_to_broker = false;

  // Launching a server with custom traits.
  struct MyServerTraits : public restinio::default_single_thread_traits_t
  {
    using request_handler_t = restinio::router::express_router_t<>;
  };

  std::unique_ptr<restinio::http_server_t<MyServerTraits>> m_server;

  std::future<void> m_future;

  std::vector<char> m_serialized_message;

  //! \brief Will be called if the MqttMessageAdapter connection status changed
  //! \param connected True if the MqttMessageAdapter connected to the MQTT Broker
  void connectionStatusChangedCallback(const bool &connected);

  //! \brief Helper method which handles the creation of a grocery entry
  //! \param session The session which holds the HTTP request
  //! \param body The body of HTTP request
  restinio::request_handling_status_t createRequestHandler(const restinio::request_handle_t &req);

  //! \brief Callback which will be called in the specified interval. Sends a heartbeat message
  void heartbeatTimerTriggered();

  static void protobufPublishCompleted(bool completed, const std::vector<std::string> &topics);
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(RestEngine::CreateRequest, shop_name, amount, day, month, year)

} // namespace rpi_utilities::rest_service
