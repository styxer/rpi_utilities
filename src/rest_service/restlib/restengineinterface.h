/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2023
*
* Module: rest_service
*
************************************************/

// local includes

// common includes

// system includes

// forward declarations

//------------------------------------------------------------------------------

namespace rpi_utilities::rest_service {

//! Interface for the RestEngine.
class RestEngineInterface
{
public:
  RestEngineInterface(const RestEngineInterface &) = default;
  RestEngineInterface(RestEngineInterface &&) = default;
  RestEngineInterface &operator=(const RestEngineInterface &) = default;
  RestEngineInterface &operator=(RestEngineInterface &&) = default;

  RestEngineInterface() = default;
  virtual ~RestEngineInterface() = default;

  //! Starts the engine.
  virtual void start() = 0;

  //! Stops the whole engine. This is only possible, if the engine has already been started.
  virtual void stop() = 0;
};

} // namespace rpi_utilities::rest_service
