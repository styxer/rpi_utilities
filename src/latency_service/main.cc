/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: latency_service
*
************************************************/

// local includes
#include "latencyengineconfig.h"
#include "latencyengine.h"

// common includes
#include "filelogginghandler.h"
#include "logginghandlerinterface.h"
#include "logginghelper.h"

// system includes
#include <format>
#include <iostream>
#include <memory>
#include <span>

//------------------------------------------------------------------------------

void loggingCallback(logging_handler::LoggingHandlerInterface &logging_handler_interface,
                     const logging_handler::LoggingHandlerInterface::LoggingLevel &level,
                     const std::string &msg)
{
  logging_handler_interface.log(level, msg);
}

//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  const std::span<char*> args {argv, static_cast<size_t>(argc)};
  if (argc != 2)
  {
    std::cerr << "Usage: latency_service <config_file_path>\n";
      return -1;
  }

  std::unique_ptr<rpi_utilities::latency::LatencyEngineConfig> config;
  try
  {
    config = std::make_unique<rpi_utilities::latency::LatencyEngineConfig>(args[1]);
  }
  catch (const common::CommonException &ex)
  {
    std::cerr << std::format("Error while parsing configuration TOML: {}\n", ex.what());
    return -1;
  }

  logging_handler::FileLoggingHandler file_logging_handler {
    "latency_service",
    config->getLogDirectory()
  };

  file_logging_handler.setLoggingLevel(config->getLogLevel());

  logging_handler::setLoggingCallback(
    [&](const logging_handler::LoggingHandlerInterface::LoggingLevel &logging_level,
        const std::string &message) {
      loggingCallback(file_logging_handler, logging_level, message);
    }
  );

  auto external_modules =
    std::make_unique<rpi_utilities::latency::LatencyEngine::ExternalModules>(
      std::move(config));

  rpi_utilities::latency::LatencyEngine latency_engine{std::move(external_modules)};
  latency_engine.start();

  while (std::tolower(std::cin.get()) != 'q');

  latency_engine.stop();

  return 0;
}
