# Latency Service

## General

This service is able to measure the latency to configured DNS endpoints utilizing the **ping** binary.

## Configuration

As already mentioned, it is possible to configure the service. Please have a look [here][1].

[1]: ../../config_templates/latency_service.toml
