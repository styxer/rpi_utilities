/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: latency
*
************************************************/

// local includes
#include "latencyengine.h"

// common includes
#include "latency.pb.h"
#include "logginghelper.h"
#include "mqttmessageadapter.h"
#include "processstarter.h"
#include "rpiutilities.h"

// system includes
#include <algorithm>
#include <chrono>
#include <regex>

//-----------------------------------------------------------------------------

namespace rpi_utilities::latency {

LatencyEngine::LatencyEngine(std::unique_ptr<ExternalModules> external_modules)
  : m_external_modules{std::move(external_modules)},
    m_mqtt_adapter_interface{std::make_shared<mqttmessage::MqttMessageAdapter>(
        nullptr,
        [this](bool connected) {
          connectionStatusChanged(connected);
        },
        [this](bool complete, const std::vector<std::string>& topics) {
          protobufDeliveryComplete(complete, topics);
        },
        m_external_modules->config->getMqttBrokerAddress(),
        m_external_modules->config->getMqttClientId()
      )
    },
    m_heartbeat_timer {
      m_mqtt_adapter_interface,
      m_external_modules->config->getHeartbeatTopic(),
      m_external_modules->config->getMqttClientId(),
      m_external_modules->config->getHeartbeatCycle()
    }
{
}

//-----------------------------------------------------------------------------

void LatencyEngine::start()
{
  m_mqtt_adapter_interface->connect(m_external_modules->config->getMqttUser(),
                                    m_external_modules->config->getMqttPassword());
}

//-----------------------------------------------------------------------------

void LatencyEngine::stop()
{
}

//-----------------------------------------------------------------------------

void LatencyEngine::connectionStatusChanged(bool connected)
{
  if (!connected)
  {
    logging_handler::info(FUNC_PREAMBLE, "Disconnected from broker! Trying to reconnect!");

    m_heartbeat_timer.stop();

    std::for_each(m_latency_timers.begin(), m_latency_timers.end(),
                  [](const std::shared_ptr<LatencyTimerPair>& latency_timer) {
                    logging_handler::info(FUNC_PREAMBLE,
                                          "Stopping timer for '{}'");
                    latency_timer->stopTimer();
                  });

    m_latency_timers.clear();
    return;
  }

  logging_handler::info(FUNC_PREAMBLE, "Connected to broker!");

  for (const auto &endpoint : m_external_modules->config->getEndpoints())
  {
    logging_handler::info(FUNC_PREAMBLE, "Registering timer for '{}' and interval of {}",
             endpoint.second.dns_name, endpoint.second.interval);

    m_latency_timers.emplace_back(std::make_shared<LatencyTimerPair> (
      endpoint.second.dns_name,
      endpoint.second.interval,
      [this] { latencyTimerTimeout(); }
    ));
    m_latency_timers.back()->startTimer();
  }

  m_heartbeat_timer.start();
}

//-----------------------------------------------------------------------------

void LatencyEngine::latencyTimerTimeout()
{
  std::lock_guard<std::mutex> guard{m_measurement_mutex};

  auto it = std::find_if(m_latency_timers.cbegin(),
                         m_latency_timers.cend(),
                         [](const std::shared_ptr<LatencyTimerPair>& element) {
    return element->getTimerTriggered();
  });

  if (it == m_latency_timers.end())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Could not find corresponding latency timer pair!");
    return;
  }

  const std::string endpoint_name = (*it)->getEndpointName();
  logging_handler::debug(FUNC_PREAMBLE, "Starting latency measurement with endpoint '{}'", endpoint_name);

  common::ProcessStarter process_starter;
  const std::string output = process_starter.startProcess("ping", {"-c", "1", endpoint_name});
  std::regex regex{R"((rtt min\/avg\/max\/mdev = ([0-9]+.[0-9]+)\/([0-9]+.[0-9]+)\/([0-9]+.[0-9]+)\/([0-9]+.[0-9]+) ms))"};
  std::smatch regex_match;

  if (!std::regex_search(output, regex_match, regex))
  {
    return;
  }

  const double avg_latency = std::stod(regex_match[3]);
  logging_handler::info(FUNC_PREAMBLE, "{} ms", avg_latency);

  try
  {
    message::Latency latency_message;
    latency_message.set_node_id(m_external_modules->config->getMqttClientId().c_str());
    latency_message.set_measurement_timestamp(
      std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now().time_since_epoch())
        .count());
    latency_message.set_latency(avg_latency);
    latency_message.set_endpoint_name(endpoint_name.c_str());

    m_serialized_latency_message.clear();
    m_serialized_latency_message.resize(latency_message.ByteSizeLong());
    serializeProtobufMessage<message::Latency>(latency_message,
                                               m_serialized_latency_message.data(),
                                               latency_message.ByteSizeLong());

    m_mqtt_adapter_interface->publish(*m_external_modules->config->getMqttTopicIds().begin(),
                                      m_serialized_latency_message.data(),
                                      latency_message.ByteSizeLong(),
                                      mqttmessage::QualityOfServiceMode::kExactlyOnce,
                                      true);

    std::unique_lock lock{m_mqtt_mutex};
    m_latency_condition_variable.wait(lock);
  }
  catch (const common::CommonException &ex)
  {
    logging_handler::warning(FUNC_PREAMBLE, "{}", ex.what());
  }
}

//-----------------------------------------------------------------------------

void LatencyEngine::protobufDeliveryComplete(bool complete, const std::vector<std::string>& topics)
{
  logging_handler::debug(FUNC_PREAMBLE,
                         "Protobuf delivery complete: {} {}",
                         complete,
                         topics);

  if (!complete)
  {
    logging_handler::warning(
      FUNC_PREAMBLE,
      "Could not complete the delivery of the following topics: {}", topics);

    return;
  }

  if (topics.empty())
  {
    logging_handler::warning(FUNC_PREAMBLE, "Topics is empty!");
    return;
  }

  bool latency_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::set<std::string> &mqtt_topic_ids =
        m_external_modules->config->getMqttTopicIds();
      return std::any_of(mqtt_topic_ids.begin(), mqtt_topic_ids.end(),
                         [&](const std::string &configured_mqtt_topic_id) {
                           return published_topic == configured_mqtt_topic_id;
                         });
    });

  if (latency_topic_published)
  {
    m_latency_condition_variable.notify_all();
  }

  bool heartbeat_topic_published = std::any_of(
    topics.begin(), topics.end(), [this](const std::string &published_topic) {
      const std::string &heartbeat_mqtt_topic =
        m_external_modules->config->getHeartbeatTopic();
      return published_topic == heartbeat_mqtt_topic;
    });

  if (heartbeat_topic_published)
  {
    m_heartbeat_timer.heartbeatSuccessfullyPublished();
  }
}

} // namespace rpi_utilities::latency
