#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: latency
*
************************************************/

// local includes
#include "async_timer.h"
#include "heartbeat.h"
#include "latencyengineconfig.h"
#include "latencyengineinterface.h"

// common includes
#include "mqttmessageadapterinterface.h"
#include "rpimacros.h"

// system includes
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <utility>

//------------------------------------------------------------------------------

namespace rpi_utilities::latency {

class LatencyEngine : public LatencyEngineInterface
{
public:
  struct ExternalModules
  {
    std::unique_ptr<LatencyEngineConfig> config;
  };

  LatencyEngine(const LatencyEngine &) = delete;
  LatencyEngine(LatencyEngine &&) = delete;
  LatencyEngine &operator=(const LatencyEngine &) = delete;
  LatencyEngine &operator=(LatencyEngine &&) = delete;

  //! Constructor
  //! \param external_modules ExternalModules which are passed to the engine.
  explicit LatencyEngine(std::unique_ptr<ExternalModules> external_modules);
  ~LatencyEngine() override = default;

  //! \see interface
  void start() override;

  //! \see interface
  void stop() override;

private:
  //! Callback which is triggered if the MQTT connection status changed.
  //! \param connected True if connected to the MQTT broker. False otherwise.
  void connectionStatusChanged(bool connected);

  //! Callback which is triggered if the latency timer triggered.
  void latencyTimerTimeout();

  //! Callback which is triggered if the MqttMessageAdapter finished publishing the message.
  //! \param complete True if publishing has been complete. False otherwise.
  //! \param topics The topics which have been published.
  void protobufDeliveryComplete(bool complete, const std::vector<std::string>& topics);

  struct LatencyTimerPair
  {
    explicit LatencyTimerPair() = delete;
    explicit LatencyTimerPair(std::string endpoint_name,
                              const std::chrono::milliseconds &interval,
                              std::function<void()> timer_callback)
      : latency_timer{new common::AsyncTimer([this]() -> bool {
          timerTriggeredCallback();
          return false;
        })},
        endpoint_name{std::move(endpoint_name)}, interval{interval},
        timer_callback{std::move(timer_callback)}
    {
    }

    virtual ~LatencyTimerPair()
    {
      delete latency_timer;
    }

    LatencyTimerPair(const LatencyTimerPair &other)
      : latency_timer{other.latency_timer},
        endpoint_name{other.endpoint_name},
        interval{other.interval},
        timer_triggered{other.timer_triggered}
    {
    }

    LatencyTimerPair(LatencyTimerPair &&) = delete;
    LatencyTimerPair &operator=(const LatencyTimerPair &) = delete;
    LatencyTimerPair &operator=(LatencyTimerPair &&) = delete;

    //! Starts the AsyncTimer.
    void startTimer()
    {
      latency_timer->start(interval);
    }

    //! Starts the AsyncTimer.
    void stopTimer()
    {
      latency_timer->stop().wait();
    }

    GET_REF(TimerTriggered, bool, timer_triggered)
    GET_CONST_REF(EndpointName, std::string, endpoint_name)

  private:
    common::AsyncTimer *latency_timer = nullptr; // NOLINT

    const std::string endpoint_name; // NOLINT
    const std::chrono::milliseconds interval; // NOLINT

    bool timer_triggered = false; // NOLINT
    std::function<void()> timer_callback = nullptr; // NOLINT

    //! Callback which is triggered if the AsyncTimer ran into the specified timeout.
    void timerTriggeredCallback()
    {
      if (timer_callback == nullptr)
      {
        return;
      }

      timer_triggered = true;

      timer_callback();

      timer_triggered = false;
    }
  };

  std::unique_ptr<ExternalModules> m_external_modules;
  std::shared_ptr<mqttmessage::MqttMessageAdapterInterface> m_mqtt_adapter_interface = nullptr;

  common::HeartBeat m_heartbeat_timer;
  std::vector<std::shared_ptr<LatencyTimerPair>> m_latency_timers;

  std::mutex m_measurement_mutex;

  std::mutex m_mqtt_mutex;
  std::condition_variable m_latency_condition_variable;

  std::vector<char> m_serialized_latency_message;
};

} // namespace rpi_utilities::latency
