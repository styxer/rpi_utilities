#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: latency
*
************************************************/

// local includes

// common includes

// system includes

//------------------------------------------------------------------------------

namespace rpi_utilities::latency {

class LatencyEngineInterface
{
public:
  LatencyEngineInterface(const LatencyEngineInterface &) = delete;
  LatencyEngineInterface(LatencyEngineInterface &&) = delete;
  LatencyEngineInterface &operator=(const LatencyEngineInterface &) = delete;
  LatencyEngineInterface &operator=(LatencyEngineInterface &&) = delete;

  LatencyEngineInterface() = default;
  virtual ~LatencyEngineInterface() = default;

  //! Starts the engine.
  virtual void start() = 0;

  //! Stops the engine.
  virtual void stop() = 0;
};

} // namespace rpi_utilities::latency
