#pragma once
/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2022
*
* Module: latency
*
************************************************/

// local includes

// common includes
#include "commonexception.h"
#include "configbase.h"
#include "rpimacros.h"
#include <toml++/toml.hpp>

// system includes
#include <string>
#include <map>
#include <utility>

//-----------------------------------------------------------------------------

struct Endpoint
{
  explicit Endpoint() = default;
  explicit Endpoint(std::string dns_name_,
                    std::chrono::milliseconds interval_)
    : dns_name{std::move(dns_name_)},
      interval{interval_}
  {}

  std::string dns_name;
  std::chrono::milliseconds interval{};

  bool operator==(const Endpoint &other) const
  {
    return other.dns_name == dns_name && other.interval == interval;
  }

  bool operator!=(const Endpoint &other) const
  {
    return !(*this == other);
  }
};

using EndpointsHash = std::map<std::string, Endpoint>; //< key: DNS name

//-----------------------------------------------------------------------------

namespace rpi_utilities::latency
{

class LatencyEngineConfig : public config::ConfigBase
{
public:
  LatencyEngineConfig(const LatencyEngineConfig &) = default;
  LatencyEngineConfig(LatencyEngineConfig &&) = default;
  LatencyEngineConfig &operator=(const LatencyEngineConfig &) = default;
  LatencyEngineConfig &operator=(LatencyEngineConfig &&) = default;

  explicit LatencyEngineConfig(const std::filesystem::path &config_file_path)
    : ConfigBase{config_file_path}
  {
    const toml::array *array = m_toml[kEndpointsName].as_array();
    array->for_each([this](auto &&element) {
      if constexpr (!toml::is_table<decltype(element)>)
      {
        return;
      }

      const toml::table *table = element.as_table();
      const std::optional<std::string> dns_name = (*table)[kDnsNameName].value<std::string>();
      const std::optional<int> interval = (*table)[kIntervalName].value<int>();

      if (!dns_name.has_value() || !interval.has_value())
      {
        throw common::CommonException("Config misses 'dns_name' or 'interval'");
      }

      m_endpoints.emplace(*dns_name, Endpoint {*dns_name, std::chrono::milliseconds{*interval}});
    });
  }

  explicit LatencyEngineConfig(std::string &&mqtt_broker_address,
                               std::string &&mqtt_client_id,
                               std::string &&mqtt_user,
                               std::string &&mqtt_password,
                               std::set<std::string> &&mqtt_topic_ids,
                               std::string &&heartbeat_topic,
                               std::chrono::milliseconds heartbeat_cycle,
                               std::filesystem::path &&log_directory,
                               logging_handler::LoggingHandlerInterface::LoggingLevel log_level,
                               EndpointsHash &&endpoints)
      : config::ConfigBase {
          std::move(mqtt_broker_address),
          std::move(mqtt_client_id),
          std::move(mqtt_user),
          std::move(mqtt_password),
          std::move(mqtt_topic_ids),
          std::move(heartbeat_topic),
          heartbeat_cycle,
          std::move(log_directory),
          log_level
        },
        m_endpoints{std::move(endpoints)}

  {}
  ~LatencyEngineConfig() override = default;

  GET_VAL(Endpoints, EndpointsHash, m_endpoints)

  bool operator==(LatencyEngineConfig &other)
  {
    return other.getMqttBrokerAddress() == getMqttBrokerAddress() &&
           other.getMqttClientId() == getMqttClientId()           &&
           other.getMqttUser() == getMqttUser()                   &&
           other.getMqttPassword() == getMqttPassword()           &&
           other.getMqttTopicIds() == getMqttTopicIds()           &&
           other.getHeartbeatTopic() == getHeartbeatTopic()       &&
           other.getHeartbeatCycle() == getHeartbeatCycle()       &&
           other.getLogDirectory() == getLogDirectory()           &&
           other.getLogLevel () == getLogLevel()                  &&
           other.getEndpoints() == getEndpoints();
  }

  bool operator!=(LatencyEngineConfig &other)
  {
    return !(other == *this);
  }

private:
  static constexpr const char *kEndpointsName = "endpoints";
  static constexpr const char *kDnsNameName = "dns_name";
  static constexpr const char *kIntervalName = "interval";

  EndpointsHash m_endpoints;
};

} // namespace rpi_utilities::latency
