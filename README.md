# RPi utilites

This is a collection of services which I find usefull to have for the Raspberry (or which I wanted to implement just out of curiosity or fun).

All services communicate with MQTT and as message library they use [protobuf][2] from Google.

## Architecture

All services are designed as standalone processes. They will connect to an *MQTT* broker and publish their messages to this broker.
Therefore it is quite easy to run multiple services on different physical instances. E.g. monitoring of multiple RPi's.

```
                                                          |-------------------------------|
                                                          |                               |
                                                          |                               |               |-------------------------------|
                                                          |                               |               |                               |
                                                          |          MQTT Broker          |-------------->|         DatabaseService       |
                                                          |                               |               |                               |
                                                          |                               |               |-------------------------------|
                                                          |                               |
                                                          |-------------------------------|
                                                           ^  |  ^  ^  ^  ^
|-------------------------------|                          |  |  |  |  |  |
|                               |                          |  |  |  |  |  |
|     CpuTemperatureService     |---------------------------  |  |  |  |  |
|                               |                             |  |  |  |  |
|-------------------------------|                             |  |  |  |  |
                                                              |  |  |  |  |
|-------------------------------|                             |  |  |  |  |
|                               |<-----------------------------  |  |  |  |
|     GpioControllerService     |                                |  |  |  |
|                               | -------------------------------|  |  |  |
|-------------------------------|                                   |  |  |
                                                                    |  |  |
|-------------------------------|                                   |  |  |
|                               |                                   |  |  |
|         LatencyService        |------------------------------------  |  |
|                               |                                      |  |
|-------------------------------|                                      |  |
                                                                       |  |
|-------------------------------|                                      |  |
|                               |                                      |  |
|          RESTService          |---------------------------------------  |
|                               |                                         |
|-------------------------------|                                         |
                                                                          |
|-------------------------------|                                         |
|                               |                                         |
|         ScrapeService         |------------------------------------------
|                               |
|-------------------------------|
```

## Available services

* [CpuTemperature Service][4]
* [GpioController Service][5]
* [Latency Service][6]
* [Database Service][7]
* [Scrape Service][8]
* [Rest Service][9]

## Dependencies

* [PAHO MQTT][1]
* [protobuf][2]
* [pqxx][3]
* [date][10]
* [nlohmann::json][11]

## Docker containers

For convenience reasons I'm using various docker containers which run 3rd party software

* [postgres][12]: Runs a PostgreSQL databse instance
* [mosquitto][13]: Runs a MQTT broker which is configured via a dedicated configuration file and a
                   password file
* [grafana][14]: Runs a Grafana instance with which I'm visualizing data
* [gotify][15]: Runs a service which is able to push notifications to mobile devices
* [apache][16]: Runs a webserver

Each service can be started with `sudo docker-compose up -d <SERVICE_NAME>`.

NOTE: You need to have [docker][17] and [docker-compose][18] installed.

## Cross compilation to AARCH64 aka RPi4 64-bit

* Install dependencies
  ```
  sudo pacman -Sy debootstrap qemu-user-static qemu-user-static-binfmt
  sudo debootstrap --arch arm64 bullseye ./bullseye-chroot  http://ftp.uk.debian.org/debian
  ```

### Chroot setup

* Check that all symlinks within the chroot are valid (especially libpthread.so and libdl.so have
  been wrong on my system). I checked the symlinks and if any symlink pointed to `/...` I checked if
  I could change it to point to the file directly (in case of libpthread.so and libdl.so this worked
  because the destination of the symlink is in the same folder).
* If you need to install some packages within the chroot, you can enter it via
  `sudo chroot PATH_TO_bullseye-chroot /bin/bash` and install everything with `apt-get install`
* Installing clang-16 dependencies and libc++-16
  ```
  wget https://apt.llvm.org/llvm.sh
  chmod +x llvm.sh
  ./llvm.sh 16
  apt-get install libc++-16-dev libc++abi-16-dev
  ```
### Conan setup

* Create a conan profile with the following content
  ```
  [settings]
  arch=armv8
  build_type=Release
  compiler=clang
  compiler.cppstd=17
  compiler.libcxx=libc++
  compiler.version=16
  os=Linux

  [buildenv]
  CC=/usr/bin/clang  --target=aarch64-linux-gnu --sysroot=PATH_TO_bullseye-chroot
  CXX=/usr/bin/clang++ --target=aarch64-linux-gnu --sysroot=PATH_TO_bullseye-chroot
  LD=/usr/bin/ld.lld
  AR=/usr/bin/llvm-ar
  AS=/usr/bin/llvm-as
  RC=/usr/bin/llvm-rc
  NM=/usr/bin/llvm-nm
  RANLIB=/usr/bin/llvm-ranlib
  STRIP=/usr/bin/llvm-strip
  ```
* Build project with conan
  ```
  conan install PATH_TO_CONANFILE_FOLDER --build=missing -pr:b=default -pr:h=raspberry -o build_folder=PATH_TO_BUILD_FOLDER
  conan build PATH_TO_CONANFILE_FOLDER --build=missing -pr:b=default -pr:h=raspberry -o build_folder=PATH_TO_BUILD_FOLDER
  ```

[1]: https://github.com/eclipse/paho.mqtt.cpp
[2]: https://developers.google.com/protocol-buffers/docs/overview
[3]: https://github.com/jtv/libpqxx
[4]: src/cputemperature_service/README.md
[5]: src/gpiocontroller_service/README.md
[6]: src/latency_service/README.md
[7]: src/database_service/README.md
[8]: src/smartmeter_service/README.md
[9]: src/rest_service/README.md
[10]: https://github.com/HowardHinnant/date
[11]: https://github.com/nlohmann/json
[12]: https://hub.docker.com/_/postgres
[13]: https://hub.docker.com/_/eclipse-mosquitto
[14]: https://hub.docker.com/r/grafana/grafana
[15]: https://github.com/gotify/server
[16]: https://hub.docker.com/_/httpd
[17]: https://docs.docker.com/engine/install/ubuntu/
[18]: https://docs.docker.com.zh.xy2401.com/v17.12/compose/install/
