function(create_service_file SERVICE_NAME_PREFIX TTY_INDEX)
  set(SERVICE_NAME ${SERVICE_NAME_PREFIX}.service)
  set(BINARY_PATH ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/bin/${SERVICE_NAME_PREFIX})
  set(CLI_ARGS ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/config/${SERVICE_NAME_PREFIX}.toml)

  set(SERVICE_IN ${CMAKE_CURRENT_LIST_DIR}/cmake/service_template.service.in)
  set(SERVICE_OUT ${CMAKE_CURRENT_BINARY_DIR}/${SERVICE_NAME})

  configure_file(${SERVICE_IN} ${SERVICE_OUT} @ONLY)

  # Install the service file in the share directory. The post
  # install RPM script will handle placing the file where
  # it's supposed to go.
  install(FILES ${SERVICE_OUT} DESTINATION ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/services/)
endfunction()
