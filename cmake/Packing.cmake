# these are cache variables, so they could be overwritten with -D,
set(CPACK_PACKAGE_NAME ${PROJECT_NAME})
message("-- Packing: PACKAGE NAME: ${CPACK_PACKAGE_NAME}")
# which is useful in case of packing only selected components instead of the whole thing
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "RpiUtilities"
    CACHE STRING "Package description for the package metadata"
)
set(CPACK_PACKAGE_VENDOR "styx3r")

set(CPACK_VERBATIM_VARIABLES YES)

set(CPACK_PACKAGE_INSTALL_DIRECTORY "/usr/local/bin")
SET(CPACK_OUTPUT_FILE_PREFIX "${CMAKE_SOURCE_DIR}/_packages")

# https://unix.stackexchange.com/a/11552/254512
set(CPACK_PACKAGING_INSTALL_PREFIX ${CPACK_PACKAGE_INSTALL_DIRECTORY})

set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

set(CPACK_PACKAGE_CONTACT "styxer@tutanota.com")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "styx3r")

# package name for deb. If set, then instead of some-application-0.9.2-Linux.deb
# you'll get some-application_0.9.2_amd64.deb (note the underscores too)
set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)
# that is if you want every group to have its own package,
# although the same will happen if this is not set (so it defaults to ONE_PER_GROUP)
# and CPACK_DEB_COMPONENT_INSTALL is set to YES
set(CPACK_COMPONENTS_GROUPING ALL_COMPONENTS_IN_ONE)#ONE_PER_GROUP)
# without this you won't be able to pack only specified component
set(CPACK_DEB_COMPONENT_INSTALL YES)
set(CPACK_STRIP_FILES YES)

message("-- Packing: TARGET_ARCH: ${TARGET_ARCH}")
if ("${TARGET_ARCH}" STREQUAL "armv8")
  set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE arm64)
else()
  set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS YES)
endif()

set(VERBOSE_LOGGING FALSE)
set(POST_IN ${CMAKE_CURRENT_LIST_DIR}/post_install.sh.in)
set(POST_OUT ${CMAKE_CURRENT_BINARY_DIR}/postinst)

set(SERVICES_PATH ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/services/)
install(
  FILES ${POST_OUT}
  PERMISSIONS OWNER_EXECUTE OWNER_READ
  DESTINATION ${SERVICES_PATH}
)

SET(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA ${POST_OUT})

set(CPACK_RPM_PACKAGE_REQUIRES_POSTUN "systemd")
set(CPACK_RPM_PACKAGE_REQUIRES_PREUN  "systemd")
set(CPACK_RPM_PACKAGE_REQUIRES_POST   "systemd")

# Needs to be a valid bash array
set(ROOT_SERVICES "\"gpiocontroller_service\"")
set(CONFIG_PATH ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/config/)
set(INSTALL_PREFIX ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/)

file(GLOB files "${CMAKE_CURRENT_SOURCE_DIR}/config_templates/*.toml")
foreach(file ${files})
  get_filename_component(filename ${file} NAME)
  configure_file(${file} ${CMAKE_CURRENT_BINARY_DIR}/${filename} @ONLY)
endforeach()

set(PRECONFIGURED_CONFIG_PATH ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/preconfigured_config/)
set(CONFIG_PATH ${CPACK_PACKAGE_INSTALL_DIRECTORY}/${CPACK_PACKAGE_NAME}/config/)
file(GLOB config_files "${CMAKE_CURRENT_BINARY_DIR}/*.toml")
install(
  FILES ${config_files}
  DESTINATION ${PRECONFIGURED_CONFIG_PATH}
)

configure_file(${POST_IN} ${POST_OUT} @ONLY)

execute_process(COMMAND curl -s https://curl.se/ca/cacert.pem -o ${CMAKE_SOURCE_DIR}/cacert.pem)

include(CPack)
