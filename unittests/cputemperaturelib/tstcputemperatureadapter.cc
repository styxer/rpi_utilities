/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: cputemperaturelib
*
************************************************/

// local includes
#include "cputemperatureadapter.h"

// system includes
#include <memory>

// common includes
#include "processstartermock.h"
#include <gtest/gtest.h>

//------------------------------------------------------------------------------

namespace rpi_utilities::cputemperature_service {

TEST(CpuTemperatureAdapter, NoValidTemperatureOutput) // NOLINT
{
  std::shared_ptr<common::ProcessStarterMock> process_starter_mock
    = std::make_shared<common::ProcessStarterMock>("FOOBAR");

  CpuTemperatureAdapter instance{*process_starter_mock};

  const std::optional<CpuTemperatureAdapterInterface::MeasurementResult> result
    = instance.measureCpuTemperature();

  ASSERT_TRUE(result == std::nullopt);
}

//------------------------------------------------------------------------------

TEST(CpuTemperatureAdapter, EvenTemperatureOutput) // NOLINT
{
  std::shared_ptr<common::ProcessStarterMock> process_starter_mock
    = std::make_shared<common::ProcessStarterMock>("temp=45'C");

  constexpr float kExpectedCpuTemperature = 45.0;

  CpuTemperatureAdapter instance{*process_starter_mock};

  const std::optional<CpuTemperatureAdapterInterface::MeasurementResult> result
    = instance.measureCpuTemperature();

  ASSERT_TRUE(result != std::nullopt);
  ASSERT_EQ(result->temperature, kExpectedCpuTemperature); // NOLINT
}

//------------------------------------------------------------------------------

TEST(CpuTemperatureAdapter, FloatingTemperatureOutput) // NOLINT
{
  std::shared_ptr<common::ProcessStarterMock> process_starter_mock
    = std::make_shared<common::ProcessStarterMock>("temp=45.23'C");

  constexpr float kExpectedCpuTemperature = 45.23;

  CpuTemperatureAdapter instance{*process_starter_mock};

  const std::optional<CpuTemperatureAdapterInterface::MeasurementResult> result
    = instance.measureCpuTemperature();

  ASSERT_TRUE(result != std::nullopt);
  EXPECT_FLOAT_EQ(result->temperature, kExpectedCpuTemperature); // NOLINT
}

} // namespace rpi_utilities::cputemperature_service
