/************************************************
*
* Author: @styxer <styxer@tutanota.com>
* Year:   2020
*
* Module: gpiocontrollerlib
*
************************************************/

// local includes
#include "controllers/fancontrollerplugin.h"
#include "gpioadaptermock.h"
#include "gpiocontrollerengineconfig.h"

// system includes
#include <memory>

// common includes
#include "cputemperature.pb.h"
#include "rpiutilities.h"
#include <gtest/gtest.h>

//------------------------------------------------------------------------------

namespace rpi_utilities::gpio {

//------------------------------------------------------------------------------

TEST(FanControllerPlugin, PluginNotInitialized) // NOLINT
{
  std::shared_ptr<GpioAdapterMock> gpio_adapter_mock
    = std::make_shared<GpioAdapterMock>(GpioAdapterInterface::GpioState::kHigh);

  std::shared_ptr<FanControllerPlugin> plugin_instance
    = std::make_shared<FanControllerPlugin>();

  plugin_instance->evaluateGpioCondition({}, nullptr, 0);

  ASSERT_FALSE(gpio_adapter_mock->lastOperationSuccessful());
}

//------------------------------------------------------------------------------

TEST(FanControllerPlugin, WrongGpioCondition) // NOLINT
{
  std::shared_ptr<GpioAdapterMock> gpio_adapter_mock
    = std::make_shared<GpioAdapterMock>(GpioAdapterInterface::GpioState::kHigh);

  std::shared_ptr<FanControllerPlugin> plugin_instance
    = std::make_shared<FanControllerPlugin>();

  plugin_instance->initialize(gpio_adapter_mock);

  const GpioPluginType gpio_condition = {
    {"mqtt_topic",  std::string{}},
    {"gpio_number", static_cast<size_t>(0)},
    {"path", std::string{}}
  };

  plugin_instance->evaluateGpioCondition(gpio_condition, nullptr, 0);

  ASSERT_FALSE(gpio_adapter_mock->lastOperationSuccessful());
}

//------------------------------------------------------------------------------

TEST(FanControllerPlugin, WrongProtobufMessage) // NOLINT
{
  std::shared_ptr<GpioAdapterMock> gpio_adapter_mock
    = std::make_shared<GpioAdapterMock>(GpioAdapterInterface::GpioState::kHigh);

  std::shared_ptr<FanControllerPlugin> plugin_instance
    = std::make_shared<FanControllerPlugin>();

  plugin_instance->initialize(gpio_adapter_mock);

  const GpioPluginType gpio_condition = {
    {"mqtt_topic",  std::string{}},
    {"gpio_number", static_cast<size_t>(0)},
    {"threshold",   0.0},
    {"path", std::string{}}
  };

  constexpr const char* kWrongMessage = "wrong";
  plugin_instance->evaluateGpioCondition(gpio_condition, kWrongMessage, strlen(kWrongMessage));

  ASSERT_FALSE(gpio_adapter_mock->lastOperationSuccessful());
}

//------------------------------------------------------------------------------

TEST(FanControllerPlugin, GpioEnable) // NOLINT
{
  std::shared_ptr<GpioAdapterMock> gpio_adapter_mock
    = std::make_shared<GpioAdapterMock>(GpioAdapterInterface::GpioState::kHigh);

  std::shared_ptr<FanControllerPlugin> plugin_instance
    = std::make_shared<FanControllerPlugin>();

  plugin_instance->initialize(gpio_adapter_mock);

  const GpioPluginType gpio_condition = {
    {"mqtt_topic",  std::string{}},
    {"gpio_number", static_cast<size_t>(0)},
    {"threshold",   0.0},
    {"path", std::string{}}
  };

  message::CpuTemperature cpu_temperature_message;
  cpu_temperature_message.set_measurement_timestamp(0);
  cpu_temperature_message.set_node_id("");
  cpu_temperature_message.set_cpu_temperature(0);

  std::vector<char> serialized_message(cpu_temperature_message.ByteSizeLong());
  serializeProtobufMessage(cpu_temperature_message,
                           serialized_message.data(),
                           cpu_temperature_message.ByteSizeLong());

  plugin_instance->evaluateGpioCondition(gpio_condition,
                                         serialized_message.data(),
                                         serialized_message.size());

  ASSERT_TRUE(gpio_adapter_mock->lastOperationSuccessful());
}

} // namespace rpi_utilities::gpio
