#!/bin/bash

read -s -p "API token: " api_token
echo ""
read -p "Package to upload path: " package_to_upload_path
echo ""

read -p "Tag name: " tag_name
echo ""

response=$(curl --request POST \
 --header "PRIVATE-TOKEN: ${api_token}" \
 --form "file=@${package_to_upload_path}" \
 "https://gitlab.com/api/v4/projects/20695717/uploads")

alt=$(echo ${response} | jq -r .alt)
url=$(echo ${response} | jq -r .full_path)

json_release=$(echo "{" \
" \"name\": \"${tag_name}\"," \
" \"tag_name\": \"${tag_name}\"," \
" \"description\": \"${alt}\"," \
" \"milestones\": []," \
" \"assets\": { " \
"   \"links\": [ " \
"     { " \
"       \"name\": \"${alt}\", " \
"       \"url\": \"https://gitlab.com${url}\", " \
"       \"link_type\": \"other\" " \
"     }" \
"   ]" \
" }" \
"}")

echo $response | jq

response=$(curl -X POST 'https://gitlab.com/api/v4/projects/20695717/releases' \
 --header 'Content-Type: application/json' \
 --header "PRIVATE-TOKEN: ${api_token}" \
 --data "${json_release}")

echo $response | jq
