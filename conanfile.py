from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain

import os
import glob
import subprocess


class RpiUtilities(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps"

    options = {
      "build_folder": ["ANY"],
      "with_tests": [True, False]
    }

    proto_output_path = ""
    protoc_bin_dir = ""

    def configure(self):
        self.options["spdlog"].header_only = True
        self.options["pahoo-mqtt-cpp"].ssl = True
        self.options.with_tests = True if self.settings.build_type == "Debug" else False

    def requirements(self):
        self.requires("nlohmann_json/3.11.2")
        self.requires("spdlog/1.13.0")
        self.requires("jwt-cpp/0.6.0")

        if self.options.with_tests:
            self.requires("gtest/cci.20210126")

        self.requires("libpqxx/7.9.0")
        self.requires("paho-mqtt-cpp/1.2.0")
        self.requires("date/3.0.1")
        self.requires("libcurl/8.0.1")
        self.requires("protobuf/3.21.12", run=False)
        self.requires("wiringpi/cci.20210727")
        self.requires("restinio/0.7.2")
        self.requires("ms-gsl/4.0.0")
        self.requires("tomlplusplus/3.4.0")
        self.requires("scnlib/2.0.2")

    def build_requirements(self):
        self.tool_requires("protobuf/3.21.12")

    def layout(self):
        self.folders.build = os.path.join(os.path.abspath(str(self.options.build_folder)),
                                          str(self.settings.arch),
                                          str(self.settings.build_type))

        self.folders.generators = os.path.join(os.path.abspath(str(self.options.build_folder)),
                                               str(self.settings.arch),
                                               str(self.settings.build_type),
                                               "generators")

    def generate(self):
        self.protoc_bin_dir = os.path.join(self.dependencies.build["protobuf"].cpp_info.bindirs[0], "protoc")
        self.proto_output_path = os.path.join(self.folders.build, "src/generated")

        if not os.path.exists(self.proto_output_path):
            print("Creating directory: {}".format(os.path.abspath(self.proto_output_path)))
            os.makedirs(self.proto_output_path)

        proto_files_path = os.path.join(self.recipe_folder, "protobuf/**/*.proto")

        proto_files = []
        for f in glob.glob(proto_files_path, recursive=True):
            proto_files.append(f)

        command = [
            "{}".format(self.protoc_bin_dir),
            "--proto_path={}".format(os.path.join(self.recipe_folder, "protobuf")),
            "--cpp_out={}".format(self.proto_output_path)
        ]
        command.extend(proto_files)

        print("Trying to generate protobuf files:\n"
              "\tprotoc path: {protoc}\n"
              "\tproto output path: {output}\n"
              "\tproto files: {files}".format(protoc=self.protoc_bin_dir,
                                              output=self.proto_output_path,
                                              files=", ".join(proto_files)))

        result = subprocess.run(command, cwd=self.recipe_folder, capture_output=True, text=True)

        print(result.stdout)
        print(result.stderr)
        print("Generated proto files: {}".format("True" if result.returncode == 0 else "False"))

        tc = CMakeToolchain(self)
        tc.presets_prefix = "conan-{}".format(str(self.settings.arch))
        tc.generate()

    def build(self):
        cmake = CMake(self)

        cmake.configure(cli_args=[
            "-DCMAKE_EXPORT_COMPILE_COMMANDS=1",
            "-DPROTO_OUT_DIR={}".format(self.proto_output_path),
            "-DWITH_TESTS={}".format(self.options.with_tests),
            "-DTARGET_ARCH={}".format(self.settings.arch)
        ])

        if self.settings.arch != "armv8":
            symlink_dest = os.path.join(self.recipe_folder, "compile_commands.json")
            if os.path.islink(symlink_dest):
                os.unlink(symlink_dest)

            os.symlink(os.path.join(self.folders.build, "compile_commands.json"), symlink_dest)

        cmake.build(cli_args=["-j 3"])

        if self.settings.arch == "armv8":
            result = subprocess.run(["cpack", self.folders.build, "-G", "DEB"],
                                    cwd=self.folders.build,
                                    capture_output=True,
                                    text=True)

            print(result.stdout)
            print(result.stderr)
